__author__ = 'Brian'

import time
from wchartype import *
from romkan import to_roma
from progressbar import progress_bar

def create_readings(rdng_list, inc_originals = True, inc_nanori = False):
    # make dictionary of possible readings for given kanji
    # each reading entry has dictionary of okurigana for given reading
    # each okurigana entry has list of references to originating details
    # all readings are in hiragana

    altInitDict = { 'か': 'が', 'き': 'ぎ', 'く': 'ぐ', 'け': 'げ', 'こ': 'ご',
                     'さ': 'ざ', 'し': 'じ', 'す': 'ず', 'せ': 'ぜ', 'そ': 'ぞ',
                     'た': 'だ', 'ち': 'ぢ', 'つ': 'づ', 'て': 'で', 'と': 'ど',
                     'は': 'ばぱ', 'ひ': 'びぴ', 'ふ': 'ぶぷ', 'へ': 'べぺ', 'ほ': 'ぼぽ'}
    altFinalDict = {'き': 'っ', 'く': 'っ', 'ち': 'っ', 'つ': 'っ', 'ぴ': 'っ', 'ぷ': 'っ'}
    altFinalKunDict = {'う': 'い', 'つ': 'ち', 'る': 'り', 'く': 'き', 'ぐ': 'ぎ', 'す': 'し', 'ぶ': 'び', 'む': 'み', 'ぬ':'に'}
    kun_verb_conj = {'う': 'いわえおっ', 'つ': 'ちたてとっ', 'る': 'りられろっ',
                     'ぬ': 'になねのん', 'む': 'みまめもん', 'ぶ': 'びばべぼん',
                     'く': 'きかけこい', 'ぐ': 'ぎがげごい', 'す': 'しさせそ'}

    validTypes = ['rdng_kun', 'rdng_on'] if not inc_nanori else ['rdng_kun', 'rdng_on', 'rdng_nanori']


    furigana_dict = dict()

    #add base readings and okurigana to dictionary
    for rdng in rdng_list:
        if rdng['type'] in validTypes:
            text = convert2hira(rdng['txt'])
            text = text.strip("-")
            okurigana = ''
            if len(text.split('.')) > 1:
                okurigana = text.split('.')[1]
            text = text.split(".")[0]
            add_rdng_to_furigana(furigana_dict, text, okurigana, rdng)

            #appended kun readings do not have okurigana markers - Add every variant
            if rdng['appended'] and rdng['type'] == 'rdng_kun':
                i = len(rdng['txt'])-1
                while i > 0:
                    add_rdng_to_furigana(furigana_dict, rdng['txt'][0:i], rdng['txt'][i:], rdng)
                    i -= 1


    # Create various conjugations of kun readings with okurigana.
    # Verb type (godan or general) not encoded so create all variants based on ending
    # Noun variants create two versions, one with okurigana and one all furigana - but do not add if already present
    base_readings = list(furigana_dict.keys())
    for rdng in rdng_list:
        if rdng['txt'].strip("-").find(".") > 0 or (rdng['type'] == 'rdng_kun' and rdng['appended']):
            text = rdng['txt'].strip("-").replace(".","")
            # Furigana noun form godan verbs
            if altFinalKunDict.get(text[-1]) is not None and (text[:-1]+altFinalKunDict[text[-1]]) not in base_readings:
                add_rdng_to_furigana(furigana_dict, text[:-1]+altFinalKunDict[text[-1]], '', rdng)
            # Furigana noun form general verbs
            if to_roma(text)[-3:] in ['iru','eru'] and text[:-1] not in base_readings:
                add_rdng_to_furigana(furigana_dict, text[:-1], '', rdng)

            # Conjugations godan verbs
            conjugations = kun_verb_conj[text[-1]] if kun_verb_conj.get(text[-1]) is not None else list()
            for conj in conjugations:
                if rdng['appended']:
                    i = len(text)-1
                    while i > 0:
                        okuri = text[i:-1]+ conj
                        add_rdng_to_furigana(furigana_dict, text[0:i], okuri, rdng)
                        i -= 1
                else:
                    furi_oku = rdng['txt'].strip('-').split('.')
                    add_rdng_to_furigana(furigana_dict, furi_oku[0], furi_oku[1][:-1]+conj, rdng)
            # Conjugations general verbs and 'い' adjectives
            if to_roma(text)[-3:] in ['iru', 'eru'] or text[-1] == 'い':
                if rdng['appended']:
                    i = len(text)-1
                    while i > 0:
                        okuri = text[i:-1]
                        add_rdng_to_furigana(furigana_dict, text[0:i], okuri, rdng)
                        i -= 1
                else:
                    furi_oku = rdng['txt'].strip('-').split('.')
                    add_rdng_to_furigana(furigana_dict, furi_oku[0], furi_oku[1][:-1], rdng)




    # add initial sounds changes - unaspirated to aspirated sound changes ka -> ga
    reading_list = list(furigana_dict.keys())
    for reading in reading_list:
        if altInitDict.get(reading[:1]) is not None:
            for char in altInitDict.get(reading[:1]):
                text = char+reading[1:]
                if furigana_dict.get(text) is None:
                    furigana_dict.update({text:furigana_dict[reading]})

    # add final sound changes - add stop (small つ) かく　-> かっ
    reading_list = list(furigana_dict.keys())
    for reading in reading_list:
        if altFinalDict.get(reading[-1]) is not None:
            text =  reading[:-1]+altFinalDict.get(reading[-1])
            if furigana_dict.get(text) is None:
                furigana_dict.update({text:furigana_dict[reading]})


    # add readings with removal of final ん
    reading_list = list(furigana_dict.keys())
    for reading in reading_list:
        if reading[-1] == "ん":
            if furigana_dict.get(reading[:-1]) is None:
                furigana_dict.update({reading[:-1]:furigana_dict[reading]})

    # sound change to handle words like 天皇 (てんのう)
    #       the preceding 'ん' affects the sound of the following kanji if it starts with あ、い、う、え、お
    sound_change_map = {'あ':'な','い':'に','う':'ぬ','え':'ね','お':'の'}
    for rdng in rdng_list:
        if rdng['type'] == 'rdng_on' and sound_change_map.get(convert2hira(rdng['txt'][0])) is not None:
            text = sound_change_map[convert2hira(rdng['txt'][0])] + convert2hira(rdng['txt'][1:])
            add_rdng_to_furigana(furigana_dict, text, '', rdng)

    return furigana_dict

def add_rdng_to_furigana(furigana_dict, text, okurigana, rdng, append=True):
    if furigana_dict.get(text) is None:
        okurigana_dict = {okurigana: [rdng]}
        if len(okurigana) > 0:
                okurigana_dict.update({'':[rdng]})
        furigana_dict.update({text:okurigana_dict})
    else:
        if furigana_dict[text].get(okurigana) is None:
            furigana_dict[text].update({okurigana:[rdng]})
        else:
            if rdng not in furigana_dict[text][okurigana] and append:
                furigana_dict[text][okurigana].append(rdng)
        if rdng not in furigana_dict[text][''] and append:
                furigana_dict[text][''].append(rdng)

def hyphen_pos(txt):
    # hyphen position -1 left, 0 none, 1 right
    if txt.find("-") >=0:
        return -1 if txt.find("-") else 1
    else:
        return 0

def create_rdngs(rdng_list, inc_originals = True, inc_nanori = False):
    altInitDict = { 'か': 'が', 'き': 'ぎ', 'く': 'ぐ', 'け': 'げ', 'こ': 'ご',
                     'さ': 'ざ', 'し': 'じ', 'す': 'ず', 'せ': 'ぜ', 'そ': 'ぞ',
                     'た': 'だ', 'ち': 'ぢ', 'つ': 'づ', 'て': 'で', 'と': 'ど',
                     'は': 'ばぱ', 'ひ': 'びぴ', 'ふ': 'ぶぷ', 'へ': 'べぺ', 'ほ': 'ぼぽ'}
    altFinalDict = {'き': 'っ', 'く': 'っ', 'ち': 'っ', 'つ': 'っ', 'ぴ': 'っ', 'ぷ': 'っ'}
    altFinalKunDict = {'う': 'い', 'つ': 'ち', 'る': 'り', 'く': 'き', 'ぐ': 'ぎ', 'す': 'し', 'ぶ': 'び', 'む': 'み'}
    validTypes = ['rdng_kun', 'rdng_on'] if not inc_nanori else ['rdng_kun', 'rdng_on', 'rdng_nanori']
    originals = []
    txt_list = []
    for rdng in rdng_list:
        if rdng['type'] in validTypes:
            text = convert2hira(rdng['txt'])
            text = text.strip("-")
            text = text.split(".")[0]
            originals.append({'txt': text, 'detail': rdng})
            txt_list.append((text, rdng['txt']))
            if rdng['appended'] and rdng['type'] == 'rdng_kun':
                i = len(rdng['txt'])-1
                while i > 0:
                    if rdng['txt'][0:i] not in txt_list:
                        txt_list.append((rdng['txt'][0:i], rdng['txt']))
                        originals.append({'txt': rdng['txt'][0:i], 'detail': rdng})
                    i -= 1
    verb_to_nouns = []
    for rdng in rdng_list:
        if rdng['txt'].strip("-").find(".") >= 0 or (rdng['type'] == 'rdng_kun' and rdng['appended']):
            if altFinalKunDict.get(rdng['txt'][-1]) is not None:
                extra_reading = rdng['txt'].strip("-").replace('.','')[:-1]+altFinalKunDict.get(rdng['txt'][-1])
                if (extra_reading, rdng['txt']) not in txt_list:
                    txt_list.append((extra_reading, rdng['txt']))
                    verb_to_nouns.append({'txt': extra_reading, 'detail': rdng})
            # if len(rdng['txt'].strip("-").split(".")[1]) == 1:
            #     expanded_list.append({'txt':rdng['txt'].strip("-").replace('.',''),'detail':rdng})
            if to_roma(rdng['txt'].strip("-").replace(".",""))[-3:] in ['iru','eru']:
                extra_reading = rdng['txt'].strip("-").replace('.','')[:-1]
                if (extra_reading, rdng['txt']) not in txt_list:
                    txt_list.append((extra_reading, rdng['txt']))
                    verb_to_nouns.append({'txt': extra_reading, 'detail': rdng})
    sound_changes = []

    # add initial sounds changes - unaspirated to aspirated sound changes ka -> ga
    for rdng in originals+verb_to_nouns:
        #if rdng['type'] not in validTypes:
        #    continue
        text = rdng['txt']
        if altInitDict.get(text[:1]) is not None:
            for char in altInitDict.get(text[:1]):
                extra_reading = char+text[1:]
                if (extra_reading, rdng['detail']['txt']) not in txt_list:
                    txt_list.append((extra_reading, rdng['detail']['txt']))
                    sound_changes.append({'txt': extra_reading, 'detail': rdng['detail']})

    sound_changes_2 = []

    # add final sound changes - add stop (small つ) かく　-> かっ
    for rdng in originals+verb_to_nouns+sound_changes:
        text = rdng['txt']
        if altFinalDict.get(text[-1]) is not None:
            extra_reading =  text[:-1]+altFinalDict.get(text[-1])
            if (extra_reading, rdng['detail']['txt']) not in txt_list:
                txt_list.append((extra_reading, rdng['detail']['txt']))
                sound_changes_2.append({'txt': extra_reading, 'detail': rdng['detail']})
    sound_changes += sound_changes_2
    sound_changes_2 = []

    # add readings with removal final ん
    for rdng in originals+verb_to_nouns+sound_changes:
        text = rdng['txt']
        if text[-1] == "ん":
            extra_reading = text[:-1]
            if (extra_reading, rdng['detail']['txt']) not in txt_list:
                txt_list.append((extra_reading, rdng['detail']['txt']))
                sound_changes_2.append({'txt': extra_reading, 'detail': rdng['detail']})
    sound_changes += sound_changes_2

    # sound change to handle words like 天皇 (てんのう)
    #       the preceding 'ん' affects the sound of the following kanji if it starts with あ、い、う、え、お
    n_sound_change = []
    sound_change_map = {'あ':'な','い':'に','う':'ぬ','え':'ね','お':'の'}
    for rdng in originals+verb_to_nouns+sound_changes:
        text = rdng['txt']
        if rdng['detail']['type'] == 'rdng_on' and text[0] in list(sound_change_map.keys()):
            extra_reading = sound_change_map[text[0]] + text[1:]
            if (extra_reading, rdng['detail']['txt']) not in txt_list:
                txt_list.append((extra_reading, rdng['detail']['txt']))
                n_sound_change.append({'txt':extra_reading, 'detail': rdng['detail']})
    sound_changes += n_sound_change
    if inc_originals:
        return originals+verb_to_nouns+sound_changes
    return verb_to_nouns+sound_changes


def convert2hira(text):
    katadict = {'ガ': 'が', 'ギ': 'ぎ', 'グ': 'ぐ', 'ゲ': 'げ', 'ゴ': 'ご',
                'ザ': 'ざ', 'ジ': 'じ', 'ズ': 'ず', 'ゼ': 'ぜ', 'ゾ': 'ぞ',
                'ダ': 'だ', 'ヂ': 'ぢ', 'ヅ': 'づ', 'デ': 'で', 'ド': 'ど',
                'バ': 'ば', 'ビ': 'び', 'ブ': 'ぶ', 'ベ': 'べ', 'ボ': 'ぼ',
                'パ': 'ぱ', 'ピ': 'ぴ', 'プ': 'ぷ', 'ペ': 'ぺ', 'ポ': 'ぽ',
                'ア': 'あ', 'イ': 'い', 'ウ': 'う', 'エ': 'え', 'オ': 'お',
                'カ': 'か', 'キ': 'き', 'ク': 'く', 'ケ': 'け', 'コ': 'こ',
                'サ': 'さ', 'シ': 'し', 'ス': 'す', 'セ': 'せ', 'ソ': 'そ',
                'タ': 'た', 'チ': 'ち', 'ツ': 'つ', 'テ': 'て', 'ト': 'と',
                'ナ': 'な', 'ニ': 'に', 'ヌ': 'ぬ', 'ネ': 'ね', 'ノ': 'の',
                'ハ': 'は', 'ヒ': 'ひ', 'フ': 'ふ', 'ヘ': 'へ', 'ホ': 'ほ',
                'マ': 'ま', 'ミ': 'み', 'ム': 'む', 'メ': 'め', 'モ': 'も',
                'ヤ': 'や', 'ユ': 'ゆ', 'ヨ': 'よ',
                'ラ': 'ら', 'リ': 'り', 'ル': 'る', 'レ': 'れ', 'ロ': 'ろ',
                'ワ': 'わ', 'ヲ': 'を', 'ン': 'ん',
                'ァ': 'ぁ', 'ィ': 'ぃ', 'ゥ': 'ぅ', 'ェ': 'ぇ', 'ォ': 'ぉ',
                'ャ': 'ゃ', 'ュ': 'ゅ', 'ョ': 'ょ', 'ッ': 'っ',
                'ｶﾞ': 'が', 'ｷﾞ': 'ぎ', 'ｸﾞ': 'ぐ', 'ｹﾞ': 'げ', 'ｺﾞ': 'ご',
                'ｻﾞ': 'ざ', 'ｼﾞ': 'じ', 'ｽﾞ': 'ず', 'ｾﾞ': 'ぜ', 'ｿﾞ': 'ぞ',
                'ﾀﾞ': 'だ', 'ﾁﾞ': 'ぢ', 'ﾂﾞ': 'づ', 'ﾃﾞ': 'で', 'ﾄﾞ': 'ど',
                'ﾊﾞ': 'ば', 'ﾋﾞ': 'び', 'ﾌﾞ': 'ぶ', 'ﾍﾞ': 'べ', 'ﾎﾞ': 'ぼ',
                'ﾊﾟ': 'ぱ', 'ﾋﾟ': 'ぴ', 'ﾌﾟ': 'ぷ', 'ﾍﾟ': 'ぺ', 'ﾎﾟ': 'ぽ',
                'ｱ': 'あ', 'ｲ': 'い', 'ｳ': 'う', 'ｴ': 'え', 'ｵ': 'お',
                'ｶ': 'か', 'ｷ': 'き', 'ｸ': 'く', 'ｹ': 'け', 'ｺ': 'こ',
                'ｻ': 'さ', 'ｼ': 'し', 'ｽ': 'す', 'ｾ': 'せ', 'ｿ': 'そ',
                'ﾀ': 'た', 'ﾁ': 'ち', 'ﾂ': 'つ', 'ﾃ': 'て', 'ﾄ': 'と',
                'ﾅ': 'な', 'ﾆ': 'に', 'ﾇ': 'ぬ', 'ﾈ': 'ね', 'ﾉ': 'の',
                'ﾊ': 'は', 'ﾋ': 'ひ', 'ﾌ': 'ふ', 'ﾍ': 'へ', 'ﾎ': 'ほ',
                'ﾏ': 'ま', 'ﾐ': 'み', 'ﾑ': 'む', 'ﾒ': 'め', 'ﾓ': 'も',
                'ﾔ': 'や', 'ﾕ': 'ゆ', 'ﾖ': 'よ',
                'ﾗ': 'ら', 'ﾘ': 'り', 'ﾙ': 'る', 'ﾚ': 'れ', 'ﾛ': 'ろ',
                'ﾜ': 'わ', 'ｦ': 'を', 'ﾝ': 'ん',
                'ｧ': 'ぁ', 'ｨ': 'ぃ', 'ｩ': 'ぅ', 'ｪ': 'ぇ', 'ｫ': 'ぉ',
                'ｬ': 'ゃ', 'ｭ': 'ゅ', 'ｮ': 'ょ', 'ｯ': 'っ'}
    cnvtd_str = str()
    for idx, char in enumerate(text):
        if char == 'ﾞ' or char == 'ﾟ':
            continue
        char_change = char
        if idx + 1 < len(text):
            if text[idx+1] == 'ﾞ' or text[idx+1] == 'ﾟ':
                char_change += text[idx+1]
        if katadict.get(char_change) is not None:
            cnvtd_str += katadict.get(char_change)
        else:
            cnvtd_str += char
    return cnvtd_str

#def _make_kanji_match(kanji, detail, txt_in_reading):
#    return _make_kanji_match(kanji, detail, "", txt_in_reading)

def _make_kanji_match(kanji, detail, okurigana, txt_in_reading):
    return {'char': kanji,
            'detail': detail,          # detail_entry list details: [txt, jy_status, type, appended]
            'okurigana': okurigana,
            'txt': txt_in_reading}

def _make_possible_furigana_list2(kanji_dict, kanji_txt, rdng_txt,
                                  all_match_sets, current_set, removed_okurigana ='', debug=False, nanori=False):
    if debug: print("Current Text: " + kanji_txt + ":" +rdng_txt)
    if len(kanji_txt) == 0 and len(rdng_txt) == 0:
        if bool(removed_okurigana):
            current_set.append(_make_kanji_match("", [], removed_okurigana, ""))
        all_match_sets.append(current_set)
        if debug: user_input = input("Match found. CONTINUE")
        return True
    elif len(kanji_txt) == 0 or len(rdng_txt) == 0:
        if debug: user_input = input("No match found - Leftover rdng/kanji. CONTINUE")
        return False
    elif kanji_txt[-1] == rdng_txt[-1]:
        if debug: print("\tRemoving last character: " + kanji_txt[-1])
        _make_possible_furigana_list2(kanji_dict, kanji_txt[:-1], rdng_txt[:-1],
                                     all_match_sets, current_set, rdng_txt[-1] + removed_okurigana, debug, nanori)
        return
    else:
        # add check for 々
        if kanji_txt[-1] == '々' and len(kanji_txt) > 1:
            kanji_char = kanji_dict.get(kanji_txt[-2])
        else:
            kanji_char = kanji_dict.get(kanji_txt[-1])
        if kanji_char is None:
            if debug: user_input = input("No match found - Kanji not found: " + kanji_txt[-1] + " CONTINUE")
            return False
        furigana_dict = kanji_char['furigana_dict'] if not nanori else kanji_char['furigana_nanori']
        max_furi_length = min(max([len(x) for x in furigana_dict.keys()]),len(rdng_txt))
        found_match = False
        if debug: print("\tBegin testing: max_len-{0} kanji-{1}".format(max_furi_length, kanji_txt[-1]))
        while max_furi_length > 0:
            test_reading = convert2hira(rdng_txt[len(rdng_txt)-max_furi_length:])
            if debug: print("\tTesting reading: {0}:{1} cur_len-{2}".format(kanji_txt[-1], test_reading, max_furi_length))
            if furigana_dict.get(test_reading) is not None:
                found_match = True
                i = len(removed_okurigana)
                if debug: print("\tFound matching reading: {0}:{1}".format(kanji_txt[-1], test_reading))
                while i >= 0:
                    if furigana_dict[test_reading].get(removed_okurigana[:i]) is not None:
                        new_furigana = current_set.copy()
                        if bool(removed_okurigana[i:]):
                            new_furigana.append(_make_kanji_match("", [], removed_okurigana[i:], ""))
                        new_furigana.append(_make_kanji_match(kanji_txt[-1],
                                                              furigana_dict[test_reading][removed_okurigana[:i]],
                                                              removed_okurigana[:i],
                                                              rdng_txt[len(rdng_txt)-max_furi_length:]))
                        if debug: print("\tFound match removing: " + kanji_txt[-1] + ":" + test_reading +' ' + removed_okurigana)

                        _make_possible_furigana_list2(kanji_dict, kanji_txt[:-1], rdng_txt[:-1*len(test_reading)],
                                                      all_match_sets, new_furigana, "", debug, nanori)
                        break
                    i -= 1
            max_furi_length -= 1
        result = False

        if not found_match and debug: user_input = input("No match found. Rdng: " + rdng_txt +
                                     " Kanji_txt: " +
                                     kanji_txt + " Kanji: " + kanji_char['txt'] + " CONTINUE")
        return result


def _make_possible_furigana_list(kanji_dict, kanji_txt, rdng_txt, all_rdngs, current_furigana, debug=False, nanori=False):
    if debug: print("Current Text: " + kanji_txt + ":" +rdng_txt)
    if len(kanji_txt) == 0 and len(rdng_txt) == 0:
        all_rdngs.append(current_furigana)
        if debug: user_input = input("Match found. CONTINUE")
        return True
    elif len(kanji_txt) == 0 or len(rdng_txt) == 0:
        if debug: user_input = input("No match found - Leftover rdng/kanji. CONTINUE")
        return False
    elif kanji_txt[-1] == rdng_txt[-1]:
        if debug: print("\tRemoving last character: " + kanji_txt[-1])
        return _make_possible_furigana_list(kanji_dict, kanji_txt[:-1], rdng_txt[:-1], all_rdngs, current_furigana, debug, nanori)
    else:
        # add check for 々
        if kanji_txt[-1] == '々' and len(kanji_txt) > 1:
            kanji_char = kanji_dict.get(kanji_txt[-2])
        else:
            kanji_char = kanji_dict.get(kanji_txt[-1])
        if kanji_char is None:
            if debug: user_input = input("No match found - Kanji not found: " + kanji_txt[-1] + " CONTINUE")
            return False
        char_rdng_list = create_rdngs(kanji_char['details'], inc_nanori=nanori)
        result = False
        for char_rdng in char_rdng_list:
            if char_rdng['txt'] == rdng_txt[-1*len(char_rdng['txt']):]:
                new_furigana = current_furigana.copy()
                new_furigana.append(_make_kanji_match(kanji_txt[-1],
                                             char_rdng['detail'],
                                             char_rdng['txt']))
                if debug: print("\tRemoving found match: " + kanji_txt[-1] + ":" + char_rdng['txt'])
                result = _make_possible_furigana_list(kanji_dict, kanji_txt[:-1],
                                                rdng_txt[:-1*len(char_rdng['txt'])],
                                                all_rdngs, new_furigana, debug, nanori)
        if debug: user_input = input("No match found. Rdng: " + rdng_txt +
                                     " Kanji_txt: " +
                                     kanji_txt + " Kanji: " + kanji_char['txt'] + " CONTINUE")
        return result


def _get_reading_exceptions(kanji_dict, kanji_txt, rdng_txt):
    current_readings = []
    for char in kanji_txt:
        kanji_char = kanji_dict.get(char)
        if kanji_char is not None:
            for detail in kanji_char['details']:
                if detail['type'] == 'rdng_excp':
                    exception_txt = detail['txt'].split(',')[0]
                    exception_rdng = convert2hira(detail['txt'].split(',')[1])
                    if exception_txt in kanji_txt and exception_rdng in rdng_txt:
                        # pos_txt = kanji_txt.find(exception_txt)
                        # pos_rdng = rdng_txt.find(exception_rdng)
                        # tokens_txt = kanji_txt.split(exception_txt)
                        # tokens_rdng = rdng_txt.split(exception_rdng)
                        # if len(tokens_txt) != len(tokens_rdng):
                        #     return []
                        current_readings.append(_make_kanji_match(kanji_char['txt'], [detail], "", exception_rdng))
    return current_readings


def make_possible_furigana_list_debug(kanji_dict, kanji_txt, rdng_txt, all_rdngs, current_furigana):
    return _make_possible_furigana_list(kanji_dict,kanji_txt,rdng_txt,all_rdngs,current_furigana,debug = True)

def make_furigana_list(kanji_dict, kanji_txt, rdng_txt, nanori = False):
    txt_rdng_matches = []
    _make_possible_furigana_list2(kanji_dict, kanji_txt, rdng_txt, txt_rdng_matches, [], nanori= nanori)
    #txt_rdng_matches = sorted(txt_rdng_matches, key = lambda y: sum([convert2hira(x['detail']['txt'].split('.')[0]) == x['txt'] for x in y]), reverse= True)
    return txt_rdng_matches



def generate_furigana_per_name(name_dict, kanji_dict):
    progress = progress_bar(len(name_dict['given_names']['dict']), "generating furigana for given names")
    for values in name_dict['given_names']['dict'].values():
        for entry in values:
            txt_rdng_matches = []
            _make_possible_furigana_list2(kanji_dict,entry['txt'],entry['rdng'],txt_rdng_matches, [], nanori=True)
            entry.update({'furigana_matches': txt_rdng_matches})
            for txt_rdng in txt_rdng_matches:
                for each in txt_rdng:
                    for detail in each['detail']:
                        detail.setdefault('name_count', 0)
                        detail['name_count'] += 1
        progress.inc_counter()

    progress.complete()
    return

def generate_furigana_per_surname(surname_dict, kanji_dict):
    progress = progress_bar(len(surname_dict), "generating furigana for surnames")
    for name in surname_dict.values():
        for entry in name:
            txt_rdng_matches = []
            _make_possible_furigana_list2(kanji_dict, entry['txt'],entry['rdng'],txt_rdng_matches, [], nanori=True)
            entry.update({'furigana_matches': txt_rdng_matches})
            for txt_rdng in txt_rdng_matches:
                for each in txt_rdng:
                    for detail in each['detail']:
                        detail.setdefault('name_count', 0)
                        detail['name_count'] += 1
        progress.inc_counter()
    progress.complete()


def generate_furigana_per_fullname(full_name_dict, kanji_dict):
    progress = progress_bar(len(full_name_dict), "generating furigana for full names")
    count_extras = 0
    for name_entry in full_name_dict.values():
        for entry in name_entry:
            txt_rdng_matches = []
            _make_possible_furigana_list2(kanji_dict, entry['txt'],
                                         entry['rdng'], txt_rdng_matches, [], nanori = True)
            if len(txt_rdng_matches) == 0:
                reading = entry['rdng']
                if reading.find('の') > 1:
                    reading = reading[0] + reading[1:].replace('の', '')
                    _make_possible_furigana_list2(kanji_dict, entry['txt'],
                                         reading, txt_rdng_matches, [], nanori = True)
                    if len(txt_rdng_matches) > 0:
                        count_extras += 1
            # entry['furigana_matches']  = txt_rdng_matches
            entry.update({'furigana_matches':txt_rdng_matches})
            for txt_rdng in txt_rdng_matches:
                    for each in txt_rdng:
                        for detail in each['detail']:
                            detail.setdefault('name_count', 0)
                            detail['name_count'] += 1
        progress.inc_counter()
    progress.complete()
    print("Found extras: {0}".format(count_extras))

def generate_furigana_per_kanji(jmdict_list, kanji_dict):
    progress = progress_bar(len(jmdict_list), "matching character readings to entry readings, creating match list.")

    for entry in jmdict_list:
        #
        # travers entry in jmdict
        # char_list holds all kanji characters used in entry
        #
        progress.inc_counter()
        for kanji_word in entry['kanji']:
            # 
            # For each kanji word in an entry make a reading list for it
            # update kanji entry to hold kanji_reading pairs
            #
            # char_list = []
            kanji_rdng = kanji_word['kanji_rdng']
            #kanji_word.update({'kanji_rdng':kanji_rdng})

            # #
            # #   add entry to kanji
            # #
            # for char in kanji_word['txt']:
            #     kanji_char = kanji_dict.get(char)
            #     if kanji_char is not None and char not in char_list:
            #         char_list.append(char)
            #         if "words" not in kanji_char.keys():
            #             kanji_char.update({"words":[]})
            #         kanji_char["words"].append({'kanji_word': kanji_word, 'entry':entry})

            for rdng in entry['rdng']:
                #
                # For each rdng check that is has kanji, and 
                # that it either has not specific kanji or it's specific kanji contains the kanji_word
                #
                if not rdng['no_kanji'] and (len(rdng['kanji']) == 0 or kanji_word['txt'] in rdng['kanji']):
                    kanji_rdng_entry = {'rdng': rdng, 'nanori':False, 'furigana_matches': []}
                    kanji_rdng.append(kanji_rdng_entry)
                    #
                    #   Compare rdng to word
                    #
                    _make_possible_furigana_list2(kanji_dict, kanji_word['txt'], rdng['txt'],
                                                  kanji_rdng_entry['furigana_matches'], [], "")
                    #
                    # If matches not found check for reading exceptions
                    #
                    if len(kanji_rdng_entry['furigana_matches']) == 0:
                        exceptions = _get_reading_exceptions(kanji_dict, kanji_word['txt'], rdng['txt'])
                        if len(exceptions) > 0:
                            kanji_rdng_entry['furigana_matches']. append(exceptions)

                    #
                    # If matches not found check for readings with nanori (name readings)
                    #
                    if len(kanji_rdng_entry['furigana_matches']) == 0:
                        _make_possible_furigana_list2(kanji_dict, kanji_word['txt'], rdng['txt'],
                                                  kanji_rdng_entry['furigana_matches'], [], "", nanori=True)
                        if len(kanji_rdng_entry['furigana_matches']) > 0:
                            kanji_rdng_entry['nanori'] = True

    progress.complete()


def print_name_list_feedback(name_list):
    count = 0
    for name in name_list:
        if len(name['furigana_matches']) == 0:
            count += 1
    print ("Name List: {0} of {1} names don't have character/reading matches.".format(count, len(name_list)))

def print_character_feedback(jmdict_list, kanji_dict):
    #
    # print # of characters with words that don't have a match for kanji_reading pair
    # print # of characters with with multiple matches per kanji_reading pair
    #
    no_match_count = 0
    extra_matches_count = 0
    no_match_list = []
    extra_matches_list = []
    no_words_count = 0
    kanji_total = len(list(kanji_dict.keys()))
    for kanji in kanji_dict.values():
        no_match_counded = False
        extra_matches_counted = False
        if kanji.get('words') is None:
            no_words_count += 1
            continue
        for entry in kanji['words']:
            for kanji_rdng in entry['kanji_word']['kanji_rdng']:
                if len(kanji_rdng['furigana_matches']) == 0 and not no_match_counded:
                    if not _check_special_allowances(entry['entry']['sense'], entry['entry']['kanji'], entry['kanji_word'], kanji_rdng['rdng']):
                        no_match_counded = True
                        no_match_count += 1
                        no_match_list.append(kanji)
                if len(kanji_rdng['furigana_matches']) > 1 and not extra_matches_counted:
                    extra_matches_counted = True
                    extra_matches_count += 1
                    extra_matches_list.append(kanji)
    print("{0} of the {1} kanji don't have any example words.".format(no_words_count, kanji_total))
    print("{0} of the {1} kanji have words with no matches per kanji-reading pair.".format(no_match_count,kanji_total))
    print("{0} of the kanji have words with multiple matches per kanji-reading pair.".format(extra_matches_count))


    jmlist_count = 0
    no_match_word_list = []
    for entry in jmdict_list:
        jmlist_counted = False
        for kanji_word in entry['kanji']:
                for kanji_rdng in kanji_word['kanji_rdng']:
                    if len(kanji_rdng['furigana_matches']) == 0 and not jmlist_counted:
                        jmlist_counted = True
                        jmlist_count += 1
                        no_match_word_list.append(entry)
                    if jmlist_counted:
                        break
                if jmlist_counted:
                    break
    print("{0} words have no matches per at least one kanji-reading pair.".format(jmlist_count))
    jmlist_count = 0
    rdng_markers = ['ok','oik','ik','gikun']
    kanji_markers = ['ateji','iK','ik','io','oK']
    for entry in no_match_word_list:
        jmlist_counted = False
        count_this_entry = False
        for kanji_word in entry['kanji']:
                for kanji_rdng in kanji_word['kanji_rdng']:
                    if len(kanji_rdng['furigana_matches']) == 0:
                        if _check_special_allowances(entry['sense'], entry['kanji'], kanji_word, kanji_rdng['rdng']):
                            count_this_entry = True

        if count_this_entry:
            jmlist_count += 1

    print("Of those words, {0} have a special allowance."
          "\n\tThe word has kanji reading matches on other kanji"
          "\n\tThe word has special markers on the kanji (old kanji, irregular kanji, ateji, etc)"
          "\n\tThe word has special markers on the rdng (old kana, irregular kana, gikun, etc)"
          "\n\tThe word is usually written as kana only".format(jmlist_count))
    return {'no_match_list':no_match_list, 'extra_matches_list':extra_matches_list}

def _check_special_allowances(sense_list, kanji_list, kanji, rdng):
    for sense in sense_list:
            if 'uk' in sense['misc']:
                return True
    if len(rdng['misc']) > 0:
        return True
    if len(kanji['misc']) > 0:
        return True
    for chr in kanji['txt']:
        if not is_asian(chr):
            return True
        if is_full_digit(chr) or is_full_letter(chr) or is_full_punct(chr):
            return True
    for kanji in kanji_list:
        for kanji_rdng in kanji['kanji_rdng']:
            if len(kanji_rdng['furigana_matches']) > 0:
                return True
    return False

def evalute_no_match_pairs(jmdict_list, kanji_dict):
    lines_to_print = []
    max_lines = 28
    user_input = ''
    for entry in jmdict_list:
        lines_to_print = []
        for kanji in entry['kanji']:
            no_match = False
            line_batch = []
            for kanji_rdng in kanji['kanji_rdng']:
                if len(kanji_rdng['furigana_matches']) == 0:
                    if not _check_special_allowances(entry['sense'], entry['kanji'], kanji, kanji_rdng['rdng']):
                        line = entry['id'] + " " + kanji['txt']
                        line += " "
                        line += kanji_rdng['rdng']['txt']
                        line_batch.append(line)
                        no_match = True
            if no_match:
                char_list = []
                for char in kanji['txt']:
                    if char not in char_list and kanji_dict.get(char) is not None:
                        char_list.append(char)
                        line_batch.append("    " + kanji_to_string(kanji_dict.get(char)))
                    if kanji_dict.get(char) is None and is_kanji(char):
                        line_batch = []
            if len(line_batch) + len(lines_to_print) > max_lines:
                user_input = user_interaction(lines_to_print)
                lines_to_print = line_batch
            else:
                lines_to_print += line_batch
            if user_input.lower() == 'n':
               break
        user_interaction(lines_to_print)
        user_input = ''


def user_interaction(lines_to_print):
    for line in lines_to_print:
        print(line)
    user_input = ''
    if len(lines_to_print) > 0:
        user_input = input("Press enter to continue or 'N' for next")
    return user_input


def kanji_to_string(kanji):
    output = kanji['txt']
    types = ['rdng_on','rdng_kun']
    output += "  "
    for detail in kanji['details']:
        if detail['type'] in types:
            output += " "
            output += detail['txt']
    return output
