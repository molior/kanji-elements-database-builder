# JMDICT License
# This package uses the JMDict dictionary files. These files are the property of the
#  Electronic Dictionary Research and Development Group, and are used in conformance with the Group's licence.

from jmdict_tools import does_kanji_word_have_furigana, does_kanji_have_furigana, does_kanji_have_ime, \
    does_kanji_have_duplicate, is_entry_usually_kana, calculate_kanji_word_importantance, calculate_sentence_importance_score,\
    calculate_rdng_word_importance

__author__ = 'Brian'

import time
import csv
from xml.etree.cElementTree import iterparse

from wchartype import is_katakana, is_kanji
from progressbar import progress_bar
from FuriganaGenerator import convert2hira, generate_furigana_per_kanji
from corpus_tools import is_function_word, get_gloss_list
from nltk.stem import WordNetLemmatizer

_jmdict_xml = "JMDict_e"
_data_location = "Data Files/"
_kw_names = ['kwfld.csv', 'kwdial.csv',
             'kwlang.csv', 'kwmisc.csv', 'kwpos.csv', 'kwkinf.csv',
             'kwrinf.csv', 'kwxref.csv']
_kanji_pri_comparison = {"news1", "ichi1", "spec1", "spec2", "gai1"}



def _make_freq_entry():
    return {'blogs':1, 'news':1, 'novel':1, 'mainichi':1, 'tanaka':0, 'tanaka_total':0}
def _make_freq_entry_zero():
    return {'blogs':0, 'news':0, 'novel':0, 'mainichi':0, 'tanaka':0, 'tanaka_total':0}

def make_freq_dictionary():
    #{'mainichi': 3711420, 'news': 4378450, 'tanaka': 0, 'blogs': 33864266, 'tanaka_total': 0, 'novel': 3618313}
    freq_dict = dict()
    freq_files = {'freq_blogs.txt':
                       {'name': 'blogs',
                        'encoding': 'UTF-8',
                        'norm': 200000,
                        # 'norm':[8,32,128,512,2048,8192,32768],
                        'condition_func': lambda row: int(row[len(row)-2][3:]) > 0,
                        'delim': '/',
                        'word_func': lambda row: row[0].split()[0],
                        'value_func': lambda row: int(row[len(row)-2][3:])},
                   'freq_newspapers.txt':
                       {'name': 'news',
                        'encoding': 'UTF-8',
                        'norm': 4000, #[6,18,54,162,486,1458,4374],
                        'condition_func': lambda row: int(row[1]) > 0,
                        'delim': '\t',
                        'word_func': lambda row: row[4].split(sep='|')[0],
                        'value_func': lambda row: int(row[1])},
                    'freq_novels.txt':
                       {'name': 'novel',
                        'encoding': 'UTF-8-sig',
                        'norm': 2700, #[6,18,54,162,486,1458,4374],
                        'condition_func': lambda row: (row[2] != '記号' and row[0] is not None),
                        'delim': '\t',
                        'word_func': lambda row: str(row[1]),
                        'value_func': lambda row: int(row[0])},
                    'wordfreq_ck_unicode.txt':
                       {'name': 'mainichi',
                        'encoding': 'UTF-8-sig',
                        'norm': 2000, #[2,6,18,54,162,486,1458],
                        'condition_func': lambda row: len(row) > 1,
                        'delim': '\t',
                        'word_func': lambda row: row[0],
                        'value_func': lambda row: int(row[1])}}



    progress = progress_bar(559832, "making frequency dictionary")
    for key, tool in freq_files.items():
        with open(_data_location + key, encoding=tool['encoding']) as f:
            reader = csv.reader(f, delimiter = tool['delim'])
            for row in reader:
                progress.inc_counter()
                if tool['condition_func'](row):
                    if freq_dict.get(tool['word_func'](row)) is None:
                        freq_dict_data = _make_freq_entry()
                        freq_dict_data[tool['name']] = _get_norm(tool['norm'],tool['value_func'](row))
                        freq_dict.update({tool['word_func'](row):freq_dict_data})
                    else:
                        freq_dict[tool['word_func'](row)][tool['name']] = _get_norm(tool['norm'],tool['value_func'](row))
    progress.complete()
    return freq_dict

def _get_norm(norm_list, value):
    # if value == 0:
    #     return 1
    # if value <= norm_list[0]:
    #     return 2
    # for idx, norm_value in enumerate(norm_list):
    #     if norm_value > value:
    #         return idx+3
    # return len(norm_list) + 3
    base_score = 3
    max_score = 10
    return min(base_score + (max_score - base_score)*value/norm_list, max_score)

def _make_jlpt_word_list():
    file_name = 'jlpt-n*.csv'
    file_count = 5
    entry_dict = {}

    count = 0
    while count < 5:
        count +=1
        with open(_data_location+file_name.replace("*",str(count)), encoding='utf-8') as f:
            line = f.readline()
            while line != '':
                entry_id = line.strip("\n")
                if line.strip("\n") not in entry_dict:
                    entry_dict.update({entry_id:count})
                else:
                    entry_dict[entry_id] = count
                line = f.readline()
    return entry_dict

def _make_jouyou_list():
    file_name = 'jouyou_words.txt'
    jouyou_set = set()
    with open(_data_location+file_name, encoding='utf-8') as f:
        reader = csv.reader(f, delimiter='，')
        for row in reader:
            for word in row:
                jouyou_set.add(word)
    return jouyou_set


def _make_entry_dict():
    dic = {"id": 0,
           'jlpt': 0,
           "kanji": [],
           "rdng": [],
           "sense": [],
           "misc": list()}
    return dic


def _make_kanji(node, kw_lists):
    kanji_entry = {'id': int(),
                   'txt': str(),
                   'misc': [],
                   'pri': 0,
                   'freq': _make_freq_entry(),
                   'has_ime': False,
                   'is_joyo_example': False,
                   'kanji_rdng':[],
                   'eng_lemmas':set(),
                   'char_meaning_score':{},
                   'example_sent': [],
                   'is_example': False,
                   'used_sent': False}

    for entry in node:
        if entry.tag == "keb":
            kanji_entry['txt'] = entry.text
            # todo Add frequency file lookup
        elif entry.tag == "ke_pri":
            if entry.text in _kanji_pri_comparison:
                kanji_entry['pri'] = 1
        elif entry.tag == "ke_inf":
            kw_kinf = kw_lists.get("kwkinf")
            if kw_kinf is not None:
                row = kw_kinf.get(entry.text)
                if row is not None:
                    kanji_entry['misc'].append(row['kw'])
    return kanji_entry


def _make_rdng(node, kw_lists):
    rdng_entry = {'id': int(),
                  'txt': str(),
                  'no_kanji': False,
                  'kanji': [],
                  'misc': [],
                  'pri': 0,
                  'freq': _make_freq_entry(),
                  'has_ime': False,
                  'example_sent': [],
                  'is_example': False,
                  'used_sent': False}
    for entry in node:
        if entry.tag == "reb":
            rdng_entry['txt'] = entry.text
        elif entry.tag == "re_nokanji":
            rdng_entry['no_kanji'] = True
        elif entry.tag == "ke_pri":
            if entry.text in _kanji_pri_comparison:
                rdng_entry['pri'] = 1
        elif entry.tag == "re_restr":
            rdng_entry['kanji'].append(entry.text)
        elif entry.tag == "re_inf":
            kw_list = kw_lists.get("kwrinf")
            if kw_list is not None:
                row = kw_list.get(entry.text)
                if row is not None:
                    rdng_entry['misc'].append(row['kw'])
    return rdng_entry


def _make_sense(node, kw_lists):
    sense_entry = {'id':int(),
                   'gloss': [],
                   'pos': [],
                   'misc': [],
                   'rdng': [],
                   'kanji': [],
                   'info':[]}
    for entry in node:
        if entry.tag == "gloss":
            sense_entry['gloss'].append(entry.text)
        elif entry.tag == "stagk":
            sense_entry['kanji'].append(entry.text)
        elif entry.tag == "stagr":
            sense_entry['rdng'].append(entry.text)
        elif entry.tag == "pos":
            pos_list = kw_lists.get("kwpos")
            if pos_list is not None:
                row = pos_list.get(entry.text)
                if row is not None:
                    sense_entry['pos'].append(row['kw'])
        elif entry.tag == "misc":
            misc_list = kw_lists.get('kwmisc')
            if misc_list is not None:
                row = misc_list.get(entry.text)
                if row is not None:
                    sense_entry['misc'].append(row['kw'])
        elif entry.tag == "s_inf":
            sense_entry['info'].append(entry.text)
    return sense_entry


def _make_kw_lists(kw_list):
    print ("Processing key word list files...")
    output = {}
    for name in _kw_names:
        print ("\t"+ name)
        output_list = {}
        output.update({name.split(".")[0]: output_list})
        f = open(_data_location+name, encoding="UTF-8")
        reader = csv.reader(f, dialect='excel-tab')
        for row in reader:
            output_list.update({row[2]:{'id': row[0], 'kw': row[1]}})
    print ("Processed")
    return output


def make_list_from_xml():
    start = time.time()
    kw_lists = {'kwfld': [],
                'kwdial': [],
                'kwlang': [],
                'kwmisc': [],
                'kwpos': [],
                'kwkinf': [],
                'kwrinf': [],
                'kwxref': []}
    kw_lists = _make_kw_lists(kw_lists)
    freq_dict = make_freq_dictionary()
    jlpt_dict = _make_jlpt_word_list()
    joyo_list = _make_jouyou_list()
    f = open(_data_location+_jmdict_xml, encoding="UTF-8")

    # get context
    context = iterparse(f, events=("end",))
    context = iter(context)
    #
    # get root element
    #
    event, root = next(context)

    count = 0
    percentage_count = 0
    percentage_interval = 0.02
    total_entries = 173294
    progress = progress_bar(173294, "importing jmdict from xml")
    entry_list = []

    for event, elem in context:
        if elem.tag == "entry":
            entry_dict = _make_entry_dict()
            entry_list.append(entry_dict)
            progress.inc_counter()
            for entry in elem:
                if entry.tag == "ent_seq":
                    entry_dict['id'] = entry.text
                elif entry.tag == "k_ele":
                    entry_dict["kanji"].append(_make_kanji(entry, kw_lists))
                elif entry.tag == "r_ele":
                    entry_dict['rdng'].append(_make_rdng(entry, kw_lists))
                elif entry.tag == "sense":
                    entry_dict["sense"].append(_make_sense(entry, kw_lists))


            # add jlpt flag to entry
            if entry_dict['id'] in jlpt_dict:
                entry_dict['jlpt'] = jlpt_dict[entry_dict['id']]

            # add frequency and joyo flag values to kanji
            for kanji in entry_dict['kanji']:
                if freq_dict.get(kanji['txt']) is not None:
                    kanji['freq'] = freq_dict[kanji['txt']].copy()
                if kanji['txt'] in joyo_list:
                    kanji['is_joyo_example'] = True


            # collect readings if there are not kanji
            rdng_list = list()
            if len(entry_dict['kanji']) == 0:
                rdng_list.extend(entry_dict['rdng'])
            #collect rdng if entry is usually written in kana (uk)
            else:
                for sense in entry_dict['sense']:
                    if 'uk' in sense['misc']:
                        if len(sense['rdng']) == 0:
                            for rdng in entry_dict['rdng']:
                                if rdng not in rdng_list:
                                    rdng_list.append(rdng)
                            break
                        else:
                            for rdng_txt in sense['rdng']:
                                for rdng in entry_dict['rdng']:
                                    if rdng_txt == rdng['txt'] and rdng not in rdng_list:
                                        rdng_list.append(rdng)

            for rdng in rdng_list:
                if rdng['txt'] in freq_dict:
                    rdng['freq'] = freq_dict[rdng['txt']].copy()


        root.clear()

    progress.complete()
    return entry_list

def has_ime_match_furigana_list(furi_list, ime_dict):
    test_text = ''
    reading_text = ''
    for each in reversed(furi_list):
        if bool(each['char']):
            test_text += each['char']+each['okurigana']
            reading_text += each['txt']+each['okurigana']
        elif bool(test_text):
            if ime_dict.get(test_text) is None:
                return False
            if reading_text not in ime_dict[test_text]:
                return False
            test_text =''
            reading_text =''
    if not bool(test_text) or ime_dict.get(test_text) and reading_text in ime_dict.get(test_text):
        return True
    else:
        return False


def has_ime_adjust_for_grammar(kanji, ime_dict):
    for kanji_rdng in kanji['kanji_rdng']:
        result = False
        for each in kanji_rdng['furigana_matches']:
            result = has_ime_match_furigana_list( each, ime_dict)
            if result:
                break
        if result:
            kanji['has_ime'] = True
            kanji_rdng['rdng']['has_ime'] = True
    return kanji['has_ime']


def mark_jmdict_for_ime(jmdict, kanji_dict, ime_dict):
    generate_furigana_per_kanji(jmdict, kanji_dict)

    count_removed_rdng = 0
    count_changed_entries = 0
    count_found = 0
    count_no_rdng_match_found = 0
    count_reduced_kanji_entries = 0
    count_reduced_kanji = 0
    progress = progress_bar(len(jmdict), "Marking jmdict entries for ime...")
    for entry_dict in jmdict:
        progress.inc_counter()
        rdng_list = list()
        new_rdng_list = list()
        found_rdng_count = 0
        for kanji in entry_dict['kanji']:
            ime_rdngs  = ime_dict.get(kanji['txt'])
            has_rdng = False
            if ime_rdngs is None:
                has_rdng = has_ime_adjust_for_grammar(kanji, ime_dict)

            if ime_rdngs is not None:
                for rdng in entry_dict['rdng']:
                    if len(rdng['kanji']) > 0 and kanji['txt'] in rdng['kanji'] or len(rdng['kanji']) == 0:
                        if convert2hira(rdng['txt']) in ime_rdngs:
                            has_rdng = True
                            rdng['has_ime'] = True
                            if rdng not in new_rdng_list:
                                new_rdng_list.append(rdng)
            if has_rdng:
                found_rdng_count += 1
            kanji['has_ime'] = has_rdng

        if found_rdng_count != 0 and found_rdng_count < len(entry_dict['kanji']):
            count_reduced_kanji_entries += 1
            count_reduced_kanji += len(entry_dict['kanji']) - found_rdng_count

        for rdng in entry_dict['rdng']:
            if is_katakana(rdng['txt'][0]) and rdng not in new_rdng_list:
                rdng['has_ime'] = True
                new_rdng_list.append(rdng)

        len_new_rdng = len(new_rdng_list)
        len_rdng_list = len(entry_dict['rdng'])
        if len_new_rdng != 0:
            count_found += 1
        if len_new_rdng != 0 and len_new_rdng < len_rdng_list:
            count_changed_entries += 1
            count_removed_rdng += len_rdng_list - len_new_rdng
        if len_new_rdng == 0 and len(entry_dict['kanji']) > 0:
            count_no_rdng_match_found += 1

    progress.complete()

    # make dictionary from jmdict kanji entries
    # only include entries that have ime for both kanji and reading
    jmdict_dict = dict()
    for entry in jmdict:
        for kanji in entry['kanji']:
            if kanji['has_ime']:
                if jmdict_dict.get(kanji['txt']) is None:
                    jmdict_dict.update({kanji['txt']:dict()})
                for kanji_rdng in kanji['kanji_rdng']:
                    if kanji_rdng['rdng']['has_ime']:
                        rdng_txt = kanji_rdng['rdng']['txt']
                        if jmdict_dict[kanji['txt']].get(rdng_txt) is None:
                            jmdict_dict[kanji['txt']].update({rdng_txt: [(entry, kanji)]})
                        else:
                            jmdict_dict[kanji['txt']][rdng_txt].append((entry,kanji))

    # Increase number of entries with ime using dictionary to break down compound entries
    # Check only entries that do not have ime on any kanji word
    for entry in jmdict:
        if not does_kanji_have_ime(entry['kanji']):
            for kanji in entry['kanji']:
                for kanji_rdng in kanji['kanji_rdng']:
                    for match in kanji_rdng['furigana_matches']:
                        result = dictionary_breakdown(list(reversed(match)), jmdict_dict)
                        if result > 0:
                            kanji['has_ime'] = True
                            kanji_rdng['rdng']['has_ime'] = True


    print("Found {0} of {1} jmdict entries in IME.".format(count_found, len(jmdict)))
    print("{0} entries with kanji could not be found.".format(count_no_rdng_match_found))
    print("Reduced kanji on {0} entries and marked {1} kanji has missing ime.".format(count_reduced_kanji_entries, count_reduced_kanji))
    print("Reduced readings on {0} entries and removed {1} readings.".format(count_changed_entries, count_removed_rdng))

def add_ids_all_dicts(jmdict_list):
    for entry in jmdict_list:
        sorted_kanji = sorted(entry['kanji'], key = lambda x:calculate_kanji_word_importantance(x, entry), reverse=True)
        idx = 1
        for kanji in sorted_kanji:
            kanji['id'] = idx
            idx += 1

        sorted_rdng = sorted(entry['rdng'], key = lambda x: calculate_rdng_word_importance(x, entry), reverse= True)
        idx = 1
        for rdng in sorted_rdng:
            rdng['id'] = idx
            idx +=1
        idx =1
        for sense in entry['sense']:
            sense['id'] = idx
            idx += 1

def add_char_meaning_score(kanji_dict, jmdict_list, jpn_lemma_dict, thesaurus_dict):
    pos_types = ['a','v','n','r']
    thesaurus_idx = 1
    meaning_link_idx = 0
    extra_meanings_idx = 2
    extra_thes_idx = 3
    thesaurus_modifier = 0.75
    wn_lem = WordNetLemmatizer()
    progress = progress_bar(len(kanji_dict), "creating extending meanings for kanji characters.")
    for char, values in kanji_dict.items():
        progress.inc_counter()
        values.update({'meaning_sets':dict()})
        for meaning in [meanings for meanings in values['details'] if meanings['type'] == 'meaning']:
            extra_meanings = set()
            extra_thes = set()
            # lemmatize meanings to expand available meanings
            for pos in pos_types:
                lemma = wn_lem.lemmatize(meaning['txt'].lower(), pos)
                if lemma != meaning['txt']:
                    extra_meanings.add(lemma)
                    extra_thes.update(thesaurus_dict.get(lemma, set()))
            values['meaning_sets'].update({meaning['txt'].lower(): (meaning, set(thesaurus_dict.get(meaning['txt'], set())), extra_meanings, extra_thes)})
    progress.complete()


    progress = progress_bar(len(jmdict_list), "adding meaning score to jmdict kanji entries.")
    for entry in jmdict_list:
        progress.inc_counter()
        for kanji in entry['kanji']:
            eng_lemmas = jpn_lemma_dict.get(kanji['txt'], set())
            primary_gloss_set = set()
            for sense in entry['sense']:
                primary_gloss_set.update(sense['gloss'])
            tokenized_gloss_list = get_gloss_list(entry,kanji['txt'])
            importance_score = calculate_kanji_word_importantance(kanji, entry)
            kanji_count = len(set([char for char in kanji['txt'] if is_kanji(char)]))
            for char in kanji['txt']:
                kanji_char = kanji_dict.get(char)
                base_score = 10
                best_score = 0

                if kanji_char is not None and char not in kanji['char_meaning_score']:
                    for meaning, meaning_tuple in kanji_char['meaning_sets'].items():
                        score = 0
                        # if direct match found in lemmas then continue to next
                        # if meaning in eng_lemmas or meaning in primary_gloss_set:
                        #     # add frequency score to meaning (for sorting meanings later
                        #     meaning_tuple[meaning_link_idx]['freq'] += importance_score
                        #     best_score = base_score
                        #     continue
                        #
                        # if eng_lemmas.intersection(meaning_tuple[thesaurus_idx]) \
                        #         or primary_gloss_set.intersection(meaning_tuple[thesaurus_idx]):
                        #     meaning_tuple[meaning_link_idx]['freq'] += importance_score * thesaurus_modifier
                        #     best_score = base_score * thesaurus_modifier
                        #     continue

                        # get best score from tokenized gloss list
                        for gloss in tokenized_gloss_list:
                            int_score = len(set(meaning.split(" ")).intersection(set(gloss)))
                            int_score = max([0, 1.5 * int_score/max(len(gloss), len(meaning.split(" "))),
                                             len(set(gloss).intersection(meaning_tuple[thesaurus_idx]))/len(gloss)* thesaurus_modifier
                                             ])
                            int_score = 1 if int_score > 1 else int_score
                            if score < base_score * int_score * 0.8:
                                score = base_score * int_score * 0.8

                        # score = max([score]+[int(not eng_lemmas.isdisjoint(thes_set))*7.5 for thes_set in meaning_tuple[thesaurus]])

                        score = max([score,             # tokenized gloss list score
                                     int(meaning in eng_lemmas.union(primary_gloss_set)) * base_score,       # compare meaning to lemmas and primary gloss set
                                     int(not meaning_tuple[extra_meanings_idx].isdisjoint(eng_lemmas.union(primary_gloss_set))) * base_score,
                                     int(not eng_lemmas.union(primary_gloss_set).isdisjoint(meaning_tuple[thesaurus_idx]))*base_score*thesaurus_modifier,   # compare thesaurus entries to lemmas and primary gloss set
                                     int(not eng_lemmas.union(primary_gloss_set).isdisjoint(meaning_tuple[extra_thes_idx]))*base_score*thesaurus_modifier
                                     ]
                                    )

                        meaning_tuple[meaning_link_idx]['freq'] += round((score/base_score * importance_score)/kanji_count)
                        best_score = max([best_score, score])
                    kanji['char_meaning_score'].update({char:best_score})
    progress.complete()

    progress = progress_bar(len(kanji_dict), "removing extending meanings from kanji charactesr.")
    for char, values in kanji_dict.items():
        progress.inc_counter()
        values.pop('meaning_sets')
    progress.complete()


def get_definitions(entry):
    return

def dictionary_breakdown(furigana_list, jmdict_dict):
    i = len(furigana_list)
    while i > 0:
        try_word = "".join([x['char'] + x['okurigana'] for x in furigana_list[:i]])
        if jmdict_dict.get(try_word) is not None:
            try_reading = "".join([x['txt'] + x['okurigana'] for x in furigana_list[:i]])
            if jmdict_dict[try_word].get(try_reading) is not None:
                if i == len(furigana_list):
                    return 1
                else:
                    result = dictionary_breakdown(furigana_list[i:], jmdict_dict)
                    if result != -1:
                        return 1 + result
        i -= 1
    return -1


def output_no_ime_list(jmdict_list):

    jmdict_dict = dict()
    jmdict_dict = create_dict_from_jmdict_list(jmdict_list, needs_furigana=False, needs_ime=True)
    # for entry in jmdict_list:
    #     for kanji in entry['kanji']:
    #         if kanji['has_ime']:
    #             if jmdict_dict.get(kanji['txt']) is None:
    #                 jmdict_dict.update({kanji['txt']:[entry]})
    #             else:
    #                 jmdict_dict[kanji['txt']].append(entry)
    no_ime = list()
    progress = progress_bar(len(jmdict_list), "finding entries with no ime")
    for entry in jmdict_list:
        progress.inc_counter()
        if not is_entry_usually_kana(entry) \
                and len(entry['kanji']) > 0 \
                and not does_kanji_have_ime(entry['kanji']) \
                and does_kanji_have_furigana(entry['kanji']) \
                and not does_kanji_have_duplicate(entry['kanji'], jmdict_dict):
            freq = _make_freq_entry()
            no_ime_entry = {'kanji': str(), 'rdng': str(), 'freq': freq, 'meaning': str()}
            for kanji in entry['kanji']:
                no_ime_entry['kanji'] = kanji['txt'] if len(no_ime_entry['kanji']) > 0 else ', ' + kanji['txt']
                for key, value in kanji['freq'].items():
                    freq[key] += value
            for key in freq.keys():
                if len(entry['kanji']) > 0:
                    freq[key] /= len(entry['kanji'])
            no_ime_entry['rdng'] = ', '.join([x['txt'] for x in entry['rdng']])
            no_ime_entry['meaning'] = ', '.join(entry['sense'][0]['gloss'])
            no_ime.append(no_ime_entry)
    progress.complete()
    no_ime = sorted(no_ime,
                    key=lambda x: (x['freq']['mainichi']*0.3 + x['freq']['novel'] *0.3 +
                                   x['freq']['news']*0.2 + x['freq']['blogs']*0.2),
                    reverse=True)
    progress = progress_bar(len(no_ime), "creating output file: no_ime.txt")
    with open("Data Files/no_ime.txt", mode = 'w', encoding = 'utf-8-sig', newline='') as f:
        fieldnames = ['kanji_txt','rdng_txt', 'meaning', 'freq']
        writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t')
        writer.writeheader()
        for each in no_ime:
            progress.inc_counter()
            catch = writer.writerow({'kanji_txt':each['kanji'].strip(', '),
                                     'rdng_txt':each['rdng'],
                                     'meaning': each['meaning'],
                                     'freq': each['freq']['mainichi']*0.3 + each['freq']['novel'] *0.3 +
                                             each['freq']['news']*0.2 + each['freq']['blogs']*0.2})


    progress.complete()
    print(len(no_ime))
    return no_ime



def output_no_furigana_matches(jmdict_list, ime_dict):
    no_furigana_list = list()
    progress = progress_bar(len(jmdict_list), "creating output file: no_furigana_matches.txt")
    jmdict = create_dict_from_jmdict_list(jmdict_list)

    # for entry in jmdict_list:
    #     if does_kanji_have_furigana(entry['kanji']):
    #         for kanji in entry['kanji']:
    #             if kanji['has_ime']:
    #                 if jmdict.get(kanji['txt']) is None:
    #                     jmdict.update({kanji['txt']:[kanji]})
    #                 else:
    #                     jmdict[kanji['txt']].append(kanji)

    for entry in jmdict_list:
        meaning = ', '.join(entry['sense'][0]['gloss'])
        if does_kanji_have_furigana(entry['kanji']) or is_entry_usually_kana(entry):
            continue
        for kanji in entry['kanji']:
            for kanji_rdng in kanji['kanji_rdng']:
                ime_dict_result = ime_dict.get(kanji['txt'])
                if jmdict.get(kanji['txt']) is None \
                        and not bool(kanji_rdng['furigana_matches']) \
                        and ime_dict_result is not None\
                        and kanji_rdng['rdng']['txt'] in ime_dict_result:
                    output_entry = {'kanji': str(), 'reading': str(), 'meaning': str(), 'freq': 0}
                    output_entry['kanji'] = kanji['txt']
                    output_entry['reading'] = kanji_rdng['rdng']['txt']
                    output_entry['meaning'] = meaning
                    output_entry['freq'] = kanji['freq']
                    no_furigana_list.append(output_entry)

    no_furigana_list = sorted(no_furigana_list,
                              key=lambda x: (x['freq']['mainichi']*0.3 + x['freq']['novel'] *0.3 +
                                   x['freq']['news']*0.2 + x['freq']['blogs']*0.2),
                              reverse=True)
    with open("Data Files/no_furigana_match.txt", mode = "w", encoding='utf-8-sig', newline='') as f:
        fieldnames = ['kanji_txt','rdng_txt', 'meaning', 'freq']
        writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t')
        writer.writeheader()
        for each in no_furigana_list:
            catch = writer.writerow({'kanji_txt':each['kanji'].strip(', '),
                                     'rdng_txt':each['reading'],
                                     'meaning': each['meaning'],
                                     'freq': each['freq']['mainichi']*0.3 + each['freq']['novel'] *0.3 +
                                             each['freq']['news']*0.2 + each['freq']['blogs']*0.2})

def output_words_with_duplicate_spelling(jmdict_list):


    jmdict_dict = create_dict_from_jmdict_list(jmdict_list, False, False)

    file = open("Data Files/dup_words.txt", mode="w", encoding='utf-8-sig', newline='')
    fieldnames = ['kanji', 'tanaka_count', 'has_ime', 'has_furi', 'is_uk', 'score', 'rdng(s)', 'meaning']
    writer = csv.DictWriter(file, fieldnames=fieldnames, delimiter='\t')
    writer.writeheader()
    count = 0
    count_all = 0
    progress = progress_bar(len(jmdict_dict), "creating output file: dup_words.txt")
    dup_list = dict()
    for key, value in jmdict_dict.items():
        progress.inc_counter()
        if len(value) > 1:
            count_all += 1
            # if duplicate exists check if clear leader exists
            # leaders high freq - tanaka count - 10, has ime - 1, has primary - 1, not usually kana - 1
            # get total tanaka count for key
            total_tanaka = 0
            for each in value:
                total_tanaka += each[0]['freq']['tanaka']

            #print first row
            output = sorted(value, key = lambda x: _calc_duplicate_score(x[0], x[1],total_tanaka), reverse=True)
            if _calc_duplicate_score(output[0][0],output[0][1], total_tanaka) != _calc_duplicate_score(output[1][0],output[1][1], total_tanaka):
                continue
            count += 1
            # writer.writerow({'kanji': key,
            #                  'tanaka_count': output[0][0]['freq']['tanaka'],
            #                  'has_ime': output[0][0]['has_ime'],
            #                  'has_furi': does_kanji_word_have_furigana(output[0][0]),
            #                  'is_uk': is_entry_usually_kana(output[0][1]),
            #                  'rdng(s)': make_rdngs_from_kanji(output[0][0]),
            #                  'meaning': make_simple_meaning_from_entry(output[0][1])})
            #print subsequent rows without kanji
            if dup_list.get(key) is None:
                dup_list.update({key:value})

            for each in output:
                writer.writerow({'kanji':key,
                                 'tanaka_count': each[0]['freq']['tanaka'],
                                 'has_ime': each[0]['has_ime'],
                                 'has_furi': does_kanji_word_have_furigana(each[0]),
                                 'is_uk': is_entry_usually_kana(each[1]),
                                 'score': _calc_duplicate_score(each[0],each[1], total_tanaka),
                                 'rdng(s)': make_rdngs_from_kanji(each[0]),
                                 'meaning': make_simple_meaning_from_entry(each[1])})
    progress.complete()
    print("Total duplicates {0}. Unsortable: {1}".format(count_all, count))
    return dup_list

def output_smallword_ingloss(jmdict_list):
    gloss_dict = dict()
    for entry in jmdict_list:
        for sense in entry['sense']:
            for gloss in sense['gloss']:
                if len(gloss.split(" ")) > 1:
                    for each in gloss.split(" "):
                        if len(each) < 4 and gloss_dict.get(each) is None:
                            gloss_dict.update({each:''})
    sorted_gloss_list = sorted(gloss_dict.keys(), key= lambda x: (len(x), x))
    with open("Data Files/smallword_ingloss.txt", mode="w", encoding='utf-8-sig') as f:
        f.writelines("%s\n" % l for l in sorted_gloss_list)


def output_multiword_gloss(jmdict_list):
    gloss_dict = dict()
    progress = progress_bar(len(jmdict_list), "creating output file multiword_gloss.txt")
    for entry in jmdict_list:
        progress.inc_counter()
        for sense in entry['sense']:
            for gloss in sense['gloss']:
                if len(gloss.split(" ")) > 1:
                    tokens = gloss.split(" ") #word_tokenize(gloss)
                    remaining_tokens = list()
                    for token in tokens:
                        #test_token = token.strip(string.punctuation).strip(string.digits)
                        if token.isalpha() and not is_function_word(token):
                            remaining_tokens.append(token)
                    if len(remaining_tokens) == 0:
                        remaining_tokens = tokens
                    if gloss_dict.get(" ".join(remaining_tokens)) is None:
                        gloss_dict.update({' '.join(remaining_tokens):''})
    sorted_gloss_list = sorted(gloss_dict.keys(), key= lambda x: (x, len(x)))

    with open(_data_location+"multiword_gloss.txt", mode="w", encoding='utf-8-sig') as f:
        f.writelines("%s\n" % l for l in sorted_gloss_list)
    progress.complete()


def _calc_duplicate_score(kanji, entry, total_tanaka):
    if total_tanaka == 0:
        tanaka_freq = 0
    else:
        tanaka_freq = kanji['freq']['tanaka']/total_tanaka*20
    return kanji['has_ime']*10 \
           + tanaka_freq + kanji['pri']*10 \
           + int(does_kanji_word_have_furigana(kanji))*10 + int(not is_entry_usually_kana(entry))*10

def create_dict_from_jmdict_list(jmdict_list, needs_furigana = True, needs_ime = True, include_readings = False):
    jmdict = dict()
    for entry in jmdict_list:
        furigana_check = does_kanji_have_furigana(entry['kanji']) if needs_furigana else True
        if furigana_check:
            for kanji in entry['kanji']:
                has_ime_check = kanji['has_ime'] if needs_ime else True
                if has_ime_check:
                    if jmdict.get(kanji['txt']) is None:
                        jmdict.update({kanji['txt']:[(kanji, entry)]})
                    else:
                        jmdict[kanji['txt']].append((kanji, entry))
        if include_readings and (is_entry_usually_kana(entry) or len(entry['kanji']) == 0):
            for rdng in entry['rdng']:
                if jmdict.get(rdng['txt']) is None:
                    jmdict.update({rdng['txt']:[(rdng,entry)]})
                else:
                    jmdict[rdng['txt']].append((rdng,entry))

    return jmdict

def add_sentences_to_entries(jmdict_list, ex_sent):
    progress = progress_bar(len(jmdict_list), 'adding example sentences to entries')
    for entry in jmdict_list:
        for kanji in entry['kanji']:
            kanji.update({'example_sent':[]})

    kanji_idx = 1
    for entry in ex_sent:
        for word in entry['words']:
            if word['kanji_link'] is not None:
                word['kanji_link'][kanji_idx]['example_sent'].append(entry)

    for entry in jmdict_list:
        progress.inc_counter()
        for kanji in entry['kanji']:
            kanji['ex_sent'] = sorted(kanji['example_sent'],
                                      key=lambda x: calculate_sentence_importance_score(x, kanji['txt']),
                                      reverse=True)
    progress.complete()



def make_set_entry_ids(jmdict_list, kanji_dict):
    #create entry table, kanji table, reading table, sense table, and sense_gloss table
    # if ex_sent is provide make sentence and sent_word tables
    # update kanji and rdng with mark for example
    # collect set of entry ids to be included
    jmdict_entry_ids = set()
    for entry in kanji_dict.values():
        for each in entry.get('examples', []):
            jmdict_entry_ids.add(each[0]['id'])
            each[1].update({'is_example': True})
            each[2]['rdng'].update({'is_example':True})
    return jmdict_entry_ids

def make_set_entry_sentence_ids(jmdict_list, selected_entry_ids):
    jmdict_entry_ids = set()
    sentence_id_set = set()
    selected_ex_sent_list = list()
    for entry in jmdict_list:
        if entry['id'] in selected_entry_ids:
            for kanji in entry['kanji']:
                # if kanji.get('is_example', False):
                if len(kanji.get('example_sent',[])) > 0:
                    selected_ex_sent_list.append(kanji['example_sent'][0])
    for sent in selected_ex_sent_list:
        sentence_id_set.add(sent['id'])
        for word in sent['words']:
            jmdict_entry_ids.add(word['id'])
            if word['kanji_link'] is not None:
                word['kanji_link'][1].update({'used_sent': True})
                word['kanji_link'][2]['rdng'].update({'used_sent': True})
            if word['rdng_link'] is not None:
                word['rdng_link'][1].update({'used_sent': True})
    return jmdict_entry_ids, sentence_id_set

def make_set_entry_ids_jlpt(jmdict_list):
    jmdict_entry_ids = set()
    for entry in jmdict_list:
        if entry['jlpt'] > 0:
            jmdict_entry_ids.add(entry['id'])
    return jmdict_entry_ids

def make_set_entry_ids_has_ime(jmdict_list):
    jmdict_entry_ids = set()
    for entry in jmdict_list:
        if does_kanji_have_ime(entry['kanji']):
            jmdict_entry_ids.add(entry['id'])
    return jmdict_entry_ids

def make_set_entry_ids_has_freq(jmdict_list):
    jmdict_entry_ids = set()
    return jmdict_entry_ids


def make_simple_meaning_from_entry(entry):
    return ", ".join(entry['sense'][0]['gloss'])

def make_rdngs_from_kanji(kanji):
    return ", ".join([rdng['rdng']['txt'] for rdng in kanji['kanji_rdng']])



