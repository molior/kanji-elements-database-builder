import collections
import sys
import time
import re
from romkan import to_hiragana, to_katakana, to_roma
from xml.etree.cElementTree import iterparse
import csv
from FuriganaGenerator import create_rdngs, make_furigana_list
from wchartype import is_kanji
from kanjivg_list import get_radicals, get_groups, get_svg_groups_colors_radicals
from radical_parser import manual_adjust_radical_kanjivg, find_radical_kanjivg
from progressbar import progress_bar
from FuriganaGenerator import create_readings, convert2hira
from jmdict_tools import calculate_kanji_word_importantance
from NameDictParser import calculate_person_score, calculate_location_score
from wiki_name_scraper import get_wiki_people
from NameDictParser import dump_download_name_data

# Creates a database of kanji from kanjidic2.xml
# Currently only imports JIS208 standard kanji



_xml_location = "Data Files/kanjidic2.xml"
_unicode_xml_location = "Data Files/ucd.unihan.flat.xml"
_special_rdngs_location = "Data Files/kanji_reading_exceptions.txt"
_special_rdngs_2_location = "Data Files/kanji_reading_excp_2.xml"

# def kanji_dict_parse(kanji_db_location):
#     base = declarative_base()
#
#
#     engine = create_engine(kanji_db_location, echo=False)
#
#     kanji_table = Table('kanji', base,
#                         Column('_id', Integer, primary_key=True),
#                         Column('txt', String),
#                         Column('heisig', Integer),
#                         Column('hesig6', Integer),
#                         Column('stroke_count', Integer),
#                         Column('bushu', String),
#                         Column('rdng_on', String),
#                         Column('rdng_kun', String),
#                         Column('meanings', String))
#
#     details_table = Table('kanji_details', base,
#                           Column('_id', Integer, primary_key=True),
#                           Column('kanji_id', Integer, ForeignKey('kanji._id')),
#                           Column('txt', String),
#                           Column('freq', Integer),
#                           Column('type', String))
#
#     engine.execute("DROP TABLE IF EXISTS " + kanji_table.name)
#     engine.execute("DROP TABLE IF EXISTS " + details_table.name)
#
#     base.metadata.create_all(engine)
#
#     kanji_dict = make_dict_from_xml()


def _make_detail_entry(text, entry_type, appended = False, idx = 0):
    #
    # Valid types: rdng_kun, rdng_on, rdng_nanori, meaning
    #
    detail_entry = {"id": idx,
                    "txt": text,
                    "jy_status": False,
                    "type": entry_type,
                    "appended":appended,
                    "freq": 0,
                    "name_freq": 0,
                    "kanji_link": []}
    return detail_entry


def _make_char_entry():
    char_entry = {"txt": "",
                  "id": 0,
                  "freq": 0,
                  "stroke_count": 0,
                  "heisig": 0,
                  "heisig6": 0,
                  "jlpt": 0,
                  "details": [],
                  "grade": 0,
                  "variant": []}
    return char_entry

def _condense_readings(char_entry):
    #rdng_list = create_rdngs(char_entry['details'], inc_originals=False)
    #rdng_list_txt = []
    #for each in rdng_list:
    #    rdng_list_txt.append(each['txt'])

    sort_dict = {'rdng_kun':2,
                 'rdng_on':2,
                 'rdng_nanori':1,
                 'meaning':0,
                 'rdng_excp':0}

    sorted_details = sorted(char_entry['details'], key = lambda x: (sort_dict[x['type']],len(x['txt']), _reading_score(x['txt'])), reverse = True)
    keep_list = []
    reading_dict = dict()
    for detail in sorted_details:
        if detail['type'] in ['meaning', 'rdng_excp']:
            keep_list.append(detail)
            continue
        detail['txt'] = detail['txt'].strip('-')
        furigana = convert2hira(detail['txt'].split('.')[0])
        okurigana = '' if len(detail['txt'].split('.')) == 1 else detail['txt'].split('.')[1]
        if not bool(reading_dict.get(furigana)):
            reading_dict.update({furigana:[okurigana]})
            keep_list.append(detail)
        else:
            if _equiv_not_in_list(okurigana, reading_dict[furigana]):
                reading_dict[furigana].append(okurigana)
                keep_list.append(detail)
    char_entry['details'] = keep_list

def _reading_score(rdng):
    if len(rdng) > 0 and to_roma(rdng)[-1] == 'u':
        return 1
    return 0

def _equiv_not_in_list(text, compare_list):
    if len(text) == 0:
        return not text in compare_list
    for each in compare_list:
        if text == each[:len(text)]:
            return False
    return True

def make_dict_from_xml():
    start = time.time()
    f = open(_xml_location, encoding='UTF-8')
    # total_elements = len(f.readlines())

    context = iterparse(f, events=("end",))
    context = iter(context)

    # get root element
    event, root = next(context)

    char_dict = {}
    count = 0
    total_characters = 13108
    percentage_count = 0
    percentage_interval = 0.02
    progressbar = progress_bar(total_characters, "importing kanji from xml")
    for event, elem in context:
        if elem.tag == "character":
            progressbar.inc_counter()
            char_entry = _make_char_entry()
            stroke_count_added = False
            skip_this_character = True
            for child in elem:
                if child.tag == "literal":
                    char_entry["txt"] = child.text
                elif child.tag == "codepoint":
                    for codepoint in child:
                        if codepoint.tag == "cp_value":
                            cp_type = codepoint.attrib.get('cp_type')
                            if cp_type == 'ucs':
                                char_entry["id"] = int(codepoint.text, 16)
                            elif cp_type == 'jis208':
                                skip_this_character = False
                elif child.tag == "misc":
                    for misc in child:
                        if misc.tag == "stroke_count" and not stroke_count_added:
                            char_entry["stroke_count"] = int(misc.text)
                        elif misc.tag == "grade":
                            char_entry["grade"] = int(misc.text)
                            skip_this_character = False
                        elif misc.tag == "jlpt":
                            char_entry["jlpt"] = int(misc.text)
                            skip_this_character = False
                        elif misc.tag == "variant" and misc.attrib.get("var_type") == "ucs":
                            char_entry["variant"].append(chr(int(misc.text,16)))
                elif child.tag == "dic_number":
                    for dic_number in child:
                        if dic_number.tag == "dic_ref" and dic_number.attrib.get('dr_type') == 'heisig':
                            char_entry["heisig"] = int(dic_number.text)
                            skip_this_character = False
                        if dic_number.tag == "dic_ref" and dic_number.attrib.get('dr_type') == "heisig6":
                            char_entry["heisig6"] = int(dic_number.text)
                            skip_this_character = False
                elif child.tag == "reading_meaning":
                    for reading_meaning in child:
                        if reading_meaning.tag == "rmgroup":
                            for rmgroup in reading_meaning:
                                if rmgroup.tag == "reading":
                                    r_type = rmgroup.attrib.get("r_type")
                                    if r_type == "ja_kun":
                                        #
                                        # Special Handling for character with space in text
                                        #
                                        if " " in rmgroup.text:
                                            char_entry["details"].append(_make_detail_entry(rmgroup.text.split(" ")[0], "rdng_kun"))
                                            char_entry["details"].append(_make_detail_entry(rmgroup.text.split(" ")[1], "rdng_kun"))
                                        else:
                                            detail_entry = _make_detail_entry(rmgroup.text, "rdng_kun")
                                            char_entry["details"].append(detail_entry)
                                            if rmgroup.attrib.get("r_status") is not None:
                                                detail_entry["jy_status"] = True
                                    if r_type == "ja_on":
                                        detail_entry = _make_detail_entry(rmgroup.text, "rdng_on")
                                        char_entry["details"].append(detail_entry)
                                        if rmgroup.attrib.get("r_status") is not None:
                                            detail_entry["jy_status"] = True
                                if rmgroup.tag == "meaning":
                                    if rmgroup.attrib.get('m_lang') is None or rmgroup.attrib.get('m_lang') == 'en':
                                        char_entry["details"].append(_make_detail_entry(rmgroup.text, "meaning"))
                        if reading_meaning.tag == "nanori":
                            char_entry["details"].append(_make_detail_entry(reading_meaning.text, "rdng_nanori"))
            if not skip_this_character:
                char_dict.update({char_entry['txt']: char_entry})
        root.clear()


    progressbar.complete()

    _add_unicode_data(char_dict)
    _add_special_exceptions(char_dict)

    progressbar = progress_bar(len(char_dict), "adding furigana dictionaries")
    for each in char_dict.values():
        progressbar.inc_counter()
        _condense_readings(each)
        furigana_dict = create_readings(each['details'])
        furigana_nanori = create_readings(each['details'], inc_nanori=True)
        each.update({'furigana_dict': furigana_dict, "furigana_nanori": furigana_nanori})

    progressbar.complete()

    return char_dict


def _add_unicode_data(kanji_dict):
    start = time.time()
    f = open(_unicode_xml_location, encoding='UTF-8')
    # total_elements = len(f.readlines())

    context = iterparse(f, events=("end",))
    context = iter(context)

    # get root element
    event, root = next(context)
    char_variant_count = 0
    char_on_count = 0
    char_kun_count = 0
    on_count = 0
    kun_count = 0

    char_count = 0
    var_count = 0
    elem_count = 0
    base_count = 0
    count = 3
    elem_tags =[]
    # print ("Processing unicode data...")
    progressbar = progress_bar(75619, "importing unicode data")
    for event, elem in context:
        # base_count += 1
        if count > 0:
            count -= 1
            elem_tags.append(elem.attrib)
        if elem.tag == "{http://www.unicode.org/ns/2003/ucd/1.0}char":
            progressbar.inc_counter()
            jsource = elem.attrib.get("kIRG_JSource")
            if jsource is not None:
                kanji = chr(int(elem.attrib.get("cp"),16))
                kanji = kanji_dict.get(kanji)
                if kanji is not None:
                    ### Radicals
                    trad_rad = '' if elem.attrib.get("kRSKangXi") is None \
                        else elem.attrib.get("kRSKangXi")
                    jap_rad = '' if elem.attrib.get("kRSJapanese") is None \
                        else elem.attrib.get("kRSJapanese")
                    radicals = []
                    if len(trad_rad) > 0:
                        trad_rad = trad_rad.split(".")[0]
                        radicals.append(('trad',trad_rad))
                    if len(jap_rad) > 0:
                        jap_rad = jap_rad.split(".")[0]
                        radicals.append(('jap',jap_rad))
                    kanji.update({'uni_rads':radicals})
                    ### Readings
                    k_on = [] if elem.attrib.get("kJapaneseOn") is None \
                        else elem.attrib.get("kJapaneseOn").split(" ")
                    k_kun = [] if elem.attrib.get("kJapaneseKun") is None \
                        else elem.attrib.get("kJapaneseKun").split(" ")
                    k_variant = [] if elem.attrib.get("kZVariant") is None \
                        else elem.attrib.get("kZVariant").split(" ")
                    k_comp_variant = [] if elem.attrib.get("kCompatibilityVariant") == "" \
                        else elem.attrib.get("kCompatibilityVariant").split(" ")
                    k_variant += k_comp_variant
                    var_counted = False
                    on_counted = False
                    kun_counted = False
                    for variant in k_variant:
                        char = chr(int(variant.strip("U+").split("<")[0],16))
                        if  char not in kanji["variant"]:
                            kanji["variant"].append(char)
                            var_count += 1
                            if not var_counted:
                                char_variant_count += 1
                                var_counted = True
                    kanji_on = []
                    kanji_kun = []
                    for detail in kanji['details']:
                        if detail['type'] == 'rdng_on':
                            kanji_on.append(detail['txt'].strip("-"))
                        elif detail['type'] == 'rdng_kun':
                            kanji_kun.append(detail['txt'].replace(".","").strip("-"))
                    for on in k_on:
                        if len(on) > 0 and to_katakana(on) not in kanji_on:
                            kanji_on.append(to_katakana(on))
                            kanji['details'].append(_make_detail_entry(to_katakana(on), "rdng_on", True))
                            on_count += 1
                            if not on_counted:
                                on_counted = True
                                char_on_count += 1
                    for kun in k_kun:
                        if len(kun) > 0 and to_hiragana(kun) not in kanji_kun:
                            kanji_kun.append(to_hiragana(kun))
                            kanji['details'].append(_make_detail_entry(to_hiragana(kun),"rdng_kun", True))
                            kun_count += 1
                            if not kun_counted:
                                kun_counted += 1
                                char_kun_count += 1


    progressbar.complete()
    # print("\nProcessed {0} base event and {1} elements.".format(base_count,elem_count))
    #print("Processed {0} characters in {1} secs".format(char_count, time.time() - start))
    print("Added {0} variants to {1} character(s).".format(var_count, char_variant_count))
    print("Added {0} on readings to {1} character(s).".format(on_count,char_on_count))
    print("Added {0} kun readings to {1} characters(s).".format(kun_count,char_kun_count))
    #print(elem_tags)

def _get_exceptions_text(kanji_char):
    return [d['txt'] for d in kanji_char['details'] if d['type'] == 'rdng_excp']

def _add_special_exceptions(kanji_dict):
    f = open(_special_rdngs_location, encoding="UTF-8")
    reader = csv.reader(f, dialect='excel-tab')
    for row in reader:
        words = row[2].split(",")
        for each_word in words:
            for each_char in each_word:
                if kanji_dict.get(each_char) is not None:
                    if (each_word+","+row[1]) not in _get_exceptions_text(kanji_dict[each_char]):
                        kanji_dict[each_char]['details'].append(_make_detail_entry(each_word+","+row[1],"rdng_excp"))

    f = open(_special_rdngs_2_location, encoding="UTF-8")
    reader = csv.reader(f, delimiter=',')
    for row in reader:
        words = row[0].split("・")
        for each_word in words:
            for each_char in each_word:
                if kanji_dict.get(each_char) is not None:
                    if (each_word+","+row[1]) not in _get_exceptions_text(kanji_dict[each_char]):
                        kanji_dict[each_char]['details'].append(_make_detail_entry(each_word+","+row[1],"rdng_excp"))

def compare_kanjivg_rads_to_web(kanji_dict, kanjivg_list, radical_dict):
    output = {'missing_bushu': list(),
              'missing_unicode_rad': list(),
              'matching': dict(),
              'no_match':list(),
              'adjusted': list()}

    web_rad_correction = ['齏', '沓', '弌', '戝', '默', '豫', '齋', '蘖', '辨', '飭', '盜', '叩', '襍', '處', '隨', '匹',
                         '竒', '乕', '杳', '燮', '甸', '霍', '冐', '禀', '彝', '夘', '頴', '會', '爽', '畋', '孱',
                         '弍', '麼', '勒', '雙', '蘗', '侫', '舉', '竸', '麓', '匝', '杲', '斈', '譱', '飮', '鞫', '營',
                         '濶', '耶', '彜', '韮', '寨', '孜', '靖', '簒', '霸', '凖', '皷']
    corrections = ['之', '巨', '巻', '皀', '皃', '的', '皈', '皋', '皎', '皐', '皓', '皖', '皙', '皚', '舎', '舗', '采']
    corrections = {'106':{'皋': 'g1', '皓': 'g1', '皙': 'g4', '皈': 'g1',
                      '皐': 'g1', '皎': 'g1', '皚': 'g1', '皃': 'g1',
                      '皀': 'g1', '皖': 'g1', '的': 'g1'},
                   '26':{'巻':'g7'},
                   '135': {'舎':'g0', '舗':'g1'},
                   '165': {'采':'g0'},
                   '48':{'巨':'g1'},
                   '4':{'之':'g2'},
                   '32':{'尭':'g2'}}
    #add 白 to element
    #add 舎 to element of 舗
    for kanjivg in kanjivg_list:
        if (is_kanji(kanjivg['txt'])):
            kanji = kanji_dict.get(kanjivg['txt'])
            if kanji is None:
                # print ("{0} not found in kanji dict".format(kanjivg['txt']))
                continue
            radicals = get_radicals(kanjivg)
            if kanji.get('bushu') is None or (kanji.get('bushu')[0][0] != kanji.get('uni_rads')[0][1] \
                    and kanjivg['txt'] in web_rad_correction):
                output['missing_bushu'].append(kanjivg['txt'])
                kanji_rad_id = kanji.get('uni_rads')[0][1]
            else:
                kanji_rad_id = kanji.get('bushu')[0][0]


            match_found = False
            used_element = list()
            overall_found = False

            found_radical = list()
            for radical in radicals:
                rad_id = radical.get('rad_id')
                if rad_id is None:
                    continue
                if rad_id == kanji_rad_id:
                    match_found = True
                elif rad_id == kanji.get('uni_rads')[0][1]:
                        match_found = True

                if (rad_id == kanji_rad_id or rad_id == kanji.get('uni_rads')[0][1])\
                        and radical['element'] not in used_element:

                    overall_found = True
                    used_element.append(radical['element'])
                    matching_on = str()
                    if radical.get('radical') != "":
                        matching_on = radical['radical']
                    else:
                        matching_on = 'general'
                    if output['matching'].get(kanjivg['txt']) is None:
                        found_radical.append(radical)

            radical_link = dict()
            if len(found_radical) > 0:
                all_nelson = True
                for radical in found_radical:
                    if radical['radical'] != 'nelson':
                        all_nelson = False
                        radical_link = radical['rad_link']
                        kanjivg.update({'rad_group_id':radical['id']})
                        break
                if all_nelson:
                    radical_link = found_radical[0]['rad_link']
                    kanjivg.update({'rad_group_id':found_radical[0]['id']})
                kanji.update({'radical':radical_link})
                output['matching'].update({kanji['txt']:radical_link})

            if not overall_found:

                manual_adjusted = False
                for key, value in corrections.items():
                    if value.get(kanji['txt']) is not None:
                        group_id = value.get(kanji['txt'])
                        groups = get_groups(kanjivg)
                        for group in groups:
                            if group['id'] == group_id:
                                #if 106 add 白 to element
                                if key == '106':
                                    group['element'] = '白'
                                if kanji['txt'] == '舗':
                                    group['element'] = '舎'
                                group['radical'] = 'manual_adjust'
                                group['orginal'] = radical_dict[key][0]['char']
                                kanjivg.update({'rad_group_id':group['id']})
                                kanji.update({'radical': radical_dict[key][0]})
                                manual_adjusted = True
                                break
                        break
                #output['no_match'].append(kanjivg['txt'])
                if not manual_adjusted:
                    result = find_radical_kanjivg(kanjivg, kanji_rad_id, radical_dict)
                    #result = manual_adjust_radical_kanjivg(kanjivg, kanji, kanji_rad_id, radical_dict)
                    if result is None:
                        output['no_match'].append(kanjivg['txt'])
                    else:
                        output['adjusted'].append(kanjivg['txt'])
                        kanji.update({'radical':result['rad_link']})
                        kanjivg.update({'rad_group_id':result['id']})
                #    output['matching'].update({kanjivg['txt']:[result['radical']]})

    return output

def print_feedback_rad_compare(compare_output):
    print("{0} kanji missing web bushu.".format(compare_output['missing_bushu']))
    print("{0} kanji missing unicode radical.".format(compare_output['missing_unicode_rad']))
    print("{0} kanji with match found.".format(len(compare_output['matching'].keys())))
    print("{0} kanji with no matches.".format(len(compare_output['no_match'])))

def add_kanjivg_to_kanji_char(kanji_dict, kanjivg_list):
    for kanjivg in kanjivg_list:
        kanji = kanji_dict.get(kanjivg['txt'])
        if kanji is None:
            #print("{0} not found in kanji dict".format(kanjivg['txt']))
            continue

        svg, colors, svg_rad_id, radical_list = get_svg_groups_colors_radicals(kanjivg)
        kanji.update({'svg':svg, 'svg_color':colors, 'svg_rad_id': svg_rad_id, 'rad_list':radical_list})


def add_kanjivg_to_kanji_dict(kanji_dict, kanjivg_list):
    exception_list = {'冒':73}
    for kanjivg in kanjivg_list:
        if is_kanji(kanjivg['txt']):
            kanji = kanji_dict.get(kanjivg['txt'])
            if kanji is None:
                #print("{0} not found in kanji dict".format(kanjivg['txt']))
                continue
            kanji.update({"svg":kanjivg})
            radicals = get_radicals(kanjivg)
            for radical in radicals:
                rad_id = radical.get("rad_id")
                if rad_id is not None:
                    if kanji.get('uni_rads') is None:
                        #print("{0} missing in unicode".format(kanji['txt']))
                        break
                    for uni_rad in kanji['uni_rads']:
                        if rad_id == uni_rad[1]:
                            if kanji.get("rad_matches") is None:
                                kanji.update({"rad_matches": list()})
                            kanji['rad_matches'].append((radical,uni_rad))
    output = {'no_match': list(),
              'no_kanjivg': list()}
    rad_types = ['general', 'tradit', 'nelson']
    uni_rad_types = ['trad', 'jap']
    for rad in rad_types:
        for uni_rad in uni_rad_types:
            output.update({rad+'_'+uni_rad:list()})

    for kanji,values in kanji_dict.items():
        if values.get("svg") is None:
            output['no_kanjivg'].append(kanji)
        elif values.get("rad_matches") is None:
            output['no_match'].append(kanji)
        else:
            for rad in values.get("rad_matches"):
                if rad[0]['radical'] == 'general' or rad[0]['trad_form'] == 'true' or rad[0]['rad_form'] == 'true':
                    output['general_'+rad[1][0]].append(kanji)
                else:
                    output[rad[0]['radical'] + '_' + rad[1][0]].append(kanji)

    for key,value in output.items():
        print("{0} has {1} kanji".format(key, len(value)))

    return output

def process_mismatched_rads(kanji_list, kanji_dict, radical_dict):
    for each in kanji_list:
        print("--- {0} ---".format(each))
        kanji = kanji_dict.get(each)
        radicals = get_radicals(kanji.get('svg'))
        print("  --------SVG Radicals-------")
        for rad in radicals:
            print ("\tId-{0} Element-{1} Original-{2}   Type: {3}"
                   .format(rad.get('rad_id'), rad['element'], rad['original'], rad['radical']))
        print("  ------Unicode Radicals-----")
        for uni_rad in kanji['uni_rads']:
            print("\tId-{0} Original-{1}   Type: {2}".format(uni_rad[1], radical_dict[uni_rad[1]][0]['char'], uni_rad[0]))
        usr_input = input("Press [ENTER] to continue or [Q] to quit: ")
        if usr_input in ['q', 'Q']:
            break

def add_ids_all_dicts(kanji_dict):
    for entry in kanji_dict.values():
        idx = 1
        for detail in entry['details']:
            detail['id'] = idx
            idx += 1


def _example_word_code(example_word):
    return example_word[0]['id'],example_word[1]['txt'],example_word[2]['rdng']['txt']

def add_jmdict_entry_to_kanji_chars(kanji_dict, jmdict):
    for entry in kanji_dict.values():
        for detail in entry['details']:
            detail.update({'word_ex':[]})

    max_word_examples = 12
    progress = progress_bar(len(jmdict), "adding example words to kanji chars")
    for entry in jmdict:
        progress.inc_counter()
        for kanji in entry['kanji']:
            for kanji_rdng in kanji['kanji_rdng']:
                for match_list in kanji_rdng['furigana_matches']:
                    for match in match_list:
                        for detail in match['detail']:
                            word_example = detail.setdefault('word_ex',[])
                            word_example.append((entry, kanji, kanji_rdng))
    progress.complete()

    progress = progress_bar(len(kanji_dict), "sorting example words in kanji char readings")
    for char, entry in kanji_dict.items():
        progress.inc_counter()
        kanji_freq = 0
        kanji_examples_selected = list()
        kanji_examples_id_set = set()
        kanji_example_set = set()
        kanji_examples_remaining = list()
        for detail in entry['details']:
            if len(detail.get('word_ex', [])) > 0:
                detail['word_ex'] = sorted(detail['word_ex'],
                                           key = lambda x: calculate_kanji_word_importantance(x[1], x[0], char, detail),
                                           reverse=True)

                # add to kanji's frequence score - could be used to sort kanji
                kanji_freq += sum([calculate_kanji_word_importantance(x[1], x[0]) for x in detail['word_ex']])
                if detail['type'] in ['rdng_kun', 'rdng_on']:
                    # add top example to exmaple list for kanji
                    # add remaining examples to pool for pick best later
                    if _example_word_code(detail['word_ex'][0]) not in  kanji_examples_selected:
                        kanji_examples_selected.append(detail['word_ex'][0])
                        # add code to set
                        kanji_example_set.add(_example_word_code(detail['word_ex'][0]))
                        # add id to set for checking for correct number of examples
                        kanji_examples_id_set.add(detail['word_ex'][0][0]['id'])
                        kanji_examples_remaining.extend(detail['word_ex'][1:])





                    # add importance score to detail freq value
                    detail['freq'] = sum([calculate_kanji_word_importantance(x[1], x[0], char, detail) for x in detail['word_ex']])
                else:
                    kanji_examples_remaining.extend(detail['word_ex'])

        # process remaining examples
        remaining_example_count = max(0, max_word_examples - len(kanji_examples_id_set))
        if remaining_example_count > 0:
            kanji_examples_remaining = sorted(kanji_examples_remaining,
                                              key = lambda x: calculate_kanji_word_importantance(x[1], x[0]),
                                              reverse=True)
            for each in kanji_examples_remaining:
                if _example_word_code(each) not in kanji_examples_selected:
                    kanji_examples_selected.append(each)
                    kanji_example_set.add(_example_word_code(each))
                    if each[0]['id'] not in kanji_examples_id_set:
                        remaining_example_count -= 1
                        kanji_examples_id_set.add(each[0]['id'])
                    if remaining_example_count == 0:
                        break

        entry.update({'examples': sorted(kanji_examples_selected,
                                           key = lambda x: calculate_kanji_word_importantance(x[1], x[0]),
                                           reverse=True)})
        entry.update({'freq':kanji_freq})
    progress.complete()


def add_locations_to_kanji_chars(kanji_dict, combined_locations):
    #combined_locations = make_combined_location_names()
    for entry in kanji_dict.values():
        for detail in entry['details']:
            detail.update({'location_ex':[]})

    progress = progress_bar(len(combined_locations), "adding location names to kanji chars")
    for location in combined_locations:
        progress.inc_counter()
        if type(location) == dict:
            txt = location['txt']
            rdng = location['rdng']
        else:
            txt = location[0]
            rdng = location[1]
        furigana_matches = make_furigana_list(kanji_dict, txt, rdng, nanori=True)
        location.update({'furigana_matches': furigana_matches})
        if furigana_matches is not None and len(furigana_matches) > 0:
            for furi_match_list in furigana_matches:
                for furi_match in furi_match_list:
                    for detail in furi_match['detail']:
                        detail['name_freq'] += 1
                        example_list = detail.setdefault('location_ex',[])
                        example_list.append(location)
    progress.complete()


def add_name_to_example_lists(name):
    current_output = None
    categories = []
    if 'categories' in name:
        current_output = name
        categories = name['categories']
    for furi_match_list in name['furigana_matches']:
        for furi_match in furi_match_list:
            for detail in furi_match['detail']:
                example_list = detail.setdefault('name_ex',[])
                detail['name_freq'] += 1
                if not is_name_list_full(example_list, name, furi_match['char']):
                    if current_output is None:
                        try:
                            current_output, categories = get_wiki_people(name['txt'])
                            name.update(current_output)
                        except:
                            e = str(sys.exc_info()[0])
                            if e == str(KeyboardInterrupt):
                                raise
                            return 'error', {'name': name['txt'], 'error':e}
                    example_list.append(name)
    return 'categories', categories


def add_names_to_kanji_chars(sorted_names, saved_name_data):
    category_dict = dict()
    failed_people =list()
    #saved_name_data = get_saved_name_data("downloaded_names.json")
    progress = progress_bar(len(sorted_names), "adding name examples to kanji details...")
    for each_name in sorted_names:
        progress.inc_counter()
        if each_name['txt'] in saved_name_data:
            each_name.update(saved_name_data[each_name['txt']])

        try:
            output_type, output = add_name_to_example_lists(each_name)
            if output_type == 'error':
                failed_people.append(output)
            if output_type == 'categories':
                for each in output:
                    category_dict.update({each:category_dict.setdefault(each,0)+1})
        except KeyboardInterrupt:
            return category_dict, failed_people


    progress.complete()


    return category_dict, failed_people


def is_name_list_full(example_name_list, name, kanji):
    max_dup_names = 2
    max_examples = 7
    example_name_counter = collections.Counter([sep['kanji'] for each in example_name_list for sep in each['separated'] if kanji in sep['kanji']])
    for each in name['separated']:
        if kanji in each['kanji'] and example_name_counter.get(each['kanji'],0) >= max_dup_names:
            return True

    for each in example_name_list:
        if each['date_born']['y'] is None:
            return False

    if len(example_name_list) >= max_examples:
        return True
    else:
        return False

def make_ex_name_dict(kanji_dict):
    output = dict()

    max_person_names = 5
    max_location_names = 5
    max_names = 10

    for char, value in kanji_dict.items():
        selected_person_names = list()
        remaining_person_names = list()
        selected_location_names = list()
        remaining_location_names = list()
        selected_names = list()
        for detail in value['details']:
            if detail.get('name_ex') is not None and len(detail.get('name_ex')) > 0:
                selected_person_names.append(detail.get('name_ex')[0])
                remaining_person_names.extend(detail.get('name_ex')[1:])
            if detail.get('location_ex') is not None and len(detail.get('location_ex')) > 0:
                selected_location_names.append(detail.get('location_ex')[0])
                remaining_location_names.extend(detail.get('location_ex')[1:])
        remaining_person_names = sorted(remaining_person_names, key = lambda person: calculate_person_score(person), reverse=True)
        selected_person_names.extend(remaining_person_names[:max(0, max_person_names-len(selected_person_names))])
        remaining_location_names = sorted(remaining_location_names, key = lambda loc: calculate_location_score(loc), reverse=True)
        selected_location_names.extend(remaining_location_names[:max(0, max_location_names-len(selected_location_names))])
        selected_names = selected_person_names + selected_location_names
        if len(selected_names) < max_names:
            selected_names.extend(remaining_location_names[:max(0, max_names - len(selected_names))])
            selected_names.extend(remaining_person_names[:max(0, max_names - len(selected_names))])
        #
        # Create entries for name dictionary
        #

        for name in selected_names:
            if output.get(name['txt']) is None:
                name_entry = {'txt': str(), 'rdng': str(), 'detail_json': str(), 'type': str(), 'char':dict(), 'freq': int()}

                output.update({name['txt']:name_entry})
                name_entry['txt'] = name['txt']
                name_entry['rdng'] = name['rdng']
                name_entry['type'] = name['type']
                if type(name_entry['type']) != list:
                    score_tuple = calculate_location_score(name)
                    name_entry['freq'] = score_tuple[0] + min(score_tuple[1] * .0000001, .9999999)
                else:
                    score_tuple = calculate_person_score(name)
                    name_entry['freq'] = score_tuple[0] * 1000 + min(score_tuple[1], 999) + min(score_tuple[2]*.00001, .99999)
                if name_entry['type'] == 'todofuken':
                    name_entry['detail_json'] = {'a': name['area'],
                                                   'p': name['population'],
                                                   'c': name['capital'],
                                                   'h': name['chihou']}
                elif name_entry['type'] == 'city':
                    name_entry['detail_json'] = {'a': name['area'],
                                                   'p': name['population'],
                                                   'l': name['location']}
                elif name_entry['type'] == 'shrine':
                    name_entry['detail_json'] = {'l': name['location'],
                                                   'f': name['founded']}
                elif name_entry['type'] == 'temple':
                    name_entry['detail_json'] = {'l': name['location'],
                                                   'f': name['founded']}
                elif name_entry['type'] == 'neighborhood':
                    location_names = sorted(name['locations'], key = lambda x: x[4], reverse=True)
                    name_entry['detail_json'] = {'t': len(name['locations']),
                                                   'l': location_names[0][2]}
                else:
                    # clean up eng name data:
                    #     remove additional information denoted with ',' or '('
                    #     add data to new field in name: 'info'
                    #     add dates for birth/death where not already included in data
                    date_pattern = '[(](?P<born_y>[0-9]{3,4})(?:.(?P<born_m>[0-9]{1,2})(?:.(?P<born_d>[0-9]{1,2}))?)?-' \
                              '(?:(?P<death_y>[0-9]{3,4})(?:.(?P<death_m>[0-9]{1,2}).(?P<death_d>[0-9]{1,2}))?)?[)]?'

                    result = re.search(date_pattern, name['eng'])
                    if result is None:
                        date_pattern = '[(][?]-' \
                              '(?P<death_y>[0-9]{3,4})(?:.(?P<death_m>[0-9]{1,2})(?:.(?P<death_d>[0-9]{1,2}))?)?[)]?'
                        result = re.search(date_pattern, name['eng'])
                    if result is not None and name['date_born']['y'] is None:
                        name['date_born']['y'] = result.groupdict().get('born_y')
                        name['date_born']['m'] = result.groupdict().get('born_m')
                        name['date_born']['d'] = result.groupdict().get('born_d')
                        name['date_death']['y'] = result.groupdict().get('death_y')
                        name['date_death']['m'] = result.groupdict().get('death_m')
                        name['date_death']['d'] = result.groupdict().get('death_d')

                    name['eng'] = re.sub(date_pattern, '', name['eng'])

                    comma_idx =  name['eng'].find(',') #name['eng'] if name['eng'].find(',') < 0 else name['eng'].find(',')
                    paren_idx =  name['eng'].find('(') #name['eng'] if name['eng'].find('(') < 0 else name['eng'].find('(')
                    idx = len(name['eng'])
                    if comma_idx >= 0 and paren_idx >= 0:
                        idx = min(comma_idx, paren_idx)
                    elif comma_idx > 0:
                        idx = comma_idx
                    elif paren_idx > 0:
                        idx = paren_idx
                    removed_data = name['eng'][idx:].strip()
                    name['eng'] = name['eng'][:idx].strip()
                    removed_data.strip('(').strip(')')
                    removed_data = removed_data.split(',')

                    name_entry['detail_json'] = {'e': name['eng'],
                                                   'i': removed_data,
                                                   'c': name['categories'],
                                                   'b': name['date_born'],
                                                   'd': name['date_death']}
            output[name['txt']]['char'].setdefault(char, set())
            for furi_match_set in name['furigana_matches']:
                for furi_match in furi_match_set:
                    if char == furi_match['char']:
                        for detail in furi_match['detail']:
                            output[name['txt']]['char'][char].add(detail['txt'])
    return output


