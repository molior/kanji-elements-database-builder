import os
import json

from sqlalchemy.schema import DropTable, CreateTable

from sqlalchemy.dialects.sqlite import REAL
from sqlalchemy.orm import relationship, backref, scoped_session, sessionmaker

from database_tools import *
from KanjiDict2Parser import *
from JMDictParser import *
from corpus_parser import *
from jmdict_tools import calculate_kanji_word_importantance
from wordnet_connector import *
from NameDictParser import *
from kanjivg_list import *
from radical_parser import *
from RadicalGet import *
import FuriganaGenerator
from imedictionary_parser import create_ime_dict
import thesaurus_parser
import shutil

jmdict_list = []
kanji_dict = dict()
name_dict = dict()
sent_list = list()
synset_dict = dict()
kanjivg_list = []
radical_dict = dict()
ime_dict = dict()
output = list()
feedback = tuple()
surnames = dict()
given_names = dict()
eng_lemma = dict()
jpn_lemma = dict()
dup_dict = dict()
kanjivg_radicals = dict()
thesaurus_list = dict()
sorted_full_names = list()
ex_name_dict = dict()

def one():
    global jmdict_list, kanji_dict, name_dict, \
            output, feedback, \
            surnames, given_names, dup_dict, kanjivg_radicals, sent_list,\
            sorted_full_names, ex_name_dict

    # global ime_dict, kanjivg_list, radical_dict, \
    #         synset_dict, eng_lemma, jpn_lemma, thesaurus_list, sorted_full_names
    progress = progress_bar(1,"processing data files")

    #
    # Import kanji diciontionary
    #
    kanji_dict = make_dict_from_xml()

    #
    # Import Japanese-English dictionary, mark for ime availability (used for sorting and collating), add word counts from tanaka corpus
    #
    jmdict_list = make_list_from_xml()
    ime_dict = create_ime_dict()
    mark_jmdict_for_ime(jmdict_list, kanji_dict, ime_dict)
    add_word_count_tanaka(jmdict_list)
    ime_dict.clear()
    #
    # Import English japanese wordnet, add char meaning scores to char details(for sorting) and to kanji words (for sorting)
    #
    synset_dict = make_wordnet_dictionary()
    jpn_lemma = make_lemma_dictionaries(synset_dict)
    thesaurus_list = thesaurus_parser.make_thesaurus_list()
    add_char_meaning_score( kanji_dict, jmdict_list, jpn_lemma, thesaurus_list)
    synset_dict.clear()
    jpn_lemma.clear()
    thesaurus_list.clear()

    #
    # Add entries to kanji chars, add importance score to applicable char details (for sorting)
    #
    add_jmdict_entry_to_kanji_chars(kanji_dict, jmdict_list)


    #
    # Import sentences and add sentences to japanese-english dictionary entries
    #
    sent_list, output = make_tanaka_sentence_list(jmdict_list)
    add_sentences_to_entries(jmdict_list, sent_list)

    #
    # Import and add svg and radical data to chars
    #
    kanjivg_list = make_kanjivg_dict()
    kanjivg_radicals, output = make_kanjivg_radical_dict(kanjivg_list)
    radical_dict = make_radical_dict()
    output = add_bushu_to_kanji(kanji_dict, radical_dict)
    match_radical_to_kanjivg(kanjivg_list, radical_dict)
    output = compare_kanjivg_rads_to_web(kanji_dict, kanjivg_list, radical_dict)
    add_kanjivg_to_kanji_char(kanji_dict, kanjivg_list)

    #
    # Import people names, sort and collate
    #
    name_dict = make_name_list()
    FuriganaGenerator.generate_furigana_per_fullname(name_dict['full_names']['dict'], kanji_dict)
    output = full_name_splitter(name_dict['full_names']['dict'])
    FuriganaGenerator.generate_furigana_per_name(name_dict,kanji_dict)
    clean_up_given_names(name_dict['given_names']['dict'])
    given_names = import_given_name_freq_file(name_dict['given_names']['dict'])
    surnames = import_freq_surnames_file()
    sorted_full_names = sort_full_names_v2(name_dict['full_names']['dict'],surnames, given_names)
    saved_name_data = get_saved_name_data('downloaded_names.json')
    output = add_names_to_kanji_chars(sorted_full_names, saved_name_data)
    # dump_download_name_data(sorted_full_names)
    name_dict.clear()
    # sorted_full_names.clear()

    #
    # Import Location names and add to character
    #
    combined_locations = make_combined_location_names()
    add_locations_to_kanji_chars(kanji_dict, combined_locations)
    combined_locations.clear()
    #
    # Make dictionary of examples names (person and locations from examples in chars
    #
    ex_name_dict = make_ex_name_dict(kanji_dict)

    #
    # Make list of entries from selected examples
    #
    entry_ids = make_set_entry_ids(jmdict_list, kanji_dict)
    sent_entry_ids, sent_ids = make_set_entry_sentence_ids(jmdict_list, entry_ids)
    jlpt_entry_ids = make_set_entry_ids_jlpt(jmdict_list)

    #
    # Make database and tables
    #
    base, table_dict = make_table_dict()
    # engine = create_database_engine_from_table(table_dict)
    # base, table_dict = make_base_table_dict()
    engine = create_database_engine(base)

    #
    # Add data to database
    #
    insert_ja_en_data(jmdict_list, entry_ids.union(sent_entry_ids,jlpt_entry_ids), kanji_dict, table_dict, engine)
    jmdict_list.clear()
    insert_ex_sent_data(sent_list, sent_ids, kanji_dict, table_dict, engine)
    sent_list.clear()
    insert_char_data(kanji_dict, kanjivg_radicals, table_dict, engine)
    #update_char_ja_en_examples(kanji_dict, table_dict, engine)
    kanji_dict.clear()
    insert_ex_name_data(ex_name_dict, table_dict, engine)
    #ex_name_dict.clear()


    #
    # Output for testing data
    #
    #output_no_ime_list(jmdict_list)
    #output_no_furigana_matches(jmdict_list, ime_dict)
    #dup_dict = output_words_with_duplicate_spelling(jmdict_list)
    #output_words_meaning_no_examples(jmdict_list, jpn_lemma)
    #output_multiword_gloss(jmdict_list)



    #
    #   Old testing name functions
    #
    #dump_name_data_to_file(given_names, 'processed_given_names2.json')
    #dump_name_data_to_file(surnames,'processed_surnames2.json')
    #dump_name_data_to_file(name_dict['full_names']['dict'],'processed_full_names2.json')
    #full_name_dict = get_saved_data('processed_full_names.json')
    #given_names = get_saved_data('processed_given_names.json')
    #surnames = get_saved_data('processed_surnames.json')

    #
    #   Old testing functions
    #
    #print_feedback_rad_compare(output)
    #feedback = add_bushu_to_kanji(bushu, kanji_dict, radical_dict)
    #output = add_kanjivg_to_kanji_dict(kanji_dict, kanjivg_list)
    #print_character_feedback(jmdict_list,kanji_dict)
    #name_list = make_name_list()
    #generate_furigana_per_name(name_list,kanji_dict)
    #print_name_list_feedback(name_list)

    progress.complete()
    return

one()

def printer_sep_by_grammar(kanji):
    for kanji_rdng in kanji['kanji_rdng']:
        print (kanji_rdng['rdng']['txt'])
        for each in kanji_rdng['furigana_matches']:
            text = ''
            holder = ''
            for match in reversed(each):
                if bool(match['char']):
                    holder += match['char'] + match['okurigana']
                elif bool(holder):
                    text += " " + holder + " " + match['okurigana']
                    holder = ''
                else:
                    text += match['okurigana']
            text += " " + holder if bool(text) else holder
            print('...\t'+text)

def get_kanji_from_dict(word):
    for each in jmdict_list:
        for kanji in each['kanji']:
            if kanji['txt'] == word:
                return kanji
    return None













