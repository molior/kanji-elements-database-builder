__author__ = 'Brian'

import csv
import re
import json
import collections

import romkan

from wchartype import is_kanji
from progressbar import progress_bar
from FuriganaGenerator import convert2hira

_data_location = "Data Files/"
_name_dict_file = "enamdict_u.txt"
_last_name_file = "last names.txt"
_first_name_file = "name_frequency.txt"

_codeblock_hiragana = '\u3040-\u309f'
_codeblock_katakana = '\u30a0-\u30ff'
_codeblock_katakana_half = '\uff66-\uff9f'
_codeblock_kanji = '\u4E00-\u9FFF|\u3005'

def _make_name_entry(txt = str(), rdng = str(), type = list(), eng = str()):
    dict = {'txt':txt,
            'rdng':rdng,
            'type':type,
            'eng':eng,
            'count':1}
    return dict

def _has_kanji(string):
    for char in string:
       if is_kanji(char):
           return True
    return False


def make_name_list():
    f = open(_data_location+_name_dict_file, encoding="UTF-8")
    reader = csv.reader(f, delimiter = '/')
    name_list = []
    progress = progress_bar(739342, "importing name_dict")

    #   s - surname (138,500)
    #   p - place-name (99,500)
    #   u - person name, either given or surname, as-yet unclassified (139,000)
    #   g - given name, as-yet not classified by sex (64,600)
    #   f - female given name (106,300)
    #   m - male given name (14,500)
    #   h - full (usually family plus given) name of a particular person (30,500)
    #   pr - product name (55)
    #   c - company name (34)
    #   st - stations (8,254)

    name_dicts = {'place_names':{'dict':dict(),'types':['p', 'st']},
                    'surnames':{'dict':dict(),'types':['s', 'u']},
                    'given_names':{'dict':dict(),'types':['f', 'm','g','u']},
                    'full_names':{'dict':dict(),'types':['h']}}

    given_names = list()
    surnames = list()
    unknown_names = list()
    full_names = list()
    valid_types = ['u','f','m','s','p','h','g','st']
    for row in reader:
        kanji_rdng = row[0]
        if _has_kanji(kanji_rdng):
            # parse readings and kanji
            txt = str(kanji_rdng.split(' ')[0])
            rdng = kanji_rdng.split(' ')[1].strip('[').strip(']')
            type_eng = row[1]
            type = type_eng.split(') ')[0].strip('(').strip(')').split(',')
            if not set(type).issubset(set(valid_types)):
                continue
            eng = type_eng.split(') ')[1]
            for name_dict in name_dicts.values():
                if set(type).issubset(set(name_dict['types'])):
                    if name_dict['dict'].get(txt) is None:
                        name_dict['dict'].update({txt:[_make_name_entry(txt,rdng,type,eng)]})
                    else:
                        name_dict['dict'][txt].append(_make_name_entry(txt,rdng,type,eng))
        progress.inc_counter()

    print("\nProcessed {0} entries in {1} secs.".format(progress.counter, progress.get_time_elapsed()))
    return name_dicts

def count_common_characters(name_dict):
    kanji_count_dict = dict()
    for each in name_dict.keys():
        for char in each:
            if kanji_count_dict.get(char) is None:
                kanji_count_dict.update({char:1})
            else:
                kanji_count_dict[char] += 1

    #sorted_kanji = sorted(kanji_count_dict, key = lambda entry: kanji_count_dict[entry], reverse= True)

    return kanji_count_dict


def process_place_names(place_name_dict):
    new_dict = place_name_dict.copy()
    size_sorted_list = sorted(place_name_dict.keys(), key = lambda entry: (len(entry), entry))
    #alpha_sorted_list = sorted(place_name_dict.keys())
    while len(size_sorted_list) > 0 and len(size_sorted_list[len(size_sorted_list) -1 ]) > 1:
        current = size_sorted_list.pop()
        for each in size_sorted_list:
            if current == each[0:len(current)]:
                for current_entry in new_dict[current]:
                    for entry in new_dict[each]:
                        if current_entry['rdng'] == entry['rdng'][0:len(current['rdng'])]:
                            current_entry['count'] += 1
                            #destroy this entry
                            break

def get_norm(norm_list, value):
    if value <= norm_list[0]:
        return 2
    for idx, norm_value in enumerate(norm_list):
        if norm_value > value:
            return idx+3
    return len(norm_list) + 3

def import_freq_surnames_file():
    norm_surnames = [2,5,15,54,162,486,1458]
    last_names = dict()
    with open(_data_location + _last_name_file, "r", encoding="UTF-8") as fp:
        reader = csv.reader(fp)
        # entry example: 鈴木,ｽｽﾞｷ,36026,1,36026,2.01905%
        skip_first = True
        for row in reader:
            if skip_first:
                skip_first = False
                continue
            not_kanji = False
            for char in row[0]:
                if not is_kanji(char):
                    not_kanji = True
                    break
            if not_kanji:
                continue
            entry = {'name': row[0], 'rdng': convert2hira(row[1]),
                     'count': row[2], 'percent' : row[5],
                     'used': 0, 'freq': get_norm(norm_surnames,int(row[2]))}
            if last_names.get(row[0]) is None:
                last_names.update({row[0]:[entry]})
            else:
                last_names[row[0]].append(entry)

    return last_names

def clean_up_given_names(given_name_dict):
    reduced_names = 0
    total_reduction = 0
    for key in given_name_dict.keys():
        old_entries = given_name_dict[key]
        new_entries = list()
        for entry in old_entries:
            if len(entry['furigana_matches']) > 0:
                max_count = 0
                for match in entry['furigana_matches']:
                    for match_set in match:
                        temp_sum = sum([detail['name_count']  for detail in match_set['detail']])
                        if   temp_sum > max_count:
                            max_count = temp_sum
                entry.update({'max_count':max_count})
                new_entries.append(entry)
        if len(new_entries) > 0:
            new_entries = sorted(new_entries, key = lambda x: x['max_count'], reverse= True)
            given_name_dict[key] = new_entries
        elif len(old_entries) > 1:
            given_name_dict[key] = []

        if len(old_entries) > len(new_entries):
            reduced_names += 1
            total_reduction += len(old_entries) - len(new_entries)

    print("Reduced {0} given names by a total of {1}".format(reduced_names, total_reduction))

def import_chouiki_as_dict():
    # import 町域 from file
    # return dict of names that points to list of reading(s) with list of location(s)

    # "住所CD","都道府県CD","市区町村CD","町域CD","郵便番号","事業所フラグ","廃止フラグ","都道府県","都道府県カナ",
    # "市区町村","市区町村カナ","町域","町域カナ","町域補足","京都通り名","字丁目","字丁目カナ","補足","事業所名",
    # "事業所名カナ","事業所住所","新住所CD"
    return


def import_given_name_freq_file(given_name_dict):
    norm_first_names = [7, 21, 63, 189, 567, 1701, 5103]
    # get first names (given names)
    first_names = dict()
    with open(_data_location + _first_name_file, "r", encoding="UTF-8") as fp:
        reader = csv.reader(fp)
        # example:  順位,名,件数
        #           1,清,165439
        skip_first = True
        for row in reader:
            if skip_first:
                skip_first = False
                continue
            entry = {'name': row[1], 'count': 0,
                     'used': 0, 'freq': get_norm(norm_first_names, int(row[2])),
                     'rdng': ''}
            first_names.update({entry['name']: []})
            rdngs = given_name_dict.get(entry['name'])
            if rdngs is not None:
                for each in rdngs:
                    new_entry = entry.copy()
                    new_entry['count'] = each['max_count'] if each.get('max_count') is not None else 0
                    new_entry['rdng'] = each['rdng']
                    first_names[entry['name']].append(new_entry)
            else:
                first_names[entry['name']].append(entry)

    return first_names



def sort_full_names(full_name_dict, last_names, first_names):
    # get last names (surnames)
    not_found_list = list()
    found_list = list()
    for name, entries in full_name_dict.items():
        pos = _try_names(last_names, first_names, name, 1)
        if pos is not None:
            for each in entries:
                last_name = name[0:pos]
                first_name = name[pos:]
                found_l_name = False
                for l_name in last_names[last_name]:
                    if l_name['rdng'] == each['rdng'][:len(l_name['rdng'])]:
                        each.update({'surname': {'name': last_name, 'freq': l_name['freq'], 'rdng': l_name['rdng']},
                                     'given': {'name': first_name, 'freq': first_names[first_name][0]['freq'],
                                               'rdng': each['rdng'][len(l_name['rdng']):]}})

                        if each['rdng'][len(l_name['rdng']):] not in first_names[first_name]['rdng']:
                            first_names[first_name]['rdng'].append(each['rdng'][len(l_name['rdng']):])

                        l_name['used'] += 1
                        found_given_name = None
                        for given_name in first_names[first_name]:
                            if each['rdng'][len(l_name['rdng']):] == given_name['rdng']:
                                given_name['used'] += 1
                                found_given_name = given_name
                                break

                        if found_given_name is None:
                            if first_names[first_name][0]['rdng'] == '':
                                first_names[first_name][0]['rdng'] = each['rdng'][len(l_name['rdng']):]
                                first_names[first_name][0]['used'] = 1
                                found_given_name = first_names[first_name][0]
                            else:
                                new_entry = first_names[first_name][0].copy()
                                new_entry['rdng'] = each['rdng'][len(l_name['rdng']):]
                                new_entry['count'] = 0
                                new_entry['used'] = 1
                                found_given_name = new_entry
                                first_names[first_name].append(new_entry)

                        each['count'] = [l_name, found_given_name]
                        found_list.append(each)
                        found_l_name = True
                        break
                if not found_l_name:
                    # each['count'] = [last_names[last_name][-1], first_names[first_name][0]]
                    # each.update({'no_match_found': True})
                    not_found_list.append(each)
        else:
            for each in entries:
                not_found_list.append(each)

    sorted_full_names = sorted(found_list,
                               key=lambda x: (x['count'][0]['freq'] + x['count'][1]['freq'],
                                              x['count'][0]['used'] + x['count'][1]['used']),
                               reverse=True)

    count_used_first_names = 0
    for each in first_names.values():
        for n in each:
            if n['used'] > 0:
                count_used_first_names += 1
                break
    count_used_last_names = 0
    for each in last_names.values():
        for n in each:
            if n[0]['used'] > 0:
                count_used_last_names += 1
                break

    print("{0} of {1} first names found - {2}%".format(count_used_first_names, len(first_names),
                                                       round(count_used_first_names/len(first_names)*100, 2)))
    print("{0} of {1} last names found - {2}%".format(count_used_last_names, len(last_names),
                                                      round(count_used_last_names/len(last_names)*100, 2)))
    print("Matched {0} of {1} full names - {2}%".format(len(found_list), len(full_name_dict),
                                                        round(len(found_list)/len(full_name_dict)*100, 2)))

    return (sorted_full_names, last_names, first_names)

def sort_full_names_v2(full_name_dict, last_names, first_names):
    # sort full names according to frequency of contained surname and given name(s)
    # sort is ordered: average frequency of contained names, average count of contained names
    not_found_list = list()
    found_list = list()
    added_first_name_count = 0
    added_last_name_count = 0
    for name, entries in full_name_dict.items():
        for each in entries:
            # if name could not be separated continue
            if each.get('separated') is None:
                continue
            found_list.append(each)
            surname = each['separated'][0]['kanji']
            # find matching reading in last_name dictionary or add surname if not found
            if last_names.get(surname) is None:
                found_surname = {'name': surname, 'rdng': each['separated'][0]['rdng'],
                                    'count': 0, 'percent' : 0,
                                    'used': 1, 'freq': 1}
                added_last_name_count += 1
                last_names.update({surname:[found_surname]})
            else:
                found_surname = None
                for s_name in last_names[surname]:
                    if s_name['rdng'] == each['separated'][0]['rdng']:
                        found_surname = s_name
                        found_surname['used'] += 1

                        break
                if found_surname is None:
                    found_surname = {'name': surname, 'rdng': each['separated'][0]['rdng'],
                                    'count': 0, 'percent' : 0,
                                    'used': 1, 'freq': 1}
                    last_names[surname].append(found_surname)
                    added_last_name_count += 1
            each['separated'][0].update({'freq_link': found_surname})

            # find matching given name in first_name dictionary or add given name(s) if not found
            # add link to found in full_name['separated'] location
            found_given_names = list()
            for g_name in each['separated'][1:]:
                if first_names.get(g_name['kanji']) is None:
                    found_given_name = {'name': g_name['kanji'], 'count': 0,
                                        'used': 1, 'freq': 1,
                                        'rdng': g_name['rdng']}
                    first_names.update({g_name['kanji']:[found_given_name]})
                    added_first_name_count += 1
                else:
                    found_given_name = None
                    for rdng_name in first_names.get(g_name['kanji']):
                        if rdng_name['rdng'] == g_name['rdng']:
                            found_given_name = rdng_name
                            found_given_name['used'] += 1
                    if found_given_name is None:
                        found_given_name = {'name': g_name['kanji'], 'count': 0,
                                            'used': 1, 'freq': 1,
                                            'rdng': g_name['rdng']}
                        first_names[g_name['kanji']].append(found_given_name)
                        added_first_name_count += 1
                g_name.update({'freq_link':found_given_name})




    sorted_full_names = sorted(found_list,
                               key=lambda person: calculate_person_score(person),
                               reverse=True)



    print("Added {0} first names.".format(added_first_name_count))
    print("Added {0} last names".format(added_last_name_count))
    print("Sorted {0} full names".format(len(sorted_full_names)))

    return sorted_full_names


def is_location_list_full(example_location_list, location):
    max_examples = 5
    max_dup_names = 1
    name_counter = collections.Counter([each['txt'] for each in example_location_list])
    if name_counter.get(location['txt'],0) >= max_dup_names:
        return True

    if len(example_location_list) >= max_examples:
        return True
    else:
        return False

def add_ime_rdngs_to_given_names(ime_list, given_names):
    return

def _try_names(last_names, first_names, name, pos):
    if pos > 5:
        return None
    if last_names.get(name[0: pos]) is None:
        return _try_names(last_names, first_names, name, pos + 1)
    else:
        if first_names.get(name[pos:]) is None:
            return _try_names(last_names, first_names, name, pos + 1)
        else:
            return pos


def _try_last_names(last_names, name, pos):
    if pos > 5:
        return -1
    if last_names.get(name[0:pos]) is None:
        return _try_last_names(last_names, name, pos + 1)
    else:
        try_next = _try_last_names(last_names, name, pos + 1)
        if try_next == -1:
            return pos
        else:
            return try_next

def full_name_splitter(full_name_dict):
    # separate full name into given and surname, add to dictionary entry
    # output list of inseparable names

    progressbar = progress_bar(len(full_name_dict), "splitting full names")
    reading_english_fail = list()
    match_failed = list()
    mismatch_list = list()
    for name_entry in full_name_dict.values():
        # break up readings based on english (romaji)
        # names that do not match reading a saved to list
        # special cases for names containing:
        #       'x', 'l', 'v', '('  - thrown out
        #       '-no-' - (usually found in Emporer names) convert to -> ' no '
        #       'Emporer Go-' - convert to -> 'go'
        #       'Emporer ' - stripped off (after previous special case
        #       [0-9] - numbers need to be stripped off
        #       'A-' - convert to 'e-'
        #       'B-' - convert to 'bi-'
        #       'nn' - double n convert to "n'n"
        #       'dz' - convert to 'd'

        # Create list of names based on eng entry
        progressbar.inc_counter()

        for entry in name_entry:
            # English entry can contain additional information separated either by ', '  or ' ('
            addit_info_idx = entry['eng'].find(' (')
            if addit_info_idx == -1:
                addit_info_idx = entry['eng'].find(', ') if entry['eng'].find(', ') != -1 else len(entry['eng'])
            else:
                addit_info_idx = entry['eng'].find(', ') \
                    if entry['eng'].find(', ') < addit_info_idx and entry['eng'].find(', ') != -1 else addit_info_idx
            name_text = entry['eng'][:addit_info_idx]

            # special case: 'x', 'l', 'v', '('  - throw out
            if len(set(name_text).intersection(['x','l','v','('])) > 0:
                reading_english_fail.append(entry)
                continue

            # special case correction startings
            special_case = {'Emperor Go-': ('go',"ten'nou"),
                            'Emperor ': ('',"ten'nou"),
                            'Empress ': ('',"ten'nou"),
                            'A-': ('e-',''),
                            'B-': ('bi-','')}
            for key, replacement in sorted(special_case.items(), key = lambda x: len(x[0]), reverse = True):
                if key == name_text[:len(key)]:
                    name_text = replacement[0] + name_text[len(key):] + replacement[1]

            # special case correction in text
            special_case = {'-no-': ' no ',
                            'nn': "n'n",
                            'dz': "d"}
            for key, replacement in special_case.items():
                start_idx = name_text.find(key)
                if start_idx >= 0:
                    name_text = name_text[:start_idx] + replacement + name_text[start_idx+len(key):]

            # split name on name " " (spaces), compare to reading, if not match throw out and continue
            seperated_name_list = list()
            current_reading = entry['rdng']
            failed_match = False
            for each in name_text.split(' '):
                # check string for numbers - if so skip
                if bool(re.search('\d',each)):
                    continue
                hira = romkan.to_hiragana(each)
                if hira != convert2hira(current_reading[:len(hira)]):
                    reading_english_fail.append(entry)
                    failed_match = True
                    break
                seperated_name_list.append({'eng':each, 'rdng':current_reading[:len(hira)], 'kanji':''})
                current_reading = current_reading[len(hira):]

            # stop if failed a match
            if failed_match:
                continue

            # cycle through names and kanji_reading pairs
            # get separated name
            # build name until it equals the name
            # then move to next name if available
            # cleanup kanji_rdng list by removing sets that don't match names seperated_name_list

            kanji_name = list()
            kanji_rdng_cleaned = list()

            mismatch_found = False
            for kanji_rdng_list in entry['furigana_matches']:
                seperated_name_list_pos = 0

                reading = seperated_name_list[seperated_name_list_pos]['rdng']

                failed_match = False

                kanji_name_temp = list()
                built_name = str()
                built_kanji = str()
                for kanji_rdng in reversed(kanji_rdng_list):

                    built_name += convert2hira(kanji_rdng['txt'])
                    built_kanji += kanji_rdng['char']
                    if len(built_name) > len(reading):
                        failed_match = True
                        break
                    if built_name == reading:
                        kanji_name_temp.append(built_kanji)
                        seperated_name_list_pos += 1
                        if seperated_name_list_pos >= len(seperated_name_list):
                            kanji_rdng_cleaned.append(kanji_rdng)
                            break
                        reading = seperated_name_list[seperated_name_list_pos]['rdng']
                        built_name = str()
                        built_kanji = str()

                if not failed_match:
                    if len(kanji_name) == 0:
                        kanji_name += kanji_name_temp
                    elif kanji_name != kanji_name_temp:
                        mismatch_found = True

            if mismatch_found:
                mismatch_list.append(entry)

            if len(kanji_name) == 0:
                match_failed.append(entry)
            else:
                for idx, each in enumerate(kanji_name):
                    seperated_name_list[idx]['kanji'] = each

                #continue

            entry.update({'separated':seperated_name_list})

    progressbar.complete()
    return (reading_english_fail, match_failed, mismatch_list)

def dump_name_data_to_file(data, file_name):
    dump_list = list(data.values())
    with open("Data Files/"+file_name, "w", encoding="UTF-8") as fp:
        json.dump(dump_list,fp)

def dump_download_name_data(data):
    output = list()
    for each in data:
        if "categories" in each:
            output.append({"categories": each["categories"],
                           "date_born": each["date_born"],
                           "date_death": each["date_death"],
                           "txt": each["txt"]})
    file_name = 'downloaded_names.json'
    with open("Data Files/"+file_name, "w", encoding="UTF-8") as fp:
        json.dump(output,fp)

def dump_list_to_file(data, file_name):
    with open("Data Files/"+file_name, "w", encoding="UTF-8") as fp:
        json.dump(data,fp)


def get_saved_data(file_name):
    with open("Data Files/"+file_name, encoding="UTF-8") as fp:
        dump_list = json.load(fp)
    output_dict = dict()
    for each in dump_list:
        key = None
        if isinstance(each, list):
            continue
        if len(each) > 0:
            if each[0].get('txt') is None:
                key = each[0].get('name')
            else:
                key = each[0]['txt']
        if key is not None:
            output_dict.update({key:each})
    return output_dict

def get_saved_name_data(file_name):
    with open("Data Files/"+file_name, encoding="UTF-8") as fp:
        dump_list = json.load(fp)
    output_dict = dict()
    for each in dump_list:
        output_dict.update({each['txt']:each})
    return output_dict


def get_neighborhood_names(loc_rdng_dict = None):
    filename = "zenkoku.csv"
    file = open(_data_location + filename)
    reader = csv.reader(file, delimiter = ',')
    # 1- "住所CD",2 - "都道府県CD", 3- "市区町村CD",4 - "町域CD", 5-"郵便番号",6-"事業所フラグ",7-"廃止フラグ",8-"都道府県",
    # 8- "都道府県カナ",9- "市区町村",10- "市区町村カナ",11- "町域",12- "町域カナ",13- "町域補足",14- "京都通り名",
    # 15- "字丁目",18- "字丁目カナ",19- "補足",20 -"事業所名",21- "事業所名カナ",22 - "事業所住所",23- "新住所CD"
    neighborhood_list = dict()
    last_char = dict()
    last_char_pair = dict()
    previous = ""
    previous_list = list()
    progress_bar_var = progress_bar(148449, "getting neighborhoods from file")

    #'郡', '局', '市','町','村','区'
    for row in reader:
        progress_bar_var.inc_counter()
        if len(row[11]) > 0:
            txt = row[11]
            rdng = convert2hira(row[12])
            if (neighborhood_list.get(txt)) is None:
                neighborhood_list.update({txt: {rdng:{"loc":[(row[7], convert2hira(row[8]), row[9], convert2hira(row[10]))], "links":[]}}})
            else:
                if neighborhood_list[txt].get(rdng) is None:
                    neighborhood_list[txt].update({rdng:{"loc":[(row[7], convert2hira(row[8]), row[9], convert2hira(row[10]))], "links":[]}})
                else:
                    if (row[7], convert2hira(row[8]), row[9], convert2hira(row[10])) not in neighborhood_list[txt][rdng]["loc"]:
                        neighborhood_list[txt][rdng]["loc"].append((row[7], convert2hira(row[8]), row[9], convert2hira(row[10])))
        # if len(row[15]) > 0:

        #    if (neighborhood_list.get(row[15])) is None:
        #        neighborhood_list.update({row[15]: [(row[7], row[9],row[16])]})
        #    else:
        #        if (row[7], row[9],row[16]) not in neighborhood_list[row[15]]:
        #            neighborhood_list[row[15]].append((row[7], row[9],row[16]))
    progress_bar_var.complete()
    # sorted_length = sorted(neighborhood_list, key = lambda x: len(x))
    # sorted_length = list(filter(lambda x: len(x) > 1, sorted_length))

    progress_bar_var = progress_bar(len(neighborhood_list), "compressing neighborhoods")
    for name in neighborhood_list.keys():
        if len(name) > 3:
            i = 2
            while i < len(name) - 1:
                if neighborhood_list.get(name[i:]) is not None and neighborhood_list.get(name[:i]) is not None:
                    right_reading = ""
                    left_reading = ""
                    for reading in neighborhood_list[name].keys():
                        right_reading = ""
                        left_reading = ""
                        for sub_reading in neighborhood_list[name[i:]].keys():
                            if sub_reading == reading[len(sub_reading)*-1:]:
                                right_reading = sub_reading
                                break
                        for sub_reading in neighborhood_list[name[:i]].keys():
                            if sub_reading == reading[:len(sub_reading)]:
                                left_reading = sub_reading
                                break
                        if len(left_reading) > 0 and len(right_reading) > 0:
                            neighborhood_list[name[i:]][right_reading]["links"].append((name, reading, neighborhood_list[name][reading]))
                            neighborhood_list[name[:i]][left_reading]["links"].append((name, reading, neighborhood_list[name][reading]))
                            break
                    if len(left_reading) > 0 and len(right_reading) > 0:
                        break
                i += 1
        progress_bar_var.inc_counter()

    progress_bar_var.complete()

    if loc_rdng_dict is not None:
        add_population_neigh_loc(neighborhood_list, loc_rdng_dict)

    progress_bar_var = progress_bar(len(neighborhood_list)+1, "reordering neighborhood list")
    location_list = list()
    for name, readings in neighborhood_list.items():
        progress_bar_var.inc_counter()
        for reading, data in readings.items():
            reading_locations = get_link_locations(data["links"])
            for each in data["loc"]:
                if each not in reading_locations:
                    if loc_rdng_dict is None or len(each) > 4:
                        reading_locations.append(each)
            location_list.append((name,reading,reading_locations))

    progress_bar_var.inc_counter()
    if loc_rdng_dict is None:
        location_list = sorted(location_list, key = lambda x: len(x[2]), reverse=True)
    else:
        location_list = sorted(location_list, key = lambda x: (len(x[2]),sum([y[4] for y in x[2]])), reverse=True)
    progress_bar_var.complete()
    return neighborhood_list, location_list

def get_todofuken_list():
    file_name = "todoufuken_list.txt"

    todofuken_list = list()
    todofuken_entry = {'txt': str(),
                               'rdng': str(),
                               'population': int(),
                               'area': int(),
                               'capital': str(),
                               'chihou': str()}

    with open(_data_location+file_name, encoding="UTF-8") as fp:

        reader = csv.reader(fp, delimiter = "\t")
        for row in reader:
            todofuken_entry = todofuken_entry.copy()
            #0-rdng	1-txt	2-flag	3-capital	4-largest_city	5-chihou	6-population	7-area	8-p/km	9-#_of_shichoson	iso/jis other
            todofuken_entry['rdng'] = row[0].strip("\ufeff")
            todofuken_entry['txt'] = row[1]
            todofuken_entry['capital'] = row[3]
            todofuken_entry['chihou'] = row[5]
            todofuken_entry['population'] = int(row[6].replace(',',''))
            todofuken_entry['area'] = float(row[7].replace(',',''))
            todofuken_list.append(todofuken_entry)

    return todofuken_list

def make_combined_location_names():
    # city, prefectures, towns, etc
    # "location":　['東京都'], "population": "1934675", "txt": "日本語", "area": "1121.26", "rdng": "とうきょう"}
    # load cities from file
    location_reading_dict = dict()
    with open("Data Files/locations.json", encoding="UTF-8") as fp:
        location_list = json.load(fp)

    adjusted_location_list = list()
    bad_list = list()

    city_corrections = {'色丹村': {'txt': '色丹村',
                                    'rdng': 'しこたんむら',
                                    'location': ['北海道','根室振興局'],
                                    'area':'253.33',
                                    'population': '1499'},
                        '丹羽郡': {'txt': '丹羽郡',
                                    'rdng': 'にわぐん',
                                    'location': ['愛知県'],
                                    'area':'24.8',
                                    'population': '57,106'},
                        '知多郡': {'txt': '知多郡',
                                    'rdng': 'ちたぐん',
                                    'location': ['愛知県'],
                                    'area':'165.43',
                                    'population': '163,572'},
                        '北設楽郡': {'txt': '知多郡',
                                    'rdng': 'ちたぐん',
                                    'location': ['愛知県'],
                                    'area':'165.43',
                                    'population': '163,572'},
                        '鹿児島郡': {'txt': '鹿児島郡',
                                    'rdng': 'かごしまぐん',
                                    'location': ['鹿児島県'],
                                    'area': '132.54',
                                    'population': '1,149'},
                        '曽於郡': {'txt': '曽於郡',
                                    'rdng': 'そおぐん',
                                    'location': ['鹿児島県'],
                                    'area': '100.67',
                                    'population': '13,144'}
                        }
    for each in location_list:
        if each['txt'] in city_corrections:
            each = city_corrections[each['txt']]
        if each['area'] is not None:
            adjusted_location_list.append(each)
            each.update({'type': 'city'})
            each['population'] = int(each['population'].replace(',',''))
            pattern = "([0-9|,|.]+)"
            each['area'] = float(re.match(pattern,each['area']).group().replace(',',''))
            local_list = location_reading_dict.setdefault(each['txt'],[])
            local_list.append(each)

    todofuken = get_todofuken_list()
    for each in todofuken:
        each.update({'type': 'todofuken'})
        local_list = location_reading_dict.setdefault(each['txt'],[])
        local_list.append(each)

    with open("Data Files/shrines.json", encoding="UTF-8") as fp:
        shrine_list = json.load(fp)
    for each in shrine_list:
        each.update({'type': 'shrine',
                     'txt': each['title']})
    with open("Data Files/temple_list.json", encoding="UTF-8") as fp:
        temple_list = json.load(fp)
    for each in temple_list:
        each.update({'type': 'temple',
                     'txt': each['title']})
    neighborhood_list, sorted_neighborhoods = get_neighborhood_names(location_reading_dict)
    neighborhoods = list()
    for each in sorted_neighborhoods:
        neigh_entry = {'txt':each[0], 'rdng':each[1], 'locations':each[2], 'type': 'neighborhood'}
        neighborhoods.append(neigh_entry)
    progress = progress_bar(1, "sorting name locations")
    sorted_locations = sorted(adjusted_location_list + shrine_list+ temple_list+ neighborhoods + todofuken,
                              key = lambda x: calculate_location_score(x), reverse=True)
    progress.complete()
    return sorted_locations

def get_pop_location(location_tuple, location_dict, failed_list=None):
    corrections = {'四日市市': ['四日市市'],
                   '田村市': ['田村市'],
                    '大和郡山市': ['大和郡山市'],
                   '高市郡高取町': ['高市郡', '高取町'],
                   '三宅島三宅村': ['三宅村'],
                   '田村郡三春町': ['田村郡','三春町'],
                   '高市郡明日香村': ['高市郡','明日香村'],
                   '余市郡余市町': ['余市郡','余市町'],
                   '田村郡小野町': ['田村郡','小野町'],
                   '武蔵村山市': ['武蔵村山市'],
                   '東村山市': ['東村山市'],
                   '余市郡赤井川村': ['余市郡','赤井川村'],
                   '愛知郡長久手町': ['愛知郡','長久手町'],
                   '八丈島八丈町': ['八丈町'],
                   '余市郡仁木町': ['余市郡','仁木町']}
    pattern = "([{}]+?[郡局市町村区])".format(_codeblock_kanji + 'ヶ'+ 'ケ' + _codeblock_hiragana + 'ノ')
    all_locations =[location_tuple[0]] + [split for split in re.split(pattern, location_tuple[2]) if split != '']

    if location_tuple[2] in corrections:
        all_locations = [location_tuple[0]] + corrections[location_tuple[2]]
    if len(all_locations) > 1:
        if len(all_locations[-1]) == 1:
            all_locations[-1] = all_locations[-2] + all_locations.pop()

        pop_location_list = location_dict.get(all_locations[-1])
        if pop_location_list is not None:
            for each in pop_location_list:
                if all_locations[0] in each['location']:
                    return each['population']
    if failed_list is not None:
        failed_list.add(location_tuple)
    return 0

def add_population_neigh_loc(neighborhood_dict, location_dict, failed_list = None):
    progress = progress_bar(len(neighborhood_dict), "adding population numbers to neighborhoods")
    for rdng_dict in neighborhood_dict.values():
        progress.inc_counter()
        for rdng_values in rdng_dict.values():
            new_loc_list = list()
            for loc in rdng_values['loc']:
                new_loc_list.append((loc[0],loc[1], loc[2],loc[3], get_pop_location(loc, location_dict, failed_list)))
            rdng_values['loc'] = new_loc_list
    progress.complete()

def calculate_person_score(person):
    # if person has categories sort for that first
    cat_score = len(person.get("categories",[])) > 0
    freq_score = sum([y['freq_link']['freq'] for y in person['separated']])/len(person['separated'])
    used_score = sum([y['freq_link']['used'] for y in person['separated']])/len(person['separated'])
    return cat_score, freq_score, used_score

def calculate_location_score(entry):
    #assume tuple type is neighborhood - return number of cities with this neighborhood name
    if type(entry) == tuple:
        return len(entry[2]), sum([x[4] for x in entry[2]])
    if entry.get('type') is not None:
        if entry['type'] == 'todofuken':
            return entry['population'], 1
        if entry['type'] == 'city':
            return entry['population'], 1
        if entry['type'] == 'shrine':
            return int(entry['founded'] != '') * 10 + 1, 1
        if entry['type'] == 'temple':
            return int(entry['founded'] != '') * 10 + 1, 1
        if entry['type'] == 'neighborhood':
            return len(entry['locations']), sum([x[4] for x in entry['locations']])
    return 0,0

def get_link_locations(links):
    location_list = list()
    for each in links:
        for loc in each[2]["loc"]:
            if loc not in location_list:
                location_list.append(loc)
        for loc in get_link_locations(each[2]["links"]):
            if loc not in location_list:
                location_list.append(loc)
    return location_list




