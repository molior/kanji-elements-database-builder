# README #


### What is this repository for? ###

* Kanji Elements Database Builder creates a database of kanji, vocabulary, and example sentences for the Kanji Elements app. The current output of this tool is not the same database as currently included with the Kanji Elements app. 

* Version - v0.95


### How do I get set up? ###

+ **Summary of set up**

    This project was built using Python 3.3 and 3.4, and PyCharm. Some files in included in the Data Files are not required for running tools but it is recommended to download all files to avoid errors.


* **Dependencies**
     * wchartype (included in package)
     * SQLALchemy-0.9.8
     * lxml-3.4.4
     * nltk-3.1
     * requests
     * romkan-0.2.1
 


* **How to run tests**

Running the KanjiElementsDatabase.py in will produce an sqlite database in the Database folder. 


### Who do I talk to? ###

* **Repo owner**

  Brian Foley brian.foley -at- gmail.com