__author__ = 'Brian'


import requests
import json
from lxml import html
html_address = 'http://www.kanjijiten.net/jiten/radical0*.html'
html_count = 3
encoding_corrections = {'�K':'黑',
                        '�瀦�': '羽',
                        '��':'青',
                        '禺':'禸',
                        '攵':'攴',
                        '│':'丨',
                        '旡':'无',
                        '彑':'彐',
                        '病':'疒',
                        'ツ':'小',
                        '之':'辵'}

def get_bushu():
    bushu = dict()

    count = 0
    while count <= html_count:
        count += 1
        page = requests.get(html_address.replace('*',str(count)))
        page.encoding = 'shift_jis'
        tree = html.fromstring(page.text)
        items = tree.xpath('//html/body/div[@id="contentgroup"]/div[@id="bushu"]/ol/li')
        for each in items:
            if each.text != '\n':
                if encoding_corrections.get(each.text.strip('部')) is not None:
                    bushu.update({encoding_corrections.get(each.text.strip('部')):
                                  tree.xpath('//html/body/div[@id="contentgroup"]/div[@id="bushu"]/ol/li[@id="'
                                             + each.attrib.get('id') + '"]/ul/li/a/text()')})
                else:
                    bushu.update({each.text.strip('部'):
                                  tree.xpath('//html/body/div[@id="contentgroup"]/div[@id="bushu"]/ol/li[@id="'
                                             + each.attrib.get('id') + '"]/ul/li/a/text()')})
    with open("Data Files/bushu_download.json", "w", encoding="UTF-8") as fp:
        json.dump(bushu,fp)


def load_bushu():
    bushu = dict()

    file = open("Data Files/bushu_download.json", encoding="UTF-8")

    bushu = json.load(file)   

    return bushu

def add_bushu_to_kanji(kanji_dict, radical_dict):
    bushu = load_bushu()
    kanji_not_found = list()
    for bushu_char, kanji_list in bushu.items():
        radical = radical_dict.get(bushu_char)
        for value in kanji_list:
            kanji = kanji_dict.get(value)
            if kanji is not None:
                kanji.update({'bushu':[(radical[0]['id'],radical[0]['char'], "web")]})
            else:
                kanji_not_found.append(value)
