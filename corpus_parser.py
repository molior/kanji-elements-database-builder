__author__ = 'BRIAN'

from JMDictParser import create_dict_from_jmdict_list, calculate_kanji_word_importantance, make_simple_meaning_from_entry
from jmdict_tools import does_entry_have_tags, does_word_have_kanji, is_entry_mostly_single_word_gloss, is_entry_usually_kana, \
    calculate_kanji_word_importantance, get_best_rdng, get_pos_list, calculate_sentence_importance_score
import re
from progressbar import progress_bar
import csv
from corpus_tools import does_word_have_kanji, get_ruby_word, simple_conjugator
from FuriganaGenerator import convert2hira

_data_location = 'Data Files/'
_tanaka_examples = 'examples_utf.txt'
_tanaka_subset = 'examples_s_utf.txt'


_tanaka_b_line_pattern = "(?P<word>[^~([{]+)(\((?P<dict_rdng>.+?)\))?" \
                         "([[](?P<sense>[0-9]{2})[]])?([{](?P<sent_rdng>.+?)[}])?(?P<pref>[~])?"

_corrections_dict = {'杭': '1279450',
                     '正気': '1580850',
                     '泡立つ': '1517550',
                     '早々': {'03': '1596420', '02':'2659710' },
                     '頭数': '1582320',
                     '一人子': '1576080',
                     '卸し': '1589530',
                     '叔父': '1607070',
                     '尤も': '1535810'}
def add_word_count_tanaka(jmdict_list):
    jmdict = create_dict_from_jmdict_list(jmdict_list, needs_furigana=False, needs_ime=False, include_readings=True)
    progress = progress_bar(149892, "adding word counts from tanaka corpus")
    words_cant_match = dict()
    word_not_found = dict()
    with open(_data_location+_tanaka_examples, encoding = 'utf-8') as file:
        line_a, line_b = file.readline(), file.readline()
        while line_a != '' or line_b != '':
            # skip line A, Example:
            # A: 「これはとてもおもしろそうだね」とひろしが言います。	"This looks pretty interesting," Hiroshi says.#ID=1514_4925
            # parse line B, example:
            # B: 此れ[01]{これ} は[01] 迚も[01]{とても}~ 面白い{おもしろ} そう[02] だ ね[01] と[04] が 言う{言います}
            # Entry: WORD   (DICTIONARY READING)    [MEANING ##]    {TEXT_IN_SENTENCE} ~Checked sentence
            progress.inc_counter()
            line_b_tokens = line_b.strip('B: ').strip('\n').split(' ')
            for token in line_b_tokens:
                if len(token) == 0:
                    continue
                result_dict = re.search(_tanaka_b_line_pattern, token)
                if result_dict is not None:
                    result_dict = result_dict.groupdict()
                else:
                    continue
                if does_word_have_kanji(result_dict['word']) and result_dict['word'] in jmdict:
                    if result_dict['dict_rdng'] is None and result_dict['sent_rdng'] is not None:
                        if not does_word_have_kanji(result_dict['sent_rdng']) and len(jmdict[result_dict['word']]) > 1:
                            result_dict['dict_rdng'] = result_dict['sent_rdng']

                    best_match = get_best_kanji(result_dict['word'],result_dict['sense'],
                                                result_dict['dict_rdng'], jmdict[result_dict['word']])
                    if best_match[0] is None:
                        if result_dict['word'] not in words_cant_match:
                            words_cant_match.update({result_dict['word']:[]})
                        words_cant_match[result_dict['word']].append((line_a, line_b))
                    # if result_dict['dict_rdng'] is not None:
                    #     for kanji, entry in jmdict[result_dict['word']]:
                    #         for kanji_rdng in kanji['kanji_rdng']:
                    #             if kanji_rdng['rdng']['txt'] == result_dict['dict_rdng']:
                    #                 kanji['freq']['tanaka'] += 1
                    #                 kanji_rdng['rdng']['freq']['tanaka'] += 1
                    #                 kanji_rdng['rdng']['freq']['tanak_total'] += 1
                    #                 break
                    #         kanji['freq']['tanaka_total'] += 1
                    # else:
                    #     kanji_idx = 0
                    #     entry_idx = 1
                    #     if len(jmdict[result_dict['word']]) > 1:
                    #         best_match = get_best_kanji(result_dict['word'], result_dict['sense'], jmdict[result_dict['word']])
                    #         if best_match is None:
                    #             if words_cant_match.get(result_dict['word']) is None:
                    #                 words_cant_match.update({result_dict['word']:[]})
                    #             words_cant_match[result_dict['word']].append((line_a, line_b))
                    #         else:
                    #             for each in jmdict[result_dict['word']]:
                    #                 each[kanji_idx]['freq']['tanaka_total'] += 1
                    #             best_match[kanji_idx]['freq']['tanaka'] += 1
                    #     else:
                    #         jmdict[result_dict['word']][0][kanji_idx]['freq']['tanaka'] += 1
                    #         jmdict[result_dict['word']][0][kanji_idx]['freq']['tanaka_total'] += 1
                elif result_dict['word'] not in jmdict:
                    if word_not_found.get(result_dict['word']) is None:
                        word_not_found.update({result_dict['word']:[]})
                    word_not_found[result_dict['word']].append((line_a, line_b))
            line_a, line_b = file.readline(), file.readline()
    progress.complete()

    return words_cant_match, word_not_found

def get_best_kanji(word, sense, dict_rdng, kanji_entry_list, add_tanaka=True, use_conjugations=False):
    # returns None if best can't be found
    kanji_idx = 0
    entry_idx = 1
    rdng_idx = 2
    score_idx = 3
    # if len(kanji_entry_list) == 1:
    #     return kanji_entry_list[kanji_idx], kanji_entry_list[entry_idx]

    sort_list = list()
    correction = _corrections_dict.get(word)
    if correction is not None:
        id = correction if type(correction) is str else correction.get(sense)
    else:
        id = None
    for kanji, entry in kanji_entry_list:
        kanji['freq']['tanaka_total'] += 1 if add_tanaka else 0
        if type(id) is str and id == entry['id']:
                sort_list.append((kanji,entry, get_best_rdng(kanji['kanji_rdng']), 20))

        if dict_rdng is not None and len(dict_rdng) > 0:
            dict_rdng = convert2hira(dict_rdng)
            for kanji_rdng in kanji['kanji_rdng']:
                kanji_rdng['rdng']['freq']['tanaka_total'] += 1 if add_tanaka else 0
                rdngs = {convert2hira(kanji_rdng['rdng']['txt'])}
                if use_conjugations:
                    pos_list = get_pos_list(entry)
                    rdngs = set(simple_conjugator(convert2hira(kanji_rdng['rdng']['txt']), pos_list))
                if dict_rdng in rdngs:
                    kanji_rdng['rdng']['freq']['tanaka'] += 1 if add_tanaka else 0
                    sort_list.append((kanji, entry, kanji_rdng, calc_kanji_rdng_score(kanji, entry)))
        else:
            # tag_score = int(not does_entry_have_tags(entry, ['arch'])) + int({'io'}.isdisjoint(set(kanji['misc'])))
            # uk_score = int(not is_entry_usually_kana(entry))
            # pri_score = kanji['pri'] * 3
            # has_ime = int(kanji['has_ime'])
            # multiple_word = 0 if len(entry['kanji']) > 1 else 1
            total = calc_kanji_rdng_score(kanji, entry)
            sort_list.append((kanji, entry, get_best_rdng(kanji['kanji_rdng']), total))

    sorted_list = sorted(sort_list, key = lambda x: x[score_idx], reverse=True)
    if len(sorted_list) == 0:
        if not use_conjugations:
            return get_best_kanji(word, sorted_list, dict_rdng, kanji_entry_list, add_tanaka, use_conjugations=True)
        else:
            return None, None, None
    elif len(sorted_list) == 1 or sorted_list[0][score_idx] != sorted_list[1][score_idx]:
        sorted_list[0][kanji_idx]['freq']['tanaka'] += 1 if add_tanaka else 0
        return sorted_list[0][kanji_idx], sorted_list[0][entry_idx], sorted_list[0][rdng_idx]
    return None, None, None

def calc_kanji_rdng_score(kanji, entry):
    tag_score = int(not does_entry_have_tags(entry, ['arch'])) + int({'io'}.isdisjoint(set(kanji['misc'])))
    uk_score = int(not is_entry_usually_kana(entry))
    pri_score = kanji['pri'] * 3
    has_ime = int(kanji['has_ime'])
    multiple_word = 0 if len(entry['kanji']) > 1 else 1
    return tag_score + uk_score + pri_score + has_ime + multiple_word

def make_tanaka_sentence_list(jmdict_list):
    sentence_count = 48390/2
    progress = progress_bar(sentence_count, "creating sentence dictionary.")
    jmdict = create_dict_from_jmdict_list(jmdict_list, needs_furigana=False, needs_ime=False, include_readings=True)
    sentence_list = list()
    words_cant_match = dict()
    word_not_found = dict()
    id_set = set()
    with open(_data_location+_tanaka_subset, encoding = 'utf-8') as file:
        line_a, line_b = file.readline(), file.readline()
        while line_a != '' or line_b != '':
            # skip line A, Example:
            # A: 「これはとてもおもしろそうだね」とひろしが言います。	"This looks pretty interesting," Hiroshi says.#ID=1514_4925
            # parse line B, example:
            # B: 此れ[01]{これ} は[01] 迚も[01]{とても}~ 面白い{おもしろ} そう[02] だ ね[01] と[04] が 言う{言います}
            # Entry: WORD   (DICTIONARY READING)    [MEANING ##]    {TEXT_IN_SENTENCE} ~Checked sentence
            progress.inc_counter()
            line_b_tokens = line_b.strip('B: ').strip('\n').split(' ')
            line_a_tokens = line_a.strip('A:').strip(' ').strip('\n').split('\t')
            if int(line_a_tokens[1].split("#ID=")[1].split('_')[1]) in id_set:
                print("Duplicate number:" + line_a_tokens[1].split("#ID=")[1].split('_')[1])
            id_set.add(int(line_a_tokens[1].split("#ID=")[1].split('_')[1]))

            sent_entry = {"id":int(line_a_tokens[1].split("#ID=")[1].split('_')[1]),
                          "jpn_sent": line_a_tokens[0],
                          "eng_sent": line_a_tokens[1].split("#ID=")[0],
                          "words": []}
            sentence_list.append(sent_entry)

            for token in line_b_tokens:
                word_entry = {"id": int(), "kanji_form": str(),
                              "sent_form": str(), "flagged": False,
                              "kanji_link": None, "rdng_link": None, 'freq': 0}
                if len(token) == 0:
                    continue
                if token.find("~") != -1:
                    word_entry['flagged'] = True
                result_dict = re.search(_tanaka_b_line_pattern, token)
                if result_dict is not None:
                    result_dict = result_dict.groupdict()
                else:
                    continue
                if does_word_have_kanji(result_dict['word']) and result_dict['word'] in jmdict:
                    # If sentence doesn't have dictionary reading set it to the sentence reading if the reading doesn't have kanji
                    if result_dict['dict_rdng'] is None and result_dict['sent_rdng'] is not None:
                        if not does_word_have_kanji(result_dict['sent_rdng']) and len(jmdict[result_dict['word']]) > 1:
                            result_dict['dict_rdng'] = result_dict['sent_rdng']

                    kanji, entry, kanji_rdng = get_best_kanji(result_dict['word'],result_dict['sense'],
                                                result_dict['dict_rdng'], jmdict[result_dict['word']], add_tanaka=False)

                    if kanji is not None:
                        sent_entry['words'].append(word_entry)
                        word_entry['kanji_link'] = (entry, kanji, kanji_rdng)
                        word_entry['id'] = entry['id']
                        word_entry['kanji_form'] = kanji['txt']
                        word_entry['sent_form'] = get_ruby_word(kanji['txt'],
                                                                kanji_rdng['rdng']['txt'],
                                                                result_dict['sent_rdng'] if result_dict['sent_rdng'] is not None else kanji['txt'])
                    else:
                        continue
                        #if result_dict['word'] not in words_cant_match:
                            #words_cant_match.update({result_dict['word']:[]})
                        #words_cant_match[result_dict['word']].append((line_a, line_b))

                elif result_dict['word'] in jmdict:
                    if len(jmdict[result_dict['word']]) == 1:
                        sent_entry['words'].append(word_entry)
                        word_entry['id'] = jmdict[result_dict['word']][0][1]['id']
                        word_entry['rdng_link'] = (jmdict[result_dict['word']][0][1],jmdict[result_dict['word']][0][0])
                        # word_entry['sent_form'] = result_dict['word']
                    else:
                        words_cant_match.setdefault(result_dict['word'], [])
                        words_cant_match[result_dict['word']].append((line_a, line_b))
                else:
                    if word_not_found.get(result_dict['word']) is None:
                        word_not_found.update({result_dict['word']:[]})
                    word_not_found[result_dict['word']].append((line_a, line_b))

            # add frequency score to each words (used for sorting sentences)
            for word in sent_entry['words']:
                word['freq'] = calculate_sentence_importance_score(sent_entry, word['kanji_form'])
            line_a, line_b = file.readline(), file.readline()
    progress.complete()
    return sentence_list, (words_cant_match, word_not_found)


def output_words_meaning_no_examples(jmdict_list, jpn_lemmas):
    output_kanji = list()
    progress = progress_bar(len(jmdict_list), "creating output file: no_examples.txt")
    for entry in jmdict_list:
        for kanji in entry['kanji']:
            if kanji['freq']['tanaka_total'] == 0 and jpn_lemmas.get(kanji['txt']) is None \
                    and not is_entry_mostly_single_word_gloss(entry):
                output_kanji.append((kanji,entry))
    output_kanji = sorted(output_kanji, key= lambda x: calculate_kanji_word_importantance(x[0],x[1]), reverse = True)

    with open("Data Files/no_examples.txt", mode="w", newline="", encoding="utf-8-sig") as f:
        fieldnames = ['kanji','meaning']
        writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t')
        writer.writeheader()
        for row in output_kanji:
            writer.writerow({'kanji':row[0], 'meaning':make_simple_meaning_from_entry(row[1])})




