__author__ = 'Brian'

_data_location = "Data Files/"
_function_word_files = ['EnglishAuxiliaryVerbs.txt',
                        'EnglishConjunctions.txt',
                        'EnglishDeterminers.txt',
                        'EnglishPrepositions.txt',
                        'EnglishPronouns.txt',
                        'EnglishQuantifiers.txt']

_function_word_dict = dict()
_tokenizer_dict = dict()
_stopwords = set()

from nltk.corpus import stopwords
from romkan import to_roma
from romkan import to_hiragana
from wchartype import is_asian, is_kanji


_ruby_dict = {'来る': [(['て','た','ま','なさい',''],'き'), (['る','れば'],'く'), ([],'こ')]}

def _get_default_ruby_rule(key):
    for rule, replacement in _ruby_dict[key]:
        if len(rule) == 0:
            return replacement

def _has_rule(rule_list, word):
    for rule in rule_list:
        if len(rule) == 0 and len(word) == 0:
            return True
        if len(rule) > 0 and word[:len(rule)] == rule:
            return True
    return False

def _get_replacement(ruby_dict_entry, word):
    for search_set, replacement in ruby_dict_entry:
            if _has_rule(search_set,  word):
                return replacement
    return str()

def get_ruby_word(kanji_dict_form, kanji_dict_rdng, rdng_in_sent, furigana_match=None):
    if not does_word_have_kanji(rdng_in_sent):
        return rdng_in_sent

    # special rules for words like 来る that change readings based on conjugation
    if kanji_dict_form in _ruby_dict and bool(rdng_in_sent):
        default_rule = _get_default_ruby_rule(kanji_dict_form)
        replacement = _get_replacement(_ruby_dict[kanji_dict_form], rdng_in_sent[1:])
        kanji_dict_rdng = default_rule + kanji_dict_rdng[1:] if len(replacement) == 0 else replacement + kanji_dict_rdng[1:]

    replacement_list = list()
    if furigana_match is None:
        idx_start = 0
        for char_k, char_r in zip(kanji_dict_form, kanji_dict_rdng):
            if char_k != char_r:
                break
            idx_start += 1
        idx_from_end = 0
        for char_k, char_r in zip(reversed(kanji_dict_form), reversed(kanji_dict_rdng)):
            if char_k != char_r:
                break
            idx_from_end -= 1
        if idx_from_end == 0:
            idx_from_end = None
        replacement_list.append((kanji_dict_form[idx_start:idx_from_end], kanji_dict_rdng[idx_start:idx_from_end]))
    else:
        #  Furigana Match structure
        #    {'char': kanji,
        #     'detail': detail,          # detail_entry list details: [txt, jy_status, type, appended]
        #     'okurigana': okurigana,
        #     'txt': txt_in_reading}
        kanji_form = ""
        reading_form = ""
        for match in reversed(furigana_match):
            if len(match['char']) > 0:
                kanji_form += match['char']
                reading_form += match['txt']
                if len(match['okurigana']) > 0:
                    replacement_list.append((kanji_form, reading_form))
                    kanji_form = ""
                    reading_form = ""
            elif len(kanji_form) > 0:
                replacement_list.append((kanji_form, reading_form))
                kanji_form = ""
                reading_form = ""
        if len(kanji_form) > 0:
            replacement_list.append((kanji_form, reading_form))

    if len(rdng_in_sent) == 0:
        rdng_in_sent = str(kanji_dict_form)
    current_idx = 0
    for kanji_form, reading_form in replacement_list:
        idx_start = rdng_in_sent[current_idx:].find(kanji_form)
        if idx_start == -1:
            return ''
        insert_text =  kanji_form + "{" + reading_form + "}"
        rdng_in_sent = rdng_in_sent.replace(kanji_form,insert_text,1)
        current_idx = idx_start + len(insert_text)

    return rdng_in_sent

def is_kanji_ditto(char):
    return is_kanji(char) or char == '々'

def does_word_have_kanji(word):
    for char in word:
        if is_kanji(char):
            return True
    return False



def simple_conjugator(word, pos, include_names=False,  previous_name=''):

    _nai, _nai_adj = ['ず'],  [('ない', ['nai'])]
    _masu = ['ます','ました','ましたら','ません','まして','なさい']
    _godan_nai = [('せる', ['seru']), ('れる',['reru'])]
    _godan_e, _godan_e_verb = ['ば'], [('る', ['v1'])]
    _te_past, _de_past = ['て','た', 'たら', 'たり'], ['で','だ','だら', 'だり']


    godan_verbs = [ 'v5u', 'v5t', 'v5r', 'v5r-i', 'v5s', 'v5k', 'v5g', 'v5n', 'v5m', 'v5b', 'v5k-s', 'v5u-s']
    godan_special = ['v5aru']
    neg_form_verb = ['v-masen']
    all_verbs = ['v-seru', 'v-reru', 'v1', 'vs-i', 'vk'] + godan_special + godan_verbs
    adj_all =  ['adj-i', 'adj-nai', 'adj-v-nai']
    adj_na = ['adj-na']
    adj_no = ['adj-no']

    conj_dict = { 'plain':
                        [{'add':'', 'replace':(0,''), 'types': all_verbs + ['v-masu']+ neg_form_verb},
                         {'add':'', 'replace':(0,''), 'types': adj_all },
                         {'add':('する',['vs-i']), 'replace':(0,''), 'types': ['vs']},
                         {'add':'な', 'replace':(0,''), 'types': adj_na },
                         {'add':'', 'replace':(0,''), 'types': adj_na },
                         {'add':'の', 'replace':(0,''), 'types': adj_no},
                         {'add':'ん', 'replace':(0,''), 'types': adj_no},
                         {'add':'', 'replace':(0,''), 'types': adj_no }],
                'past':
                        [{'add':'た', 'replace':(1,''), 'types': ['v1', 'vk','v-seru', 'v-reru']},
                         {'add':'た', 'replace':(1,'っ'), 'types': ['v5u','v5t','v5r','v5r-i','v5aru', 'v5k-s']},
                         {'add':'た', 'replace':(1,'i'), 'types': ['v5s', 'v-masu']},
                         {'add':'でした', 'replace':(0,''), 'types': neg_form_verb},
                         {'add':'た', 'replace':(1,'い'), 'types': ['v5k']},
                         {'add':'だ', 'replace':(1,'い'), 'types': ['v5g']},
                         {'add':'だ', 'replace':(1,'ん'), 'types': ['v5n','v5m','v5b']},
                         {'add':'た', 'replace':(2,'し'), 'types': ['vs-i']},
                         {'add':'た', 'replace':(1,'う'), 'types': ['v5u-s']},
                         {'add':'た', 'replace':(1,'かっ'), 'types': adj_all}],
                'conjunctive':
                        [{'add':'て', 'replace':(1,''), 'types': ['v1', 'vk','v-seru', 'v-reru']},
                         {'add':'て', 'replace':(1,'っ'), 'types': ['v5u','v5t','v5r','v5r-i','v5aru', 'v5k-s']},
                         {'add':'て', 'replace':(1,'i'), 'types': ['v5s', 'v-masu']},
                         {'add':'で', 'replace':(0,''), 'types': neg_form_verb},
                         {'add':'て', 'replace':(1,'い'), 'types': ['v5k']},
                         {'add':'で', 'replace':(1,'い'), 'types': ['v5g']},
                         {'add':'で', 'replace':(1,'ん'), 'types': ['v5n','v5m','v5b']},
                         {'add':'て', 'replace':(2,'し'), 'types': ['vs-i']},
                         {'add':'て', 'replace':(1,'う'), 'types': ['v5u-s']},
                         {'add':'て', 'replace':(1,'く'), 'types': adj_all},
                         {'add':'で', 'replace':(2,''), 'types': ['adv-v-nai']}],
                'provisional':
                        [{'add':'ば', 'replace':(1,'e'), 'types': all_verbs},
                         {'add':'なら', 'replace':(0,''), 'types': ['v-masu']+neg_form_verb},
                         {'add':'ならば', 'replace':(0,''), 'types': ['v-masu']+neg_form_verb},
                         {'add':'れば', 'replace':(1,'け'), 'types': ['adj-i','adj-nai']}],
                'potential':
                        [{'add':('る',['v-reru']), 'replace':(1,'e'), 'types': godan_verbs+godan_special},
                         {'add':('る',['v-reru']), 'replace':(1,'られ'), 'types': ['v1','vk']},
                         {'add':('る',['v-reru']), 'replace':(1,'れ'), 'types': ['v1','vk']},
                         {'add':('る',['v-reru']), 'replace':(2,'でき'), 'types': ['vs-i']},
                         {'add':'なら', 'replace':(0,''), 'types': ['v-masu']},
                         {'add':'ならば', 'replace':(0,''), 'types': ['v-masu']},
                         {'add':'れば', 'replace':(1,'け'), 'types': ['adj-i','adj-nai']}],
                'causative':
                        [{'add':('せる',['v-seru']), 'replace':(1,'a'), 'types': godan_verbs+godan_special},
                         {'add':('る',['v-seru']), 'replace':(1,'させ'), 'types': ['v1','vk']},
                         {'add':('る',['v-seru']), 'replace':(2,'させ'), 'types': ['vs-i']},
                         {'add':('る',['v-seru']), 'replace':(1,'くさせ'), 'types': ['adj-i','adj-nai']}],
                'passive':
                        [{'add':('れる',['v-reru']), 'replace':(1,'a'), 'types': godan_verbs+godan_special},
                         {'add':('る',['v-reru']), 'replace':(1,'られ'), 'types': ['v1','vk']},
                         {'add':('る',['v-reru']), 'replace':(2,'され'), 'types': ['vs-i']}],
                'volitional':
                        [{'add':'う', 'replace':(1,'o'), 'types': godan_verbs+godan_special},
                         {'add':'う', 'replace':(1,'よ'), 'types': ['v1','vk','v-seru']},
                         {'add':'う', 'replace':(1,'しょ'), 'types': ['v-masu']},
                         {'add':'う', 'replace':(2,'しよ'), 'types': ['vs-i']}],
                'negative-volitional':
                        [{'add':'まい', 'replace':(0,''), 'types': godan_verbs+godan_special+['vs-i','v-masu']},
                         {'add':'まい', 'replace':(1,''), 'types': ['v1','vk','v-seru']}],
                'imperative':
                        [{'add':'', 'replace':(1,'e'), 'types': godan_verbs},
                         {'add':'ろ', 'replace':(1,''), 'types': ['v1','v-seru']},
                         {'add':'い', 'replace':(1,''), 'types': ['vk']+godan_special},
                         {'add':'ろ', 'replace':(2,'し'), 'types': ['vs-i']},
                         {'add':'なさい', 'replace':(2,''), 'types': ['v-masu']},
                         {'add':'で', 'replace':(0,''), 'types': ['adj-v-nai']}],
                'negative-imperative':
                        [{'add':'な', 'replace':(0,''), 'types':  ['v-seru', 'v1', 'vs-i', 'vk'] + godan_verbs + godan_special},
                         {'add':'なさるな', 'replace':(2,''), 'types': ['v-masu']}],
                'conditional':
                        [{'add':'たら', 'replace':(1,''), 'types': ['v1', 'vk','v-seru', 'v-reru']},
                         {'add':'たら', 'replace':(1,'っ'), 'types': ['v5u','v5t','v5r','v5r-i','v5aru', 'v5k-s']},
                         {'add':'たら', 'replace':(1,'i'), 'types': ['v5s', 'v-masu']},
                         {'add':'でしたら', 'replace':(0,''), 'types': neg_form_verb},
                         {'add':'たら', 'replace':(1,'い'), 'types': ['v5k']},
                         {'add':'だら', 'replace':(1,'い'), 'types': ['v5g']},
                         {'add':'だら', 'replace':(1,'ん'), 'types': ['v5n','v5m','v5b']},
                         {'add':'たら', 'replace':(2,'し'), 'types': ['vs-i']},
                         {'add':'たら', 'replace':(1,'う'), 'types': ['v5u-s']},
                         {'add':'たら', 'replace':(1,'かっ'), 'types': adj_all}],
                'alternative':
                        [{'add':'たり', 'replace':(1,''), 'types': ['v1', 'vk','v-seru', 'v-reru']},
                         {'add':'たり', 'replace':(1,'っ'), 'types': ['v5u','v5t','v5r','v5r-i','v5aru', 'v5k-s']},
                         {'add':'たり', 'replace':(1,'i'), 'types': ['v5s', 'v-masu']},
                         {'add':'でしたり', 'replace':(0,''), 'types': neg_form_verb},
                         {'add':'たり', 'replace':(1,'い'), 'types': ['v5k']},
                         {'add':'だり', 'replace':(1,'い'), 'types': ['v5g']},
                         {'add':'だり', 'replace':(1,'ん'), 'types': ['v5n','v5m','v5b']},
                         {'add':'たり', 'replace':(2,'し'), 'types': ['vs-i']},
                         {'add':'たり', 'replace':(1,'う'), 'types': ['v5u-s']},
                         {'add':'たり', 'replace':(1,'かっ'), 'types': adj_all}],
                'continuative':
                        [{'add':'', 'replace':(1,''), 'types': ['v1', 'vk','v-seru', 'v-reru']},
                         {'add':'', 'replace':(1,'i'), 'types': godan_verbs},
                         {'add':'', 'replace':(1,'い'), 'types': godan_special},
                         {'add':'', 'replace':(2,'し'), 'types': ['vs-i']}],
                'formal':
                        [{'add':('ます',['v-masu']), 'replace':(1,''), 'types': ['v1', 'vk','v-seru', 'v-reru']},
                         {'add':('ます',['v-masu']), 'replace':(1,'i'), 'types': godan_verbs},
                         {'add':('ます',['v-masu']), 'replace':(1,'い'), 'types': godan_special},
                         {'add':('ます',['v-masu']), 'replace':(2,'し'), 'types': ['vs-i']}],
                'negative':
                        [{'add':('ない',['adj-v-nai']), 'replace':(1,''), 'types': ['v1', 'vk','v-seru', 'v-reru']},
                         {'add':('ない',['adj-v-nai']), 'replace':(1,'a'), 'types': godan_verbs + godan_verbs},
                         {'add':('ない',['adj-v-nai']), 'replace':(2,'し'), 'types': ['vs-i']},
                         {'add':('ません',neg_form_verb), 'replace':(2,''), 'types': ['v-masu']},
                         {'add':('ない',['adj-nai']), 'replace':(1,'く'), 'types': ['adj-i']}],
                'adverbial':
                        [{'add':'', 'replace':(1,'く'), 'types': ['adj-i']},
                         {'add':'に', 'replace':(0,''), 'types': adj_na},
                         {'add':'に', 'replace':(0,''), 'types': adj_no}]
                }

    conj_list = list()
    for each in pos:
        for name, rules in conj_dict.items():
            for rule in rules:
                if each in rule['types']:
                    end = -rule['replace'][0] if rule['replace'][0] != 0 else None
                    output_name = previous_name + '-' + name if len(previous_name) > 0 else name
                    if rule['replace'][1].islower(): # is lower returns false if not roma character
                        sound_shift = to_hiragana(to_roma(word[end])[0] + rule['replace'][1])
                        output_word = word[:end]+sound_shift
                    else:
                        output_word = word[:end]+rule['replace'][1]
                    if type(rule['add']) is tuple:
                        conj_list.extend(simple_conjugator(output_word+rule['add'][0], rule['add'][1],include_names, output_name))
                    else:
                        if not include_names:
                            conj_list.append(output_word+rule['add'])
                        else:
                            conj_list.append((output_name, output_word+rule['add']))
    return conj_list
    # conj_dict ={ 'seru' : {'te-past':(' ',_te_past, None), 'conjuct':(' ',_masu,None), 'neg': (' ',_nai, _nai_adj), 'imp': (' ',['ろ'],[('られる','reru')]), 'vol':(' ',['よう'], None)},
    #             'reru' : {'te-past':(' ',_te_past, None), 'conjuct':(' ',_masu,None), 'neg': (' ',_nai, _nai_adj), 'imp': (' ',['ろ']), 'vol':(' ', ['よう'], None)},
    #             'v1': {'te-past':(' ',_te_past, None), 'conjuct':(' ',_masu,None), 'neg': (' ',_nai, _nai_adj), 'imp': (' ',['ろ'],[('させる','seru'),('られる','reru')]), 'vol':(' ', ['よう'])},
    #             'v5u': {'te-past':('っ',_te_past, None), 'conjuct':('い',_masu,None), 'neg': ('わ',_nai, _nai_adj+_godan_nai), 'imp':('え',_godan_e), 'vol':('お',['う'])},
    #             'v5t': {'te-past':('っ',_te_past, None), 'conjuct':('ち',_masu,None), 'neg': ('た',_nai, _nai_adj+_godan_nai), 'imp':('て',_godan_e), 'vol':('と',['う'])},
    #             'v5r': {'te-past':('っ',_te_past, None), 'conjuct':('り',_masu,None), 'neg': ('ら',_nai, _nai_adj+_godan_nai), 'imp':('れ',_godan_e), 'vol':('ろ',['う'])},
    #             'v5r-i': {'te-past':('っ',_te_past, None), 'conjuct':('り',_masu,None), 'neg': (' ',['ない']), 'imp':('れ',_godan_e), 'vol':('ろ',['う'])},
    #             'v5aru': {'te-past':('っ',_te_past, None), 'conjuct':('い',_masu,None), 'neg': ('ら',_nai), 'imp':('い',[]), 'vol':'ろう'},
    #             'v5s': {'te-past':('し',_te_past, None), 'conjuct':('し',_masu,None), 'neg': ('さ',_nai, _nai_adj+_godan_nai), 'imp':('せ',_godan_e), 'vol':('そ',['う'])},
    #             'v5k': {'te-past':('い',_te_past, None), 'conjuct':('き',_masu,None), 'neg': ('か',_nai, _nai_adj+_godan_nai), 'imp':('け',_godan_e), 'vol':('こ',['う'])},
    #             'v5g': {'te-past':('い',_de_past, None), 'conjuct':('ぎ',_masu,None), 'neg': ('が',_nai, _nai_adj+_godan_nai), 'imp':('げ',_godan_e), 'vol':('ご',['う'])},
    #             'v5n': {'te-past':('ん',_de_past, None), 'conjuct':('に',_masu,None), 'neg': ('な',_nai, _nai_adj+_godan_nai), 'imp':('ね',_godan_e), 'vol':('の',['う'])},
    #             'v5m': {'te-past':('ん',_de_past, None), 'conjuct':('み',_masu,None), 'neg': ('ま',_nai, _nai_adj+_godan_nai), 'imp':('め',_godan_e), 'vol':('も',['う'])},
    #             'v5b': {'te-past':('ん',_de_past, None), 'conjuct':('び',_masu,None), 'neg': ('ば',_nai, _nai_adj+_godan_nai), 'imp':('べ',_godan_e), 'vol':('ぼ',['う'])},
    #             'v5k-s': {'te-past':('っ',_te_past, None), 'conjuct':('き',_masu,None), 'neg': ('か',_nai, _nai_adj+_godan_nai), 'imp':('け',_godan_e), 'vol':('こ',['う'])},
    #             'v5u-s': {'te-past':('う',_te_past, None), 'conjuct':('い',_masu,None), 'neg': ('わ',_nai, _nai_adj+_godan_nai), 'imp':('え',_godan_e), 'vol':('お',['う'])},
    #             'vs-i': {'te-past':('し ',_te_past, None), 'conjuct':('し　',_masu,None), 'neg': (' 　',['せず'], ('しない',['nai'])), 'imp':('　 ',['しろ','させる','される']), 'vol':('し ',['よう'])},
    #             'vk': {'te-past':('　',_te_past, None), 'conjuct':('　',_masu,None), 'neg': ('　',_nai, _nai_adj+_godan_nai), 'imp':('　',['い','れる','させる']), 'vol':(' ',['よう'])},
    #             'vs': {'add-suru': ('',simple_conjugator('する', ['vs-i']))},
    #             'adj-i': {'conj': (' ', ['く','ければ','かった','かったら']+ simple_conjugator("ない", ['nai']))},
    #             'nai': {'conj': (' ', ['く','ければ','かった','かったら'])}}

    #
    # conj_list = list()
    # conj_list.append(word)
    # for each in pos:
    #     rules = conj_dict.get(each)
    #     if rules is not None:
    #         for value in rules.values():
    #             for conj in value[1]:
    #                 conj_list.append(word[:-len(value[0])] + value[0].strip() + conj)
    # return conj_list


    # for each in pos:
    #     if each == 'v1':
    #         conj_list.append(word[:-1]+'た')  # Past
    #         conj_list.append(word[:-1]+'て')  # Te form
    #         #conj_list.append(word[:-1]+'れば')  # Conditional
    #         #conj_list.append(word[:-1]+'れる')  # Potential (v1 verb)
    #         conj_list.append(word[:-1])       # Plain form
    #         # conj_list.append(word[:-1]+'ず')
    #         # conj_list.append(word[:-1]+'ます')
    #         # conj_list.append(word[:-1]+'ません')
    #         # conj_list.append(word[:-1]+'まして')
    #         # conj_list.append(word[:-1]+'ました')
    #         # conj_list.append(word[:-1]+'なさい')   # Polite Imperative
    #         # conj_list.append(word[:-1]+'られる')
    #         # conj_list.append(word[:-1]+'ない')    # Negative
    #         # conj_list.append(word[:-1]+'な')  # Imperative Negative
    #         conj_list.append(word[:-1]+'よう')    # Volitional
    #         conj_list.append(word[:-1]+'ろ')  # Imperative
    #     if each == 'v5u':
    #         conj_list.append(word[:-1]+'った')  # Past
    #         conj_list.append(word[:-1]+'って')  # Te - form
    #         conj_list.append(word[:-1]+'た')
    #         conj_list.append(word[:-1]+'ち')
    #         conj_list.append(word[:-1]+'て')
    #         conj_list.append(word[:-1]+'とう')
    #     if each == 'v5r':
    #         conj_list.append(word[:-1]+'った')  # Past
    #         conj_list.append(word[:-1]+'って')  # Te- Form
    #         conj_list.append(word[:-1]+'ら')
    #         conj_list.append(word[:-1]+'り')
    #         conj_list.append(word[:-1]+'れ')
    #         conj_list.append(word[:-1]+'ろう')
    #     if each == 'v5r-i':
    #         conj_list.append(word[:-1]+'った')  # Past
    #         conj_list.append(word[:-1]+'って')  # Te- Form
    #         conj_list.append(word[:-1]+'ら')
    #         conj_list.append(word[:-1]+'り')
    #         conj_list.append(word[:-1]+'れ')
    #         conj_list.append(word[:-1]+'ろう')
    #     if each == 'v5aru':
    #         conj_list.append(word[:-1]+'った')  # Past
    #         conj_list.append(word[:-1]+'って')  # Te- Form
    #         conj_list.append(word[:-1]+'ら')
    #         conj_list.append(word[:-1]+'い')
    #         conj_list.append(word[:-1]+'れ')
    #         conj_list.append(word[:-1]+'ろう')
    #     if each == 'v5s':
    #         conj_list.append(word[:-1]+'した')
    #         conj_list.append(word[:-1]+'して')
    #         conj_list.append(word[:-1]+'さ')
    #         conj_list.append(word[:-1]+'せ')
    #         conj_list.append(word[:-1]+'そう')
    #     if each == 'v5k':
    #         conj_list.append(word[:-1]+'いた')
    #         conj_list.append(word[:-1]+'いて')
    #         conj_list.append(word[:-1]+'か')
    #         conj_list.append(word[:-1]+'き')
    #         conj_list.append(word[:-1]+'け')
    #         conj_list.append(word[:-1]+'こう')
    #     if each == 'v5g':
    #         conj_list.append(word[:-1]+'いだ')
    #         conj_list.append(word[:-1]+'いで')
    #         conj_list.append(word[:-1]+'が')
    #         conj_list.append(word[:-1]+'ぎ')
    #         conj_list.append(word[:-1]+'げ')
    #         conj_list.append(word[:-1]+'ごう')
    #     if each == 'v5n':
    #         conj_list.append(word[:-1]+'んだ')
    #         conj_list.append(word[:-1]+'んで')
    #         conj_list.append(word[:-1]+'な')
    #         conj_list.append(word[:-1]+'に')
    #         conj_list.append(word[:-1]+'ね')
    #         conj_list.append(word[:-1]+'のう')
    #     if each == 'v5m':
    #         conj_list.append(word[:-1]+'んだ')
    #         conj_list.append(word[:-1]+'んで')
    #         conj_list.append(word[:-1]+'ま')
    #         conj_list.append(word[:-1]+'み')
    #         conj_list.append(word[:-1]+'め')
    #         conj_list.append(word[:-1]+'もう')
    #     if each == 'v5b':
    #         conj_list.append(word[:-1]+'んだ')
    #         conj_list.append(word[:-1]+'んで')
    #         conj_list.append(word[:-1]+'ば')
    #         conj_list.append(word[:-1]+'び')
    #         conj_list.append(word[:-1]+'べ')
    #         conj_list.append(word[:-1]+'ぼう')
    #     if each == 'vs-i':
    #         conj_list.append(word[:-2]+'し')
    #         conj_list.append(word[:-2]+'した')
    #         conj_list.append(word[:-2]+'して')
    #         conj_list.append(word[:-2]+'でき')
    #         conj_list.append(word[:-2]+'せず')
    #         conj_list.append(word[:-2]+'され')
    #         conj_list.append(word[:-2]+'させ')
    #         conj_list.append(word[:-2]+'さし')
    #         conj_list.append(word[:-2]+'ささ')
    #         conj_list.append(word[:-1]+'れ')
    #     if each == 'v5k-s':
    #         conj_list.append(word[:-1]+'った')
    #         conj_list.append(word[:-1]+'って')
    #         conj_list.append(word[:-1]+'か')
    #         conj_list.append(word[:-1]+'き')
    #         conj_list.append(word[:-1]+'け')
    #         conj_list.append(word[:-1]+'こ')
    #     if each == 'vk':
    #         conj_list.append(word[:-1]+'た')
    #         conj_list.append(word[:-1]+'て')
    #         conj_list.append(word[:-1]+'れ')
    #         conj_list.append(word[:-1]+'ら')
    #         conj_list.append(word[:-1]+'さ')
    #         conj_list.append(word[:-1]+'よ')
    #         conj_list.append(word[:-1]+'な')
    #         conj_list.append(word[:-1]+'ま')
    #         conj_list.append(word[:-1]+'い')
    #     if each == 'adj-i':
    #         conj_list.append(word[:-1]+ 'く')
    #         conj_list.append(word[:-1]+ 'ければ')
    #         conj_list.append(word[:-1]+ 'かった')
    # return conj_list


def get_function_word_dict():
    global _function_word_dict
    if len(_function_word_dict) == 0:
        for file_name in _function_word_files:
            with open(_data_location+file_name) as f:
                line = f.readline().strip()
                while line != '':
                    if line[0] != '/' and _function_word_dict.get(line) is None:
                        _function_word_dict.update({line:''})
                    line = f.readline().strip()
    return _function_word_dict

def is_function_word(word):
    global _function_word_dict
    if len(_function_word_dict) == 0:
        get_function_word_dict()
    return _function_word_dict.get(word) is not None

def word_tokenizer_jpn_rev(text, jmdict_dict):
    max_length = len(max(jmdict_dict, key = len))
    current_idx = len(text)
    idx_range = max_length

    tokens = list()
    while current_idx > 0:
        if text[current_idx-idx_range:current_idx] in jmdict_dict:
            #print(text[current_idx:idx_range], current_idx, idx_range)
            tokens.append(text[current_idx-idx_range:current_idx])
            current_idx -= idx_range
            idx_range = min(current_idx, max_length)
        else:
            idx_range -= 1
            if idx_range == 0:
                tokens.append(text[current_idx - 1])
                current_idx -= 1
                idx_range = min(current_idx, max_length)
    tokens.reverse()
    return tokens

def tokenize_word(word):
    stop_words = get_stopwords()
    tokens = [token for token in word.split(" ") if token not in stop_words]
    return tokens

def word_tokenizer_jpn(text, jmdict_dict):
    max_length = len(max(jmdict_dict, key = len))
    current_idx = 0
    idx_range = current_idx + max_length

    tokens = list()
    while current_idx < len(text):
        if text[current_idx:current_idx+idx_range] in jmdict_dict:
            #print(text[current_idx:idx_range], current_idx, idx_range)
            tokens.append(text[current_idx:current_idx+idx_range])
            current_idx += idx_range
            idx_range = min(len(text) - current_idx, max_length)
        else:
            idx_range -= 1
            if idx_range == 0:
                tokens.append(text[current_idx])
                current_idx += 1
                idx_range = min(len(text) - current_idx, max_length)
    return tokens


def make_stopwords():
    global _stopwords
    _stopwords = set(stopwords.words("english"))

def get_stopwords():
    global _stopwords
    if len(_stopwords) == 0:
        make_stopwords()
    return _stopwords


def make_tokenizer_dict(jmdict_list):
    global _tokenizer_dict

    for entry in jmdict_list:
        has_uk = False
        for kanji in entry['kanji']:
            # look for most valid readings
            sorted_rdngs = sorted(kanji['kanji_rdng'], key = lambda x: (x['rdng']['has_ime'],x['rdng']['pri']), reverse=True)
            if len(sorted_rdngs) == 0:
                continue
            rdng = sorted_rdngs[0]['rdng']['txt']
            gloss_list, pos_list, has_uk = _get_definitions_kanjis(entry, kanji, dict())
            tokenizer_value_tuple = (rdng,gloss_list)
            conjugations = simple_conjugator(kanji['txt'], pos_list)
            for each in conjugations+[kanji['txt']]:
                kanji_entry = _tokenizer_dict.get(each)
                if kanji_entry is None:
                    _tokenizer_dict.update({each:[tokenizer_value_tuple]})
                else:
                    kanji_entry.append(tokenizer_value_tuple)
        if has_uk or len(entry['kanji']) == 0:
            for rdng in entry['rdng']:
                gloss_list, pos_list, has_uk = _get_definitions_kanjis(entry, dict(), rdng)
                tokenizer_value_tuple = (rdng['txt'],gloss_list)
                conjugations = simple_conjugator(rdng['txt'], pos_list)
                for each in conjugations+[rdng['txt']]:
                    kanji_entry = _tokenizer_dict.get(each)
                    if kanji_entry is None:
                        _tokenizer_dict.update({each:[tokenizer_value_tuple]})
                    else:
                        kanji_entry.append(tokenizer_value_tuple)

def get_gloss_list(entry, kanji_txt):
    return _get_definitions_kanjis(entry, kanji_txt, "", ignore_pos=True)

def _get_definitions_kanjis(entry, kanji, rdng, ignore_pos = False):
    sense_list = dict()
    pos_list = set()
    has_uk = False
    all_gloss_list = list()
    for sense in entry['sense']:
        if 'uk' in sense['misc']:
            has_uk = True
        use_this = False
        if len(kanji) > 0:
            if not bool(sense['kanji']) or kanji in sense['kanji']:
                use_this = True
        if len(rdng) > 0:
            if not bool(sense['rdng']) or rdng in sense['rdng']:
                use_this = True
        if use_this:
            gloss_list = list()
            for gloss in sense['gloss']:
                if len(gloss.split(' ')) == 1:
                    gloss_list.append([gloss])
                else:
                    tokens = gloss.lower().split(" ")
                    remaining_tokens = list()
                    for token in tokens:
                        if token.isalpha() and token not in get_stopwords():
                            remaining_tokens.append(token)
                    if len(remaining_tokens) == 0:
                        gloss_list.append(gloss.lower().split(" "))
                    else:
                        gloss_list.append(remaining_tokens)
            if ignore_pos:
                all_gloss_list.extend(gloss_list)
            else:
                for pos in sense['pos']:
                    pos_list.add(pos)
                    key = ''
                    if pos[0] == 'v':
                        key = 'v'
                    if pos[0] == 'n':
                        key = 'n'
                    if pos[0:3] == 'adj':
                        key = 'a'
                    if pos[0:3] == 'adv':
                        key = 'r'
                    pos_entry = sense_list.get(key)
                    if pos_entry is None:
                        sense_list.update({key: gloss_list})
                    else:
                        pos_entry.extend(gloss_list)

    if ignore_pos:
        return all_gloss_list
    else:
        return sense_list, pos_list, has_uk

