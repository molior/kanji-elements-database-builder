__author__ = 'Brian'

from sqlalchemy.schema import DropTable, CreateTable
from sqlalchemy import create_engine, Table, Column, Index, Integer, Boolean, String, ForeignKey, Unicode, PrimaryKeyConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import and_, bindparam
import json
import os
from jmdict_tools import calculate_kanji_word_importantance, calculate_sentence_importance_score, calculate_rdng_word_importance
from progressbar import progress_bar
import time

def get_engine():
    return create_engine('sqlite:///Database/kanji_elem.sqlite', echo=False)
    
def get_result(engine, sql, result):
    start = time.time()
    if result is not None:
        result.close()
    result = engine.execute(sql)
    print(time.time() - start)
    return result

def create_database_engine(Base):
    if not os.path.exists('Database'):
        os.makedirs('Database')
    engine = create_engine('sqlite:///Database/kanji_elem.sqlite', echo=False)
    if os.path.exists('Database/kanji_elem.sqlite'):
        print('Overwriting existing database named kanji_elem.sqlite')
        os.remove('Database/kanji_elem.sqlite')
    Base.metadata.create_all(engine)
    return engine

def create_database_engine_from_table(table_dict):
    if not os.path.exists('Database'):
        os.makedirs('Database')
    engine = create_engine('sqlite:///Database/kanji_elem.sqlite', echo=False)
    if os.path.exists('Database/kanji_elem.sqlite'):
        print('Overwriting existing database named kanji_elem.sqlite')
        os.remove('Database/kanji_elem.sqlite')
    for table_name, value in table_dict.items():
        stmt = str(CreateTable(value['table']))
        # if not value['rowid']:
        #     stmt += " WITHOUT ROWID"
        engine.execute(stmt)
    return engine

def insert_all(table_dict, eng):
    for table in table_dict.values():
        if len(table['data']) > 0:
            eng.execute(table['table'].insert(),table['data'])
            del table['data'][:]

def clear_tables(table_dict, engine):
    for table in table_dict.values():
        table['data'] = []
        engine.execute(DropTable(table['table']))
        engine.execute(CreateTable(table['table']))

def make_base_table_dict():
    Base = declarative_base()
    table_dict = {'char':
                    {'table':
                           Table('char', Base.metadata,
                                Column('_id', Integer, primary_key=True),
                                Column('txt', Unicode),
                                Column('heisig', Integer),
                                Column('heisig6', Integer),
                                Column('jlpt', Integer),
                                Column('status', String),
                                Column('rad', String),
                                Column('rad_id', String),
                                Column('rad_name', String),
                                Column('svg', String),
                                Column('svg_color', String),
                                Column('svg_rad_id', String),
                                Column('stroke_count', Integer)),
                       'data':[]},
                  'char_detail':
                      {'table':
                           Table('char_detail', Base.metadata,
                                Column('char_id', Integer, ForeignKey('char._id')),
                                Column('txt', String),
                                #Column('search_txt', String),
                                Column('type', String),
                                Column('freq', Integer),
                                Column('name_freq', Integer),
                                Index('char_detail_id','char_id')),
                       'data':[]},
                  'char_rad':
                      {'table':
                          Table('char_rad', Base.metadata,
                                Column('char_id', Integer, ForeignKey('char._id')),
                                Column('rad', String),
                                Column('stroke_count', Integer),
                                Column('svg_g', String),
                                Index('char_rad_id','char_id', 'rad')),
                       'data':[]},
              'ja_en':
                  {'table':
                       Table('ja_en', Base.metadata,
                              Column('entry_id', Integer, primary_key=True),
                              Column('freq', Integer)
                              #Column('sense', String),
                              ),
                   'data':[]},
              'ja_en_char':
                  {'table':
                       Table('ja_en_char', Base.metadata,
                              Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                              Column('char_id', String, ForeignKey('char._id')),
                              Column('details', String),
                              Column('is_example', String),
                             Index('ja_en_char_id', 'ja_en_id', 'char_id')
                              #Column('sense', String),
                              ),
                   'data':[]},
              'ja_en_kanji':
                  {'table':
                       Table('ja_en_kanji', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('txt', String),
                             Column('has_ime', Boolean),
                             Column('freq', Integer),
                             Column('is_example', Boolean),
                             Column('used_sent', Boolean),
                             Column('misc', String),
                             Index('ja_en_kanji_id', 'ja_en_id')
                             # Column('info', String),
                             # Column('pri', Integer),
                             # Column('freq_blog', Integer),
                             # Column('freq_news1', Integer),
                             # Column('freq_news2', Integer),
                             # Column('freq_novels', Integer),
                             # Column('freq_tanaka', Integer),
                             # Column('freq_tanaka_total', Integer),
                             ),
                   'data':[]},
              # 'ja_en_kanji_char':
              #     {'table':
              #          Table('ja_en_kanji_char', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('ja_en_kanji_txt', String),
              #                Column('ja_en_rdng_txt', String),
              #                Column('order_id', Integer),
              #                Column('char', Unicode, ForeignKey('char.char')),
              #                Column('freq', Integer),
              #                Column('char_detail', String),
              #                Column('rdng', String)),
              #      'data':[]},
              'ja_en_gloss':
                  {'table':
                       Table('ja_en_gloss', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('sense_id', Integer),
                             Column('gloss', String),
                             Index('ja_en_gloss_id','ja_en_id')),
                   'data': []},
              'ja_en_misc':
                  {'table':
                       Table('ja_en_misc', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('sense_id', Integer),
                             Column('misc', String),
                             Index('ja_en_misc_id','ja_en_id')),
                   'data': []},
              'ja_en_info':
                  {'table':
                       Table('ja_en_info', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('sense_id', Integer),
                             Column('info', String),
                             Index('ja_en_info_id','ja_en_id')),
                   'data': []},
              'ja_en_pos':
                  {'table':
                       Table('ja_en_pos', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('sense_id', Integer),
                             Column('pos', String),
                             Index('ja_en_pos_id','ja_en_id')),
                   'data': []},
              'ja_en_sense_kanji':
                  {'table':
                       Table('ja_en_sense_kanji', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('sense_id', Integer),
                             Column('kanji', String),
                             Index('ja_en_sense_kanji_id','ja_en_id')),
                   'data': []},
              'ja_en_sense_rdng':
                  {'table':
                       Table('ja_en_sense_rdng', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('sense_id', Integer),
                             Column('rdng', String),
                             Index('ja_en_sense_rdng_id','ja_en_id')),
                   'data': []},
              # 'ja_en_sense':
              #     {'table':
              #          Table('ja_en_sense', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('gloss', String),
              #                Column('misc', String),
              #                Column('info', String),
              #                Column('pos', String),
              #                Column('kanji', String),
              #                Column('rdng', String)),
              #      'data': []},
              'ja_en_rdng':
                  {'table':
                       Table('ja_en_rdng', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('txt', String),
                             Column('freq', Integer),
                             Column('has_ime', Integer),
                             Column('is_example', Boolean),
                             Column('used_sent', Boolean),
                             Column('kanji_rel', String),
                             Column('misc', String),
                             Index('ja_en_rdng_in','ja_en_id')),
                   'data': []},
              'ex_sent':
                  {'table':
                       Table('ex_sent', Base.metadata,
                             Column('sent_id', String, primary_key=True),
                             Column('ja_sent', String),
                             Column('en_sent', String),
                             Index('ex_sent_id', 'sent_id')
                             ),
                   'data': []},
              'ex_sent_char':{
                  'table': Table('ex_sent_char', Base.metadata,
                                 Column('sent_id', String, ForeignKey('ex_sent.sent_id')),
                                 Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                                 Column('char_id', Integer, ForeignKey('char._id')),
                                 Index('ex_sent_char_id','sent_id','ja_en_id','char_id')),
                  'data':[]
              },
              'ex_sent_words':
                  {'table':
                       Table('ex_sent_words', Base.metadata,
                             Column('sent_id', String, ForeignKey('ex_sent.sent_id')),
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('kanji_form', String),
                             Column('sent_form', String),
                             Column('freq', Integer),
                             Index('ex_sent_words_id', 'sent_id', 'ja_en_id')
                             ),
                   'data': []},
              'ex_name':
                  {'table':
                       Table('ex_name', Base.metadata,
                             Column('txt', String, primary_key=True),
                             Column('rdng', String),
                             Column('details_json', String),
                             Column('type', String),
                             Column('freq', Integer),
                             Index('ex_name_id', 'txt')),
                   'data':[]
                  },
              'ex_name_char':
                  {'table':
                       Table('ex_name_char', Base.metadata,
                             Column('txt', String, ForeignKey('ex_name.txt')),
                             Column('char_id', String, ForeignKey('char._id')),
                             Column('details', String),
                             Index('ex_name_char_id', 'txt','char_id')),
                   'data': []}
              }
    return Base, table_dict

def make_table_dict():
    Base = declarative_base()
    table_dict = {'char':
                    {'table':
                           Table('char', Base.metadata,
                                Column('_id', Integer, primary_key=True, autoincrement=False),
                                Column('txt', Unicode),
                                Column('heisig', Integer),
                                Column('heisig6', Integer),
                                Column('jlpt', Integer),
                                Column('status', String),
                                Column('rad', String),
                                Column('rad_id', String),
                                Column('rad_name', String),
                                Column('svg', String),
                                Column('svg_color', String),
                                Column('svg_rad_id', String),
                                Column('stroke_count', Integer)),
                       'rowid': True,
                       'data':[]},
                  'char_detail':
                      {'table':
                           Table('char_detail', Base.metadata,
                                Column('char_id', Integer, ForeignKey('char._id')),
                                Column('txt', String),
                                #Column('search_txt', String),
                                Column('type', Integer),
                                Column('freq', Integer),
                                Column('name_freq', Integer),
                                Index('char_detail_id','char_id')),
                       'rowid': False,
                       'data':[]},
                  'char_rad':
                      {'table':
                          Table('char_rad', Base.metadata,
                                Column('char_id', Integer, ForeignKey('char._id')),
                                Column('rad', String),
                                Column('stroke_count', Integer),
                                Column('svg_g', String),
                                Index('char_rad_id','char_id')),
                       'rowid': False,
                       'data':[]},
              'ja_en':
                  {'table':
                       Table('ja_en', Base.metadata,
                              Column('entry_id', Integer, primary_key=True, autoincrement=False),
                              Column('freq', Integer)
                              ),
                   'rowid': True,
                   'data':[]},
              'ja_en_char':
                  {'table':
                       Table('ja_en_char', Base.metadata,
                              Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                              Column('char_id', Integer, ForeignKey('char._id')),
                              Column('details', String),
                              Column('is_example', Integer),
                              Index('ja_en_char_id','ja_en_id', 'char_id')
                              ),
                   'rowid': False,
                   'data':[]},
              'ja_en_kanji':
                  {'table':
                       Table('ja_en_kanji', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('txt', String),
                             Column('has_ime', Boolean),
                             Column('freq', Integer),
                             Column('is_example', Boolean),
                             Column('used_sent', Boolean),
                             Column('misc', String),
                             Index('ja_en_kanji_id', 'ja_en_id')
                             ),
                   'rowid':False,
                   'data':[]},
              # 'ja_en_gloss':
              #     {'table':
              #          Table('ja_en_gloss', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('sense_id', Integer),
              #                Column('gloss', String),
              #                Index('ja_en_gloss_id','ja_en_id')),
              #      'data': []},
              # 'ja_en_misc':
              #     {'table':
              #          Table('ja_en_misc', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('sense_id', Integer),
              #                Column('misc', String),
              #                Index('ja_en_misc_id','ja_en_id')),
              #      'data': []},
              # 'ja_en_info':
              #     {'table':
              #          Table('ja_en_info', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('sense_id', Integer),
              #                Column('info', String),
              #                Index('ja_en_info_id','ja_en_id')),
              #      'data': []},
              # 'ja_en_pos':
              #     {'table':
              #          Table('ja_en_pos', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('sense_id', Integer),
              #                Column('pos', String),
              #                Index('ja_en_pos_id','ja_en_id')),
              #      'data': []},
              # 'ja_en_sense_kanji':
              #     {'table':
              #          Table('ja_en_sense_kanji', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('sense_id', Integer),
              #                Column('kanji', String),
              #                Index('ja_en_sense_kanji_id','ja_en_id')),
              #      'data': []},
              # 'ja_en_sense_rdng':
              #     {'table':
              #          Table('ja_en_sense_rdng', Base.metadata,
              #                Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
              #                Column('sense_id', Integer),
              #                Column('rdng', String),
              #                Index('ja_en_sense_rdng_id','ja_en_id')),
              #      'data': []},
              'ja_en_sense':
                  {'table':
                       Table('ja_en_sense', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('sense_id', Integer),
                             Column('gloss', String),
                             Column('misc', String),
                             Column('info', String),
                             Column('pos', String),
                             Column('kanji', String),
                             Column('rdng', String),
                             Index('ja_sense_id','ja_en_id')),
                   'rowid':False,
                   'data': []},
              'ja_en_rdng':
                  {'table':
                       Table('ja_en_rdng', Base.metadata,
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('txt', String),
                             Column('freq', Integer),
                             Column('has_ime', Integer),
                             Column('is_example', Boolean),
                             Column('used_sent', Boolean),
                             Column('kanji_rel', String),
                             Column('misc', String),
                             Index('ja_en_rdng_id','ja_en_id')),
                   'rowid':False,
                   'data': []},
              'ex_sent':
                  {'table':
                       Table('ex_sent', Base.metadata,
                             Column('sent_id', Integer, primary_key=True, autoincrement=False),
                             Column('ja_sent', String),
                             Column('en_sent', String)
                             ),
                   'rowid':True,
                   'data': []},
              'ex_sent_char':{
                  'table': Table('ex_sent_char', Base.metadata,
                                 Column('sent_id', Integer, ForeignKey('ex_sent.sent_id')),
                                 Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                                 Column('char_id', Integer, ForeignKey('char._id')),
                                 Index('ex_sent_char_id','sent_id','ja_en_id','char_id')),
                  'rowid':False,
                  'data':[]
              },
              'ex_sent_words':
                  {'table':
                       Table('ex_sent_words', Base.metadata,
                             Column('sent_id', Integer, ForeignKey('ex_sent.sent_id')),
                             Column('ja_en_id', Integer, ForeignKey('ja_en.entry_id')),
                             Column('kanji_form', String),
                             Column('sent_form', String),
                             Column('freq', Integer),
                             Index('ex_sent_words_id', 'sent_id', 'ja_en_id')
                             ),
                   'rowid':True,
                   'data': []},
              'ex_name':
                  {'table':
                       Table('ex_name', Base.metadata,
                             Column('name_id', Integer, primary_key=True, autoincrement=False),
                             Column('txt', String),
                             Column('rdng', String),
                             Column('detail_one', String),
                             Column('detail_two', String),
                             Column('detail_three', String),
                             Column('detail_four', String),
                             Column('type', String),
                             Column('freq', Integer),
                             ),
                   'rowid':False,
                   'data':[]
                  },
              'ex_name_char':
                  {'table':
                       Table('ex_name_char', Base.metadata,
                             Column('name_id', String, ForeignKey('ex_name.name_id')),
                             Column('char_id', String, ForeignKey('char._id')),
                             Column('details', String),
                             Index('ex_name_char_id','name_id','char_id')),
                   'rowid':False,
                   'data': []}
              }
    return Base, table_dict


def insert_ja_en_data(jmdict_list, entry_id_set, kanji_dict, table_dict, engine):
    progress = progress_bar(len(jmdict_list), "inserting ja_en dictionary data in to database: kanji_elem.sqlite")
    insert_at = 10000
    count = 0
    example_dict = dict()
    for char, value in kanji_dict.items():
        entry = set()
        examples = value.get('examples', [])
        for example in examples:
            entry.add(example[0]['id'])
        example_dict.update({char: entry})

    for entry in jmdict_list:
        progress.inc_counter()
        count += 1
        if entry['id'] == '' or entry['id'] is None or entry['id'] not in entry_id_set:
            continue


        table_dict['ja_en']['data'].append(
            {'entry_id': int(entry['id']),
             'freq': sum([round(calculate_kanji_word_importantance(kanji, entry),2) for kanji in entry['kanji']])})
        # 'sense': json.dumps([{'g':sense['gloss'], 'm': sense['misc'],
        #                            'i': sense['info'], 'p': sense['pos'],
        #                            'k': sense['kanji'], 'r': sense['rdng']} for sense in entry['sense']])})
        # for sense in entry['sense']:
        #
        sense_count = 0
        for sense in entry['sense']:
            sense_count += 1
            if 'ja_en_gloss' in table_dict:
                table_dict['ja_en_gloss']['data'].extend(
                    [{'ja_en_id': int(entry['id']), 'sense_id': sense_count, 'gloss': gloss} for gloss in sense['gloss']])
                table_dict['ja_en_misc']['data'].extend(
                    [{'ja_en_id': int(entry['id']), 'sense_id': sense_count, 'misc': misc} for misc in sense['misc']])
                table_dict['ja_en_pos']['data'].extend(
                    [{'ja_en_id': int(entry['id']), 'sense_id': sense_count, 'pos': pos} for pos in sense['pos']])
                table_dict['ja_en_info']['data'].extend(
                    [{'ja_en_id': int(entry['id']), 'sense_id': sense_count, 'info': info} for info in sense['info']])
                table_dict['ja_en_sense_kanji']['data'].extend(
                    [{'ja_en_id': int(entry['id']), 'sense_id': sense_count, 'kanji': kanji} for kanji in sense['kanji']])
                table_dict['ja_en_sense_rdng']['data'].extend(
                    [{'ja_en_id': int(entry['id']), 'sense_id': sense_count, 'rdng': rdng} for rdng in sense['rdng']])
            else:
                table_dict['ja_en_sense']['data'].append(
                    {'ja_en_id': int(entry['id']),
                     'sense_id': sense_count,
                     'gloss': '; '.join(sense['gloss']),
                     'pos': ', '.join(sense['pos']),
                     'info': '; '.join(sense['info']),
                     'misc': ', '.join(sense['misc']),
                     'kanji': ', '.join(sense['kanji']),
                     'rdng': ', '.join(sense['rdng'])})
        # 'misc': [],
        #            'pri': 0,
        #            'freq': _make_freq_entry(),
        #            'has_ime': False,
        # 'blogs':1, 'news':1, 'novel':1, 'mainichi':1, 'tanaka':0, 'tanaka_total':0
        # Table('ja_en_kanji', Base.metadata,
        #                      Column('ja_en_id', ForeignKey('ja_en.entry_id')),
        #                      Column('txt'),
        #                      Column('ime'),
        #                      Column('pri'),
        #                      Column('freq_blog'),
        #                      Column('freq_news1'),
        #                      Column('freq_news2'),
        #                      Column('freq_novel'),
        #                      Column('freq_tanaka'),
        #                      Column('freq_tanaka_total'),
        char_dict = dict()
        for kanji in entry['kanji']:
            table_dict['ja_en_kanji']['data'].append(
                {'ja_en_id': int(entry['id']),
                 'txt': kanji['txt'],
                 'has_ime': kanji['has_ime'],
                 'is_example': int(kanji.get('is_example', 0)),
                 'used_sent': int(kanji.get('used_sent', 0)),
                 #'pri': kanji['pri'],
                 #'freq_blog': kanji['freq']['blogs'],
                 #'freq_news1': kanji['freq']['mainichi'],
                 #'freq_news2': kanji['freq']['news'],
                 #'freq_novel': kanji['freq']['novel'],
                 #'freq_tanaka': kanji['freq']['tanaka'],
                 #'freq_tanaka_total': kanji['freq']['tanaka_total'],
                 # 'info': json.dumps(simple_matches(kanji)),
                 'freq': round(calculate_kanji_word_importantance(kanji, entry),2),
                 'misc': ', '.join(kanji['misc'])})


            # Column('ja_en_id', String, ForeignKey('ja_en.entry_id'), primary_key=True),
            # Column('ja_en_kanji_txt', String, ForeignKey('ja_en_kanji.txt'), primary_key=True),
            # Column('ja_en_rdng_txt', String, ForeignKey('ja_en_rdng.txt'), primary_key=True),
            # Column('order_id', Integer, primary_key=True),
            # Column('char', Unicode, ForeignKey('char.char')),
            # Column('freq', Integer),
            # Column('char_detail', String, ForeignKey('char_detail.txt')),
            # Column('rdng', String)),

            for kanji_rdng in kanji['kanji_rdng']:
                if bool(kanji_rdng['furigana_matches']):
                    order_id = 0
                    for match in kanji_rdng['furigana_matches'][0]:
                        # 'char': kanji,
                        # 'detail': detail,          # detail_entry list details: [txt, jy_status, type, appended]
                        # 'okurigana': okurigana,
                        # 'txt': txt_in_reading}

                        if match['char'] != '':
                            if match['char'] not in char_dict:
                                char_dict.update({match['char']:set()})
                            for detail in match['detail']:
                                char_dict[match['char']].add(detail['txt'])

        for char, details in char_dict.items():
            table_dict['ja_en_char']['data'].append(
                {'ja_en_id': int(entry['id']),
                 'char_id': ord(char),
                 'details': json.dumps(list(details)),
                 'is_example': entry['id'] in example_dict.get(char,set())})
                            # order_id += 1
                            # table_dict['ja_en_kanji_char']['data'].append(
                            #     {'ja_en_id': int(entry['id']),
                            #      'ja_en_kanji_txt': kanji['txt'],
                            #      'ja_en_rdng_txt': kanji_rdng['rdng']['txt'],
                            #      'order_id': order_id,
                            #      'char': match['char'],
                            #      'freq': 0,
                            #      'char_detail': json.dumps([detail['txt'] for detail in match['detail']]),
                            #      'rdng': match['txt']}
                            # )

        for rdng in entry['rdng']:
            table_dict['ja_en_rdng']['data'].append(
                {'ja_en_id': int(entry['id']),
                 'txt': rdng['txt'],
                 'has_ime': int(rdng['has_ime']),
                 'is_example': int(rdng.get('is_example',0)),
                 'used_sent': int(rdng.get('used_sent', 0)),
                 'freq': round(calculate_rdng_word_importance(rdng, entry), 2),
                 'kanji_rel': ', '.join(rdng['kanji']),
                 'misc': ', '.join(rdng['misc'])}
            )
        if count == insert_at:
            count = 0
            insert_all(table_dict, engine)
    insert_all(table_dict, engine)
    progress.complete()

def insert_ex_sent_data(sent_list, sent_ids, kanji_dict, table_dict, engine):
    insert_at = 500
    count = 0
    progress = progress_bar(len(sent_list), "inserting example sentence data in to database: kanji_elem.sqlite")

    example_dict = dict()
    for char, value in kanji_dict.items():
        entry = dict()
        examples = value.get('examples', [])
        for example in examples:
            entry.update({example[0]['id']: {'id': 0, 'freq': 0}})
        example_dict.update({char: entry})

    for sent in sent_list:
        count += 1
        progress.inc_counter()
        if sent['id'] not in sent_ids:
            continue
        table_dict['ex_sent']['data'].append(
                {'sent_id': sent['id'],
                 'ja_sent': sent['jpn_sent'],
                 'en_sent': sent['eng_sent']})

        for word in sent['words']:
            freq = round(calculate_sentence_importance_score(sent, word['kanji_form']),2)
            for char in word['sent_form']:
                if char in example_dict and word['id'] in example_dict[char] and example_dict[char][word['id']]['freq'] < freq:
                    example_dict[char][word['id']]['freq'] = freq
                    example_dict[char][word['id']]['id'] = sent['id']
            table_dict['ex_sent_words']['data'].append(
                    {'sent_id': sent['id'],
                     'ja_en_id': word['id'],
                     'kanji_form': word['kanji_form'],
                     'sent_form': word['sent_form'],
                     'freq': freq})



        if count == insert_at:
            count = 0
            insert_all(table_dict, engine)

    for char, entry in example_dict.items():

        for entry_id, values in entry.items():
            if values['id'] > 0:
                table_dict['ex_sent_char']['data'].append({'sent_id': values['id'],
                                                           'ja_en_id': entry_id,
                                                       'char_id': ord(char)})


    insert_all(table_dict, engine)
    progress.complete()

def insert_ex_name_data(ex_name_dict, table_dict, engine):
    insert_at = 500
    count = 0
    progress = progress_bar(len(ex_name_dict), "inserting name data in to database: kanji_elem.sqlite")
    name_id = 0
    name_types = {'todofuken': 1, 'city': 2, 'neighborhood': 3, 'temple': 4, 'shrine': 5, 'person': 6}
    column_types = {'todofuken': {'a': ('detail_one', None), # area
                                  'c': ('detail_two', None), #capital city
                                  'h': ('detail_three', None), #location
                                  'p': ('detail_four', None) #population
                                  },
                    'city': {'a': ('detail_one', None), #area
                              'l': ('detail_three', ''), #location - list needs to be joined
                              'p': ('detail_four', None), #populatioj
                             },
                    'neighborhood': {'l': ('detail_three', None), #location (most populated)
                                      't': ('detail_four', None) #total number of locations
                                     },
                    'temple': {'l': ('detail_three', ''), #location - list needs to be joined
                               'f': ('detail_four', None) #year founded
                               },
                    'shrine': {'l': ('detail_three', ''), #location - list needs to be joined
                               'f': ('detail_four', None) #year founded
                               },
                    'person': {'i': ('detail_one', ', '), # info - list needs to be joined
                                'e': ('detail_two', None), # english name
                                'c': ('detail_three', ', '), # categories - list needs to be joined
                                'b': ('detail_four', None), # birth - dictionary {y m d} needs to be combined needs to be combined with death
                                'd': ('detail_four', None) # death - dictionary {y m d} needs to be combined with birth
                               }
                    }
    for name, entry in ex_name_dict.items():
        progress.inc_counter()
        count += 1
        name_id += 1
        entry_type = "person" if type(entry['type']) == list else entry['type']


        columns = column_types[entry_type]
        data_dict = {'name_id': name_id,
                     'txt': name,
                      'rdng': entry['rdng'],
                     'type': name_types[entry_type],
                     'freq': entry['freq'],
                     'detail_one': None,
                     'detail_two': None,
                     'detail_three': None,
                     'detail_four': None}
        for detail_name, detail in entry['detail_json'].items():
            if entry_type == "person" and detail_name == 'b':
                date_list = list()
                date_list.append(_make_person_dates(entry['detail_json']['b']))
                date_list.append(_make_person_dates(entry['detail_json']['d']))
                data_dict[columns[detail_name][0]] = ','.join(date_list) if len(date_list[0]) > 0 or len(date_list[1]) > 0 else None
            elif detail_name != "d":
                data_dict[columns[detail_name][0]] = detail if columns[detail_name][1] is None else columns[detail_name][1].join(detail)
        table_dict['ex_name']['data'].append(data_dict)

        for char, details in entry['char'].items():
            table_dict['ex_name_char']['data'].append(
                    {'name_id': name_id,
                     'char_id': ord(char),
                     'details': json.dumps(list(details))})
        if count == insert_at:
            count = 0
            insert_all(table_dict, engine)
    insert_all(table_dict, engine)
    progress.complete()

def _make_person_dates(detail):
    detail_date = str(detail['y']) if detail['y'] is not None else ''
    detail_date += "."+str(detail['m']) if detail['m'] is not None else ''
    detail_date += "."+str(detail['d']) if detail['d'] is not None else ''
    return detail_date

def insert_char_data(kanji_dict, kanjivg_radicals, table_dict, engine):
    insert_at = 500
    count = 0
    progress = progress_bar(len(kanji_dict), "inserting char data in to database: kanji_elem.sqlite")
    name_dict = dict()

    detail_types = {'rdng_on':1, 'rdng_kun': 2, 'rdng_nanori': 3, 'meaning': 4, 'rdng_excp':5}

    for char, value in kanji_dict.items():
        if value.get('svg') is None:
            continue
        count += 1
        progress.inc_counter()
        table_dict['char']['data'].append(
            {'_id': ord(char),
             'txt': char,
             'heisig': value['heisig'],
             'heisig6': value['heisig6'],
             'jlpt': value['jlpt'],
             'status': value['grade'] ,
             'rad': value['radical']['char'],
             'rad_id': value['radical']['id'],
             'rad_name': json.dumps(value['radical']['name']),
             'svg': json.dumps(value['svg']) ,
             'svg_color': json.dumps(value['svg_color']),
             'svg_rad_id': value['svg_rad_id'],
             'stroke_count': value['stroke_count']})



        for detail in value['details']:
            if detail['type'] != 'rdng_excp':
                table_dict['char_detail']['data'].append(
                    {'char_id':ord(char),
                     'txt': detail['txt'],
                     'type': detail_types[detail['type']],
                     'freq': detail['freq'],
                     'name_freq': detail['name_freq']})

            # Create entries for example name tables
            name_list = detail.get('name_ex',[])[:5]
            name_list = name_list + detail.get('location_ex',[])[:max(10-len(name_list),0)]
            for name in name_list:
                name_entry = name_dict.setdefault(name['txt'], {'entry': name, 'name_chars': set()})
                name_entry['name_chars'].update(char)

        for radical, ids in value['rad_list'].items():
            table_dict['char_rad']['data'].append(
                {'char_id': ord(char),
                 'rad': radical,
                 'stroke_count': kanjivg_radicals[radical],
                 'svg_g': json.dumps(ids)})
        if count == insert_at:
            count = 0
            insert_all(table_dict, engine)
    insert_all(table_dict, engine)
    progress.complete()

def update_char_ja_en_examples(kanji_dict, table_dict, engine):

    progress = progress_bar(len(kanji_dict), "updating ja_en to char example data in database: kanji_elem.sqlite")
    for char, values in kanji_dict.items():
        progress.inc_counter()

        examples = values.get("examples", [])
        for example in examples:
            table_dict['ja_en_char']['data'].append({'en_id': example[0]['id'],
                                                     'c_id': ord(char),
                                                     'is_examples': 1})


    table = table_dict['ja_en_char']['table']  # type: Table
    engine.execute(table.update().\
                   where(and_(table.c.ja_en_id == bindparam('en_id'), table.c.char_id == bindparam('c_id'))).\
                   values(is_example=1), table_dict['ja_en_char']['data'])

    progress.complete()




