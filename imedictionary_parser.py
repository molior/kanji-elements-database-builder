__author__ = 'Brian'

from progressbar import progress_bar
import csv
from wchartype import is_kanji

# Mozc License
# Copyright 2010-2016, Google Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following disclaimer
#     in the documentation and/or other materials provided with the
#     distribution.
#   * Neither the name of Google Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from
#     this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#
# Anthy License
# GPLv3

_data_location = 'Data Files/IME_Dictionaries/'

_mozc_file = 'dictionary0#.txt'
_mozc_file_count = 10

_anthy_ut_file_list = [['01-ippan.txt',4],
                       ['10-jinmei.txt',4],
                       ['20-myouji.txt',5],
                       ['21-namae.txt',5],
                       ['ekimei.txt',3]]


def _make_entry(kanji = str(), rdng = str(), left = 0, right = 0):
    return {'kanji': kanji, 'rdng': rdng, 'left': left, 'right': right}


def _contains_kanji(string):
    for char in string:
        if is_kanji(char):
            return True
    return False
def parse_mozc_files():
    i = 0
    mozc_list = list()
    pbar = progress_bar(1637334,"Processing mozc ime...")

    while i < _mozc_file_count:
        file = open(_data_location + _mozc_file.replace('#',str(i)),encoding='UTF-8')
        reader = csv.reader(file,delimiter = '\t')
        for row in reader:
            if _contains_kanji(row[4]):
                mozc_list.append(_make_entry(row[4],row[0], right = 1))
            pbar.inc_counter()
        i += 1
    pbar.complete()
    return mozc_list


def parse_anthy_files():
    anthy_list = list()
    pbar = progress_bar(561749,"Processing anthy ime...")

    for each in _anthy_ut_file_list:
        file = open(_data_location + each[0], encoding='UTF-8')
        reader = csv.reader(file, delimiter = '\t')
        for row in reader:
            if _contains_kanji(row[each[1]]):
                anthy_list.append(_make_entry(row[each[1]],row[0], left = 1))
            pbar.inc_counter()

    pbar.complete()
    return anthy_list


def combine_ime_dicts():
    combined_list = list()


    mozc_list = sorted(parse_mozc_files(), key=lambda k: (k['kanji'], k['rdng']))
    anthy_list = sorted(parse_anthy_files(), key=lambda k: (k['kanji'], k['rdng']))
    used_anthy = list()
    pbar = progress_bar(len(mozc_list) + len(anthy_list), "Combining lists")
    output_dict = dict()
    for each in mozc_list:
        pbar.inc_counter()
        if output_dict.get(each['kanji']) is None:
            output_dict.update({each['kanji']:[each['rdng']]})
        else:
            if each['rdng'] not in output_dict[each['kanji']]:
                output_dict[each['kanji']].append(each['rdng'])
    for each in anthy_list:
        pbar.inc_counter()
        if output_dict.get(each['kanji']) is None:
            output_dict.update({each['kanji']:[each['rdng']]})
        else:
            if each['rdng'] not in output_dict[each['kanji']]:
                output_dict[each['kanji']].append(each['rdng'])
    pbar.complete()
    return output_dict
    # print("Combining lists...")
    # cur_idx = 0
    # length = len(anthy_list)
    # for each in mozc_list:
    #     found = False
    #     while cur_idx < length and anthy_list[cur_idx]['kanji'] <= each['kanji']:
    #         if anthy_list[cur_idx]['kanji'] == each['kanji'] and anthy_list[cur_idx]['rdng'] == each['rdng']:
    #             found = True
    #             each['left'] = 1
    #             used_anthy.append(anthy_list[cur_idx])
    #             combined_list.append(each)
    #             cur_idx += 1
    #             break
    #         combined_list.append(anthy_list[cur_idx])
    #         cur_idx += 1
    #     if not found:
    #         combined_list.append(each)
    #     pbar.inc_counter()
    #
    # while cur_idx < length:
    #     combined_list.append(anthy_list[cur_idx])
    #     cur_idx += 1
    #
    # print("\nProcessed {0} entries in {1} secs.".format(pbar.counter, pbar.get_time_elapsed()))
    # return combined_list


def create_ime_dict():
    # print("Creating dictionary...")
    # combined = sorted(combine_ime_dicts(), key=lambda k: (k['kanji'],k['rdng']))
    # output = dict()
    # prev = {'kanji': ''}
    # for each in combined:
    #     if prev['kanji'] == each['kanji']:
    #         if each['rdng'] not in  output[each['kanji']]:
    #             output[each['kanji']].append(each['rdng'])
    #     else:
    #         output.update({each['kanji']:[each['rdng']]})
    #         prev = each
    return combine_ime_dicts()

