from corpus_tools import is_function_word
from wchartype import is_kanji

__author__ = 'Brian'

def get_best_rdng(kanji_rdng_list):
    if len(kanji_rdng_list) == 1:
        return kanji_rdng_list[0]
    sorted_list = sorted(kanji_rdng_list,
                         key = lambda x: (x['rdng']['has_ime'],
                                          x['rdng']['pri'] + int(bool(x['furigana_matches'])),
                                          freq_score(x['rdng']['freq'])), reverse=True)
    return sorted_list[0]

def get_pos_list(entry):
    pos_list = list()
    for sense in entry['sense']:
        pos_list.extend(sense['pos'])
    return pos_list

def freq_score(freq_dict):
    frequency_score = (freq_dict['mainichi']*0.3 + freq_dict['novel'] *0.3 +
                       freq_dict['news']*0.2 + freq_dict['blogs']*0.2) * 2
    if freq_dict['tanaka_total'] != freq_dict['tanaka']:
        frequency_score *= freq_dict['tanaka']/freq_dict['tanaka_total']
        if frequency_score == 0:
            frequency_score = 2
    return frequency_score


def does_entry_have_tags(entry, tag_list):
    tag_set = set(tag_list)
    for kanji in entry['kanji']:
        if not tag_set.isdisjoint(set(kanji['misc'])):
            return True
    for rdng in entry['rdng']:
        if not tag_set.isdisjoint(set(rdng['misc'])):
            return True
    for sense in entry['sense']:
        if not tag_set.isdisjoint(set(sense['misc'])):
            return True
    return False



def does_word_have_kanji(word):
    for char in word:
        if is_kanji(char):
            return True
    return False


def does_kanji_word_have_furigana(kanji):
    for kanji_rdng in kanji['kanji_rdng']:
            if len(kanji_rdng['furigana_matches']) > 0:
                return True
    return False


def does_kanji_have_furigana(kanji_list):
    for kanji in kanji_list:
        if does_kanji_word_have_furigana(kanji):
            return True
    return False


def does_kanji_have_ime(kanji_list):
    for kanji in kanji_list:
        if kanji['has_ime']:
            return True
    return False


def does_kanji_have_duplicate(kanji_list, jmdict_dict):
    for kanji in kanji_list:
        if jmdict_dict.get(kanji['txt']) is not None:
            return True
    return False


def is_entry_usually_kana(entry):
    uk_count = 0
    if len(entry['kanji']) == 0:
        return True
    for sense in entry['sense']:
        if 'uk' in sense['misc']:
            uk_count += 1
    if uk_count/len(entry['sense']) > 0.5:
        return True
    return False


def is_entry_mostly_single_word_gloss(entry):
    total_gloss = 0
    single_word_gloss = 0
    for sense in entry['sense']:
        for gloss in sense['gloss']:
            total_gloss += 1
            if len(gloss.split(' ')) == 1:
                single_word_gloss += 1
            else:
                tokens = gloss.split(" ")
                remaining_tokens = list()
                for token in tokens:
                    if token.isalpha() and not is_function_word(token):
                        remaining_tokens.append(token)
                if len(remaining_tokens) == 1:
                    single_word_gloss += 1
    return single_word_gloss/total_gloss > 0.5


def simple_matches(kanji):
    output = list()
    for kanji_rdng in kanji['kanji_rdng']:
        for match_list in kanji_rdng['furigana_matches']:
            temp_list = list()
            for match in reversed(match_list):
                temp_list.append([match['char'],match['txt'],match['okurigana'], [detail['txt'] for detail in match['detail']]])
            output.append(temp_list)
    return output

def calculate_rdng_word_importance(rdng_entry, entry):
    frequence_score = freq_score(rdng_entry['freq'])
    preference_score = rdng_entry['pri'] * 5
    jlpt_score = (entry['jlpt'] > 0) * 5

    return frequence_score + preference_score + jlpt_score


def calculate_kanji_word_importantance(kanji_entry, entry, kanji_char=None, char_detail=None):
    """
    :param kanji_entry: jmdict kanji entry
    :param kanji_char: kanji char as text (not required)
    :return: importance score of kanji based on ime availability,
    frequency, kanji to char reading matching, preference markings,
    joyo and JLPT markings, and meaning relation (when kanji supplied)
    """
    ime_score = int(kanji_entry['has_ime']) * 20
    frequency_score = freq_score(kanji_entry['freq']) * 3
    kanji_char_score = int(does_kanji_word_have_furigana(kanji_entry)) * 5
    preference_score = kanji_entry['pri'] * 5

    jlpt_score = (entry['jlpt'] > 0) * 15
    joyo_score = kanji_entry['is_joyo_example'] * 20

    # char_meaning_score is 0 - 10, 0 for no relation in meaning
    meaning_score = 0
    if kanji_char is not None:
        meaning_score = kanji_entry['char_meaning_score'].get(kanji_char, 0) * 2
            #kanji_entry['char_meaning_score'][kanji_char] * 2 if kanji_char in kanji_entry['char_meaning_score'] else 0

    # reading score - 0 or 15 if reading is a perfect match to kun reading check add 10
    reading_score = 0
    if char_detail is not None:
        if char_detail['type'] == 'rdng_kun' or char_detail['type'] == 'rdng_excp':
            rdng_txt = char_detail['txt'].replace('.','')
            if rdng_txt in [rdng['rdng']['txt'] for rdng in kanji_entry['kanji_rdng']]:
                reading_score = 15

    total = ime_score + frequency_score + kanji_char_score+ preference_score + jlpt_score + joyo_score + meaning_score + reading_score
    return total



def calculate_sentence_importance_score(sentence, specific_kanji_form =''):
    total = 0
    for word in sentence['words']:
        if word['kanji_form'] == specific_kanji_form and word['flagged']:
            total += 20
        if word['kanji_link'] is not None:
            total += calculate_kanji_word_importantance(word['kanji_link'][1],word['kanji_link'][0])
        elif word['rdng_link'] is not None:
            total += calculate_rdng_word_importance(word['rdng_link'][1], word['rdng_link'][0])
    if len(sentence['words']):
        total = total/len(sentence['words'])
    return total