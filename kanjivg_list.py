__author__ = 'Brian'


from xml.etree.cElementTree import iterparse
import progressbar
from wchartype import is_kanji
import svg.path

_KANJI_FILE = "kanjivg.xml"
_KANJI_FILE_LOCATION = "Data Files/"
_namespace = '{http://kanjivg.tagaini.net}'

def _make_group_dict(xml_elem):
    element = xml_elem.attrib.get(_namespace+"element")
    position = xml_elem.attrib.get(_namespace+"position")
    radical = xml_elem.attrib.get(_namespace+"radical")
    variant = xml_elem.attrib.get(_namespace+"variant")
    phon = xml_elem.attrib.get(_namespace+"phon")
    part = xml_elem.attrib.get(_namespace+"part")
    number = xml_elem.attrib.get(_namespace+"number")
    trad_form = xml_elem.attrib.get(_namespace+"tradForm")
    rad_form = xml_elem.attrib.get(_namespace+"radicalForm")
    original = xml_elem.attrib.get(_namespace+"original")
    group_id = xml_elem.attrib.get("id")

    new_group = {'element': str(), 'position': str(), 'radical':str(),
                 'variant': str(), 'phon': str(), 'number':int(),
                 'trad_form': str(), 'part': str(), 'rad_form': str(),
                 'groups': list(), 'paths': list(), 'original': str(),
                 'id': str()}
    if element is not None:
        new_group['element'] = element
    if original is not None:
        new_group['original'] = original
    if number is not None:
        new_group['number'] = number
    if position is not None:
        new_group['position'] = position
    if radical is not None:
        new_group['radical'] = radical
    if variant is not None:
        new_group['variant'] = variant
    if phon is not None:
        new_group['phon'] = phon
    if part is not None:
        new_group['part'] = part
    if trad_form is not None:
        new_group['trad_form'] = trad_form
    if rad_form is not None:
        new_group['rad_form'] = rad_form
    if group_id is not None:
        new_group['id'] = group_id.split('-')[1] if len(group_id.split('-')) > 1 else 'g0'

    return new_group

def get_svg_groups_colors_radicals(kanji_svg):
    output_group = build_condensed_groups(kanji_svg['group'])
    output_radicals = {}
    output_main_rad = kanji_svg['rad_group_id']
    output_colors = []
    groups = get_groups(kanji_svg)
    for group in groups:
        if group['id'] != 'g0':
            rad = group['element'] if group['original'] == '' else group['original']
            if rad == '':
                continue
            if output_radicals.get(rad) is None:
                output_radicals.update({rad:[group['id']]})
            else:
                output_radicals[rad].append(group['id'])
    paths = get_paths(kanji_svg)
    paths = sorted(paths, key=lambda x: x['id'])
    last_color = ''
    color_dict = {'0':0, '54':1, '100':2,'200':3,'280':4}

    last_color = paths[0]['stroke'].strip('hsl(').split(',')[0]
    for path in paths:
        if last_color != path['stroke'].strip('hsl(').split(',')[0]:
            output_colors.append((path['id']-2,color_dict[path['stroke'].strip('hsl(').split(',')[0]]-1))
        last_color = path['stroke'].strip('hsl(').split(',')[0]
    output_colors.append((len(paths)-1, color_dict[paths[len(paths)-1]['stroke'].strip('hsl(').split(',')[0]]))
    return output_group, output_colors, output_main_rad, output_radicals




def build_condensed_groups(group):
    output_group = {'id': group['id'],
                    'g': [build_condensed_groups(g) for g in group['groups']],
                    'p': [{'d': p['d'], 't': p['type'], 'id':p['id']} for p in group['paths']]}
    return output_group



def get_radicals(kanji):
    groups = get_groups(kanji)
    rad_list = list()
    for each in groups:
        if len(each['radical']) > 0 or each['trad_form'] or each['rad_form']:
            radical = each['radical'] if len(each['radical']) > 0 else 'general'
            rad_list.append(each)
    return rad_list

def make_rad_list(kanjivg_list, radical, position = ''):
    output_list = list()
    for kanji in kanjivg_list:
        groups = get_groups(kanji)
        for each in groups:
            if (each['element'] == radical or each['original'] == radical) and len(each['radical']) > 0:
                if len(position) == 0:
                    output_list.append(kanji)
                elif each['position'] == position:
                    output_list.append(kanji)
    print("Found {0} matching kanji".format(len(output_list)))
    return output_list

def make_no_radical_list(kanjivg_list):
    no_rad_list = list()

    rad_dict_enhance = {'general': {'c':0,'list':[]},
                        'tradit': {'c':0,'list':[]},
                        'nelson': {'c':0,'list':[]},
                        'general_tradit': {'c':0,'list':[]},
                        'general_nelson':{'c':0,'list':[]},
                        'tradit_nelson':{'c':0,'list':[]},
                        'all_three':{'c':0,'list':[]},
                        'rad_form':{'c':0,'list':[]},
                        'trad_form':{'c':0,'list':[]}}

    for kanji in kanjivg_list:
        no_radical = True
        if is_kanji(kanji['txt']):
            groups = get_groups(kanji)
            rad_dict = {'general':False,
                        'tradit':False,
                        'nelson':False,
                        'trad_form':False,
                        'rad_form':False}
            for group in groups:
                if len(group['radical']) != 0:
                    rad_dict[group['radical']] = True
                    no_radical = False
                if group['trad_form'] == 'true':
                    no_radical = False
                    rad_dict['trad_form'] = True
                if group['rad_form'] == 'true':
                    no_radical = False
                    rad_dict['rad_form'] = True

            if rad_dict['general'] and rad_dict['tradit'] and rad_dict['nelson']:
                rad_dict_enhance['all_three']['c'] += 1
                rad_dict_enhance['all_three']['list'].append(kanji)
            elif rad_dict['tradit'] and rad_dict['nelson']:
                rad_dict_enhance['tradit_nelson']['c'] += 1
                rad_dict_enhance['tradit_nelson']['list'].append(kanji)
            elif rad_dict['general'] and rad_dict['tradit']:
                rad_dict_enhance['general_tradit']['c'] += 1
                rad_dict_enhance['general_tradit']['list'].append(kanji)
            elif rad_dict['general'] and rad_dict['nelson']:
                rad_dict_enhance['general_nelson']['c'] += 1
                rad_dict_enhance['general_tradit']['list'].append(kanji)
            elif rad_dict['tradit']:
                rad_dict_enhance['tradit']['c'] += 1
                rad_dict_enhance['tradit']['list'].append(kanji)
            elif rad_dict['general']:
                rad_dict_enhance['general']['c'] += 1
                rad_dict_enhance['general']['list'].append(kanji)
            elif rad_dict['nelson']:
                rad_dict_enhance['nelson']['c'] += 1
                rad_dict_enhance['nelson']['list'].append(kanji)

            if rad_dict['trad_form']:
                rad_dict_enhance['trad_form']['c'] += 1
                rad_dict_enhance['trad_form']['list'].append(kanji)
            if rad_dict['rad_form']:
                rad_dict_enhance['rad_form']['c'] += 1
                rad_dict_enhance['rad_form']['list'].append(kanji)

            if no_radical:
                no_rad_list.append(kanji)
    print("{0} kanji have no radicals.".format(len(no_rad_list)))
    for key,value in rad_dict_enhance.items():
        if key != 'other':
            print("{0} kanji have {1} radical.".format(value['c'], key))
    return (no_rad_list, rad_dict_enhance)

def get_groups(kanji):
    return get_subgroups(kanji['group'])

def get_subgroups(group):
    groups = list()
    groups.append(group)
    for each in group['groups']:
        groups += get_subgroups(each)
    return groups

def make_kanjivg_dict():
    kanji_list = list()

    progress = progressbar.progress_bar(6744, "importing kanji vg")
    f = open(_KANJI_FILE_LOCATION+_KANJI_FILE, encoding="UTF-8")

    context = iterparse(f, events=("end",))
    context = iter(context)

    event, root = next(context)
    max_g = 0
    max_kanji = None
    for event, elem in context:
        if elem.tag == "kanji":
            hexcode = elem.attrib.get('id')
            if hexcode is None:
                continue
            hexcode = hexcode.split('_')[1]
            kanji = {'txt': chr(int(hexcode,16)), 'group': None}
            #  Get base group
            for group in elem:
                if group.tag == "g":
                    base_group = _make_group_dict(group)
                    kanji['group'] = base_group
                    for sub_group in group:
                        recurse_groups_dict(sub_group, base_group)

            g_count = add_color_to_path(kanji)
            if max_g < g_count:
                max_g = g_count
                max_kanji = kanji

            kanji_list.append(kanji)
            progress.inc_counter()
    progress.complete()
    print("{0}, {1}".format(max_g, max_kanji['txt']))
    return kanji_list

def get_paths_group(group):
    paths = group['paths'] + [paths['paths'] for paths in get_subgroups(group)]
    return paths

def make_kanjivg_radical_dict(kanjivg_list):
    kanjivg_radical_dict = dict()
    for kanjivg in kanjivg_list:
        groups = get_groups(kanjivg)
        for group in groups:
            if group['id'] != 'g0':
                rad = group['original'] if group['original'] != '' else group['element']
                stroke_count = 0
                if group['part'] == '':
                    stroke_count = len(group['paths'] + [each['paths'] for each in get_subgroups(group)])
                if rad != '' and rad not in kanjivg_radical_dict:
                    kanjivg_radical_dict.update({rad:stroke_count})

    not_found_rads = list()


    for rad in kanjivg_radical_dict.keys():
        not_found = True
        for kanjivg in kanjivg_list:
            if kanjivg['txt'] == rad:
                not_found = False
                kanjivg_radical_dict[rad] = len(get_paths(kanjivg))
                break
        if not_found:
            not_found_rads.append(rad)

    return kanjivg_radical_dict, not_found_rads



def recurse_groups_dict(elem, parent_group):
    if elem.tag == "g":
        new_group = _make_group_dict(elem)
        parent_group['groups'].append(new_group)
        for each in elem:
            recurse_groups_dict(each, new_group)
    if elem.tag == "path":
        path = {"id": int(), "d": str(), "type": str()}
        path['id'] = int(elem.attrib.get("id").split("-")[1].strip("s"))
        parsed_path = svg.path.parse_path(elem.attrib.get("d"))
        path['d'] = parsed_path.d()
        path['type'] = elem.attrib.get(_namespace+"type")
        parent_group['paths'].append(path)

def get_paths(kanji):
    paths = list()
    groups = get_groups(kanji)
    for group in groups:
        paths += group['paths']
    return paths

def unique_stroke_types(kanjivg_list):
    output_list = dict()
    missing_type = list()
    for kanji in kanjivg_list:
        if is_kanji(kanji['txt']) is False:
            continue
        paths = get_paths(kanji)
        string = str()
        for path in paths:
            if path['type'] is not None:
                string += path['type']
            else:
                string += ' '
                if kanji['txt'] not in missing_type:
                    missing_type.append(kanji['txt'])
        if output_list.get(string) is None:
            output_list.update({string:[kanji['txt']]})
        else:
            output_list[string].append(kanji['txt'])
    return (output_list, missing_type)

def no_element_groups_list(kanjivg_list):
    output_list = list()
    counted_list = list()
    for kanji in kanjivg_list:
        groups = get_groups(kanji)
        for group in groups:
            if group['element'] == '':
                if len(group['paths']) > 0:
                    output_list.append((kanji['txt'],kanji,group['id'],group))
                    if kanji['txt'] not in counted_list:
                        counted_list.append(kanji['txt'])
                    break
                for sub_group in group['groups']:
                    if sub_group['element'] == '':
                        output_list.append((kanji['txt'],kanji,group['id'],group))
                        if kanji['txt'] not in counted_list:
                            counted_list.append(kanji['txt'])
                        break

    print("{0} characters have groups without elements".format(len(counted_list)))
    print("{0} groups don't have elements.".format(len(output_list)))
    return output_list

def add_color_to_path(kanjivg):
    g_list = []

    hue = ['0', '54', '100','200','280']
    sat = ['100%','97%','100%','100%', '100%']

    g_list = build_glist(0,kanjivg['group'])
    path_count = 0
    lum = 0

    g_count = 0
    for g in g_list:
        lum = 0
        change = 50
        # group must have more than stroke
        if len(g['paths']) - 1 > 0:
            change = 50/(len(g['paths'])-1)

        g['paths'] = sorted(g['paths'], key = lambda x: x['id'])
        for path in g['paths']:
            lum += change
            stroke =  'hsl(' + hue[g_count] +','+\
                      sat[g_count] +','+ str(int(lum))+'%)'
            path.update({'stroke': stroke})
        g_count += 1

    return g_count

def print_svg(kanjivg):
    groups = get_groups(kanjivg)
    stroke_width = 'stroke-width = "3"'
    fill = 'fill = "none"'
    stroke_linecap = 'stroke-linecap = "round"'
    print("<g {0} {1} {2}>".format(stroke_width,fill,stroke_linecap))
    for group in groups:
        for path in group['paths']:
            string = '<path ' + " ".join([key + '="' + str(value) + '"' for key,value in path.items()]) + '/>'
            print("\t" + string)
    print("</g>")


def build_glist(level, group):

    g_list = []
    pathInLvl = False
    combine = False
    if len(group['paths']) > 0:
        g_list += [{'paths':group['paths']}]
        pathInLvl = True
    for sub_group in group['groups']:
        if len(sub_group['part']) > 0:
            combine = True
        g_list += build_glist(level+1, sub_group)

    for g in g_list:
        if level > 1 or pathInLvl or combine:
            combine = True
            break
        if level == 1 and len(g['paths']) < 3:
            combine = True
            break
        if level == 0 and len(g['paths']) < 2:
            combine = True
            break

    if combine:
        new_g = {'paths':[]}
        for g in g_list:
            new_g['paths'] += g['paths']
        g_list = [new_g]

    return g_list