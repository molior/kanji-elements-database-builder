import os
import sys
import time
import csv
import gc
import operator
from sqlalchemy import create_engine, Table, Column, Integer, Boolean, String, ForeignKey, Unicode
from sqlalchemy.dialects.sqlite import REAL
from sqlalchemy.orm import relationship, backref, scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from xml.etree.cElementTree import iterparse, tostring
from wchartype import *

KANJIVG_PATH = '../data/kanjivg.xml'

Base = declarative_base()
engine = create_engine('sqlite:///mobilekanji.sqlite', echo = False)

charsvg = Table('charsvg', Base.metadata,
                Column('char_id', Integer, ForeignKey('char.id')),
                Column('svg', String))

class char(Base):
    __tablename__ = 'char'

    id = Column(Integer, primary_key = True)
    txt = Column(Unicode)
    stroke_count = Column(Integer)
    heisig = Column(Integer)
    jlpt = Column(String)

char_list = []

elemString = []
data = {'part_high': int(),
            'number_high': int(),
            'part_count':0,
            'number_count':0}

kanjivg = {}
one_elem = []
engine.execute('DROP TABLE IF EXISTS charsvg')
Base.metadata.create_all(engine)




def process_kanjivg():

    global data
    f = open(KANJIVG_PATH, encoding = 'UTF-8')

    match = False
    count = 0
    count_id = 0
    count_g = 0
    count_g_max = 4
    t0 = time.time()
    
    #get context
    context = iterparse(f, events = ("end",))
    context = iter(context)
    #get root element
    event, root = next(context)
    
    
    first_child = 'None'
    kanji = str()

    hue = ['0', '60', '100','200','280']
    sat = ['100%','100%','100%','100%', '100%']

    insert = []
    
    for even, elem in context:
        if elem.tag == "kanji":           
            count += 1
            count_g = 0
            match = False
            g_list = []
            found = {'part': False,
                    'number': False}
            if elem.attrib.get('id') is not None:
                kanji = str(elem.attrib.get('id')[-4:])
                kanjivg.update({kanji: {'orig': tostring(elem, encoding = 'unicode'), 'new':str(), 'gcount': 0, 'pathcount': 0}})
            #    if(elem.attrib.get('id') == 'kvg:kanji_078ba'):
            #        elemString.extend([tostring(elem, encoding = 'unicode')])
            #        one_elem.append([elem])
            #        match = True
            #for child in elem:
            #    if child.tag == "g":
            #        for subchild in child:
            #            if subchild.tag == "g":
            #                count_g +=1
            #if count_g == count_g_max:
            #    char_list.extend([tostring(elem, encoding = 'unicode')])
            #for child in elem:
            #    recurse_group(child, data, found)
            for child in elem:
                g_list += build_glist(0, child)

            
            string = ''
            g_count = 0
            path_count = 0
            lum = 0
            string = '<g>\n'
            for g in g_list:
                string += '<g>\n'

                lum = 0
                change = 50
                if len(g['paths']) > 0:
                    change = 50/(len(g['paths']))
                
                for path in g['paths']:                    
                    path_count += 1
                    string += '\t<path d="'
                    string += path                    
                    string += '" stroke = "hsl(' + hue[g_count] +','+\
                              sat[g_count] +','+ str(int(lum)) + \
                              '%)" stroke-width="5" stroke-linecap="round" fill="none"/>\n'
                    lum += change
                string += '</g>\n'
                g_count += 1
            string += '</g>'
                      
            kanjivg[kanji]['new'] = string
            kanjivg[kanji]['gcount'] = g_count
            kanjivg[kanji]['pathcount'] = path_count

            #engine.execute(charsvg.insert(), {'char_id': int(kanji,16), 'svg': string})
            if len(engine.execute('SELECT id FROM char WHERE id = ' + str(int(kanji,16))).fetchall()) > 0:
                insert += [{'char_id': int(kanji,16), 'svg': string}]
            
            if found['part']:
                data['part_count'] +=1
            if found['number']:
                data['number_count'] +=1
            root.clear()

            
    engine.execute(charsvg.insert(),insert)
    print (count, len(char_list))
    #print (elemString)

def recurse_group(elem, data, found):
    
    if elem.get('{kvg}part') is not None:       
        found['part'] = True
        if int(elem.attrib.get('{kvg}part')) > data['part_high']:
            data['part_high'] = int(elem.attrib.get('{kvg}part'))
    if elem.attrib.get('{kvg}number') is not None:
        found['number'] = True
        if int(elem.attrib.get('{kvg}number')) > data['number_high']:
            data['number_high'] = int(elem.attrib.get('{kvg}number'))
    #recurse to next level
    for child in elem:        
        recurse_group(child ,data ,found)

def c2hex(char):
    return(str(hex(ord(char)))[-4:])

def build_glist(level, elem):

    g_list = []    
    pathInLvl = False
    combine = False
    for child in elem:
        if child.tag == 'path':
            g_list += [{'paths':[child.get('d')]}]
            pathInLvl = True          
        if child.tag == 'g':
            if child.attrib.get('{kvg}part') is not None:
                combine = True
            g_list += build_glist(level+1, child)

    

    for g in g_list:
        if level > 1 or pathInLvl or combine:
            combine = True
            break
        if level == 1 and len(g['paths']) < 3:
            combine = True
            break
        if level == 0 and len(g['paths']) < 2:
            combine = True
            break
        
    if combine:
        new_g = {'paths':[]}
        for g in g_list:
            new_g['paths'] += g['paths']
        g_list = [new_g]

    return g_list
            
    

    
        
    
