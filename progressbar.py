__author__ = 'Brian'

import time

class progress_bar:
    """ Display progress using periods
    """
    def __init__(self, total_entries, title = ""):
        self.total_entries = total_entries
        self.counter = 0
        self.percentage_count = 0
        self.percentage_interval = 0.02
        self.start = time.time()
        self.title = title
        print("Started {0}...".format(title))

    def inc_counter(self):
        self.counter += 1
        while self.counter/self.total_entries > self.percentage_count:
                self.percentage_count += self.percentage_interval
                print (end ='.')
                if int(self.percentage_count*100) % 10 == 0:
                    print("{0}%".format(int(self.percentage_count * 100)), end = '')

    def get_count(self):
        return self.counter

    def complete(self):
        print("\nFinished {0} in {1} secs".format(self.title, self.get_time_elapsed()))

    def get_time_elapsed(self):
        return round(time.time() - self.start, ndigits=2)

