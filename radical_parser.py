__author__ = 'Brian'

import csv
import progressbar
from kanjivg_list import *
from wchartype import is_kanji

_DATA_LOCATION = "Data Files/"
_FILE_NAME = "Radicals.txt"


def make_radical_dict():
    progress = progressbar.progress_bar(346, 'making radical list')
    f = open(_DATA_LOCATION+_FILE_NAME, encoding='UTF-8')

    reader = csv.reader(f,dialect='excel-tab')
    radical_dict = dict()
    for row in reader:
        progress.inc_counter()
        id = row[0].split('.')[0]
        char = row[1]
        if len(char) == 0 or char == ' ':
            print ("Id - {0} - missing char.".format(id))
        name = row[2].split(',')
        location = row[3]
        if radical_dict.get(id) is None:
            radical_dict.update({id:[{'id': id,
                                      'char': char,
                                      'name':name,
                                      'location': location}],
                                 char: [{'id': id,
                                         'char':char,
                                         'name':name,
                                         'location': location}]})
        else:

            base_char = radical_dict[id][0]['char']
            for each in radical_dict[base_char]:
                if each['char'] == char and each['name'] == name:
                    print (base_char)
            radical_dict[base_char].append({'id': id,
                                      'char': char,
                                      'name':name,
                                      'location': location})
            radical_dict[id].append({'id': id,
                                      'char': char,
                                      'name':name,
                                      'location': location})
    progress.complete()
    return radical_dict

# def add_bushu_to_kanji(bushu, kanji_dict, rad_dict):
#     missing_kanji = list()
#     no_match = list()
#     for key, value in bushu.items():
#         if rad_dict.get(key) is None:
#                     no_match.append(key)
#         for kanji_rad in value:
#             kanji = kanji_dict.get(kanji_rad)
#             if kanji is None:
#                 missing_kanji.append(kanji_rad)
#                 continue
#             kanji.update({'dict_rad':key})
#     print("Missing kanji: ".format(missing_kanji))
#     print("No matches: ".format(no_match))
#     return (no_match, missing_kanji)

def match_radical_to_kanjivg(kanjivg_list, radical_dict):
    progress = progressbar.progress_bar(len(kanjivg_list),'matching radical to kanjivg')
    output_dict = {'no_matches': list(),
                   'no_reduction': list(),
                   'multi_reduction': list(),
                   'one_reduction': list()}
    for kanji in kanjivg_list:
        progress.inc_counter()
        if is_kanji(kanji['txt']):
            kvg_rads = get_radicals(kanji)
            for rad_group in kvg_rads:
                if len(rad_group['original']) > 0:
                    output = _match_radical(radical_dict,rad_group['original'], rad_group, kanji)
                    output_dict[output[0]].append(output[1])

                    ## State one zero in matching_radicals
                        ## error - collect kanji and radical
                    ## State two zero in reduced_list
                        ## set radical name to original 'O'
                    ## State three multiple in reduced_list
                        ## error - collect kanji and radical
                    ## State four one in reduced_list
                        ## set radical name to found radical
                elif len(rad_group['element']) > 0:
                    output = _match_radical(radical_dict, rad_group['element'], rad_group, kanji)
                    output_dict[output[0]].append(output[1])

    print ("No matches - {0}".format(len(output_dict['no_matches'])))
    print ("No reductions - {0}.".format(len(output_dict['no_reduction'])))
    print ("Multi reductions - {0}.".format(len(output_dict['multi_reduction'])))
    print ("One match - {0}".format(len(output_dict['one_reduction'])))
    progress.complete()
    return output_dict

def _match_radical(radical_dict, rad, rad_group, kanji):
    correction_dict = {'氷': '冫',
                    'ト': '卜',
                    'つ': '小',
                    '彑': '彐',
                    '母': '毋',
                    '辶': '辵',
                    '黒': '黑',
                    '攵': '攴',
                    '巳': '己',
                    '旡': '无'}

    position_dict = {'left': 'L',
                     'right': 'R',
                     'top': 'T',
                     'bottom': 'B',
                     'nyo': 'E',
                     'tare': 'E',
                     'kamae': 'E',
                     'kamae1': 'E'}

    pair_correction = {'八': '儿',
                       '水': '冫',
                       '土': '士'}

    if pair_correction.get(rad) is not None and rad_group['element'] == pair_correction.get(rad):
        rad = pair_correction.get(rad)
    radical = radical_dict.get(rad)
    if radical is None:
        radical = radical_dict.get(correction_dict.get(rad))
    if radical is None:
        print("{0} radical in {1} not found.".format(rad, kanji['txt']))
    matching_radical = list()
    for each in radical:
        if each['char'] == rad_group['element']:
            matching_radical.append(each)
    if len(matching_radical) > 1:
        reduced_list = list()
        for each in matching_radical:
            if each['location'] == position_dict.get(rad_group['position']):
                reduced_list.append(each)
        if len(reduced_list) == 0:
            rad_group.update({'rad_id':radical[0]['id'], 'rad_link': matching_radical[0]})
            return ('no_reduction', (matching_radical[0],rad_group, kanji))
        if len(reduced_list) == 1:
            rad_group.update({'rad_id':radical[0]['id'], 'rad_link': reduced_list[0]})
            return ('one_reduction', (reduced_list[0], rad_group, kanji))
        if len(reduced_list) > 1:
            return ('multi_reduction', (reduced_list, rad_group, kanji))
    if len(matching_radical) == 1:
        rad_group.update({'rad_id':radical[0]['id'], 'rad_link': matching_radical[0]})
        return ('one_reduction', (matching_radical[0], rad_group, kanji))
    return ('no_matches', (rad_group, kanji))

def feedback_process(matches_output, key):
    lines = 30
    count = 0
    for each in matches_output[key]:
        if key == 'no_matches':
            print ('Element {0} of original {1} not found in {2}'
                   .format(each[0]['element'],each[0]['original'], each[1]['txt']))
        elif key == 'multi_reduction':
            print ('Multiple matches {0} for element {1}/{2} in {3}'
                   .format(','.join(each[0]),each[1]['element'],each[1]['original'], each[2]['txt']))
        else:
            print ('Possible match - {0} for element {1}/{2} in {3}'
                   .format(each[0],each[1]['element'],each[1]['original'], each[2]['txt']))
        count += 1
        if count == lines:
            count = 0
            user_input = input("Press [ENTER] to continue or [Q] to quit")
            if user_input in ['q','Q']:
                break

def find_radical_kanjivg(kanjivg, radical_id, radical_dict):
    groups = get_groups(kanjivg)
    new_radical = None
    radical_list = [rad['char'] for rad in radical_dict[str(radical_id)]]
    for group in groups:
        if group['element'] in radical_list or group['original'] in radical_list:
            new_radical = group
            new_radical['radical'] = 'adjusted'
            radical = new_radical['original'] if new_radical['original'] in radical_list else new_radical['element']
            result = _match_radical(radical_dict, radical_dict[str(radical_id)][0]['char'], new_radical, kanjivg)
            # if result[0] != "one_reduction":
                # print ("{0}, {1}".format(result[0], kanjivg['txt']))
            break
    return new_radical

def manual_adjust_radical_kanjivg(kanjivg, kanji, radical_id, radical_dict):
    print("\n"*2)
    print("-"*20)
    print("\n {0}.....Web-{1},{2}\tUni-{3},{4}".format(kanji['txt'],kanji['bushu'][0][0],kanji['bushu'][0][1],
                                                     kanji['uni_rads'][0][1], radical_dict[kanji['uni_rads'][0][1]][0]))
    print("")
    groups = get_groups(kanjivg)
    for idx, group in enumerate(groups):
        print("{0} - Element: {1}, Original: {2}".format(idx, group['element'], group['original']))
    usr_input = input("\nSkip (S), (Radical Index, Element[1]/Original[2], New Character:  ")
    if usr_input.lower() != "s":
        index = usr_input.split(",")[0]
        if len( usr_input.split(",")) > 1:
            change =  usr_input.split(",")[1]
            new =  usr_input.split(",")[2]
            if change == 1:
                groups[index]['element'] = new
            else:
                groups[index]['original'] = new
        if len( usr_input.split(",") ) > 3:
            change =  usr_input.split(",")[1]
            new =  usr_input.split(",")[2]
            if change == 1:
                groups[index]['element'] = new
            else:
                groups[index]['original'] = new
        groups[index]['radical'] = 'adjusted'
        return {kanjivg['txt']:{'group_id':groups[index]['id'], 'element':groups[index]['element'], 'original':groups[index]['original']}}
    return