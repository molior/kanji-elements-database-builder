__author__ = 'Brian'


from nltk.corpus import stopwords
from progressbar import progress_bar

_data_location = "Data Files/"
_file = "mobythes.aur"


def make_thesaurus_list():
    progress = progress_bar(30260, "creating thesaurus list from file.")
    stopword_set = set(stopwords.words("english"))
    output_list = list()
    output_dict = dict()
    with open(_data_location+_file) as f:
        lines = f.readlines()
        for line in lines:
            progress.inc_counter()
            lemmas = line.split(',')
            word_set = set(lemmas)
            # for words in lemmas:
            #    word_list = set(words.split(" ")).difference(stopword_set)
            #    if len(word_list) == 1:
            #       word_set.update(word_list)
            if lemmas[0] not in output_dict:
                output_dict.update({lemmas[0]:word_set})
            else:
                output_dict[lemmas[0]].update(word_set)
            # for word in word_set:
            #     if word not in output_dict:
            #         output_dict.update({word:word_set})
            #     else:
            #         output_dict[word].update(word_set)
            #output_list.append(word_set)
    progress.complete()
    return output_dict