__author__ = 'BRIAN'

from nltk.stem import WordNetLemmatizer
from progressbar import progress_bar

def add_meaning_sets_kanji(kanji_dict, thesaurus_dict):
    pos_types = ['a','v','n','r']
    thesaurus_idx = 1
    meaning_link_idx = 0
    thesaurus_modifier = 0.75
    wn_lem = WordNetLemmatizer()
    progress = progress_bar(len(kanji_dict), "creating extending meanings for kanji characters.")
    for char, values in kanji_dict.items():
        progress.inc_counter()
        values.update({'meaning_sets':dict()})
        for meaning in [meanings for meanings in values['details'] if meanings['type'] == 'meaning']:
            values['meaning_sets'].update({meaning['txt'].lower(): (meaning, set())})
            for pos in pos_types:
                if wn_lem.lemmatize(meaning['txt'].lower(), pos) != meaning['txt']:
                    values['meaning_sets'].update({meaning['txt'].lower(): (meaning, set())})
        for meaning, meaning_tuple in values['meaning_sets'].items():
            if meaning in thesaurus_dict:
                meaning_tuple[thesaurus_idx].update(thesaurus_dict[meaning])
    progress.complete()