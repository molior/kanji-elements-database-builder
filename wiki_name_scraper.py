import json
import re
import sys
from lxml import html
import requests
from progressbar import progress_bar

__author__ = 'Brian'


_data_location = "Data Files/"
_codeblock_hiragana = '\u3040-\u309f'
_codeblock_katakana = '\u30a0-\u30ff'
_codeblock_katakana_half = '\uff66-\uff9f'
_codeblock_kanji = '\u4E00-\u9FFF|\u3005'

def get_wiki_pages(name_dict):

    output_list = list()
    progress = progress_bar(len(name_dict.keys()),title="get wiki pages")
    for name in name_dict.keys():
        progress.inc_counter()
        html_path = "https://ja.wikipedia.org/wiki/"
        page = requests.get("{0}{1}".format(html_path, name))
        page.encoding = 'UTF-8'
        tree = html.fromstring(page.text)
        items = tree.find_class("noarticletext")
        if len(items) == 0:
            output_list.append({'name':name, 'html':page.text})

    progress.complete()
    with open("Data Files/wiki_names.json", "w", encoding="UTF-8") as fp:
        json.dump(output_list,fp)


def process_wiki_pages(name_dict):

    fp = open(_data_location + "wiki_names.json", "r", encoding = "UTF-8")

    wiki_pages = json.load(fp)
    successful = list()
    unsuccessful = list()
    for page in wiki_pages:
        parse_feedback = lookup_name_wiki(page['html'],page['name'])
        if parse_feedback.get("reason") is not None:
            unsuccessful.append(parse_feedback)
        else:
            successful.append(parse_feedback)

    print(len(unsuccessful))
    print(len(successful))
    with open(_data_location + "wiki_names_pass.json", "w", encoding= "UTF-8") as file:
        json.dump(successful, file, indent = 3)

    with open(_data_location + "wiki_names_fail.json", "w", encoding = "UTF-8") as file:
        json.dump(unsuccessful, file, indent = 3)


def get_wiki_people(name):
    # progress = progress_bar(len(name_list),title="get wiki pages")
    html_base = "https://ja.wikipedia.org"
    html_wiki = "/wiki/"
    # for person in name_list:
    # progress.inc_counter()
    page = requests.get("{0}{1}{2}".format(html_base, html_wiki, name))
    output = parse_wiki_people(page.text)
    if '人名の曖昧さ回避' in output['categories']:
        xpath = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'
        tree = html.fromstring(page.text)
        content = tree.xpath(xpath)
        if len(content) == 1:
            content = content[0]
            links = content.xpath('.//li/a')
            for link in links:
                if link.attrib.get('class') != 'new' and name in link.text_content():
                    page = requests.get("{0}{1}".format(html_base, link.attrib.get('href')))
                    if page is not None:
                        output = parse_wiki_people(page.text)
                        if output['date_born']['y'] is not None:
                            break
    # for category in output['categories']:
    #     category_dict.update({category:''})
    # person.update(output)

    # name_list = sorted(name_list,
    #                    key=lambda x: bool(x['date_born']['y']) if x.get('date_born') is not None else None,
    #                    reverse=True)
    # progress.complete()
    return output, output['categories']


def lookup_name_wiki(page_text, full_name):
    return parse_wiki_people(page_text)


def parse_wiki_people(page_text):
    tree = html.fromstring(page_text)
    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]'
    xpath_content = '/div[@id="mw-content-text"]/p'
    xpath_category = '/div[@id="catlinks"]/div[@id="mw-normal-catlinks"]/ul/li/a'

    pattern_hyphen = '[-|~|～|―|－|‐]'
    pattern_year = '([0-9]{3,4})年'
    pattern_month= '([0-9]{1,2})月'
    pattern_day = '([0-9]{1,2})日'
    pattern_date = {'date_born':{'y': (pattern_year, pattern_hyphen),
                                 'm': (pattern_month, pattern_hyphen),
                                 'd': (pattern_day, pattern_hyphen)},
                    'date_death':{'y':(pattern_hyphen, pattern_year),
                                  'm':(pattern_hyphen, pattern_month),
                                  'd':(pattern_hyphen, pattern_day)}}
    output = {'date_born': {'y':None,'m':None,'d':None},
              'date_death':{'y':None,'m':None,'d':None},
              'categories':list()}

    content = tree.xpath(xpath_base + xpath_content)
    if len(content) > 0:
        text = content[0].text_content()
        for key, value in pattern_date.items():
            for date_key, date_pattern in value.items():
                result = re.findall("{0}.*?{1}.*?）.*?。".format(date_pattern[0], date_pattern[1]), text)
                if len(result) > 0:
                    output[key][date_key] = result[0]

    categories = tree.xpath(xpath_base + xpath_category)
    for ea_category in categories:
        output['categories'].append(ea_category.text_content())

    return output

    # items = tree.xpath(xpath_base + xpath_content)
    #
    # date_patterns = {"all":"(?P<year>[0-9]{4})(?:年)(?P<month>[0-9]{1,2})(?:月)(?P<day>[0-9]{1,2})(?:日)",
    #                  "seperated": ["(?P<year>[0-9]{4})(?:年)","(?P<month>[0-9]{1,2})(?:月)",
    #                               "(?P<day>[0-9]{1,2})(?:日)"]}

    # if len(items) > 0:
    #     output = {'name': str(), 'reading': str(),
    #               'date_born': dict(), 'date_death': dict(),
    #               'descr':str(), 'text': items[0].text_content()}
    #     # possibly error prone
    #     text = items[0].text_content().split("。")[0]
    #
    #     start_end = _find_open_close_sympbol(text, "（", "）")
    #     start = start_end[0]
    #     end = start_end[1]
    #
    #     if start == -1:
    #         return {"name": fullname, "reason": "no_reading_date", "text": items[0].text_content(), "html": page_text}
    #
    #     #todo handling for different types of names: kanji, hiragana, katakana
    #     #todo check kanji against used name
    #     name = text[0:start]
    #
    #     if name.find(" ") < 0 or (name.split(" ")[0] + name.split(" ")[1]) != fullname:
    #         return {"name": fullname, "reason": "name_mismatch", "text": items[0].text_content(), "html": page_text}
    #
    #     if name.find("、") >= 0:
    #         return {"name": fullname, "reason": "name_has_breaks", "text": items[0].text_content(), "html": page_text}
    #
    #     output['name'] = name
    #     reading_date = text[start+1:end]
    #     date = str()
    #
    #     if len(reading_date.split("、")) == 2:
    #         date = reading_date.split("、")[-1]
    #         reading = reading_date.split("、")[:-1]
    #
    #     elif len(reading_date.split("、")) > 2:
    #         return {"name": fullname, "reason": "reading_has_breaks", "text": items[0].text_content(), "html": page_text}
    #
    #     else:
    #         has_num = False
    #         idx = int()
    #         for idx, char in enumerate(reading_date):
    #             if char.isnumeric():
    #                 has_num = True
    #                 break
    #         if has_num:
    #             date = reading_date[idx:]
    #         reading = reading_date[0:idx]
    #
    #         return {"name": fullname, "reason": "no_date", "text": items[0].text_content(), "html": page_text}
    #
    #     output['reading'] = reading
    #
    #     if len(date) > 0:
    #         dates = date.split("-")
    #         born_death = list()
    #         for each in dates:
    #             result = re.search(date_patterns['all'], each)
    #             if result is not None:
    #                 born_death.append(result.groupdict())
    #             else:
    #                 date_dict = dict()
    #                 for pattern in date_patterns['seperated']:
    #                     result = re.search(pattern, each)
    #                     if result is not None:
    #                         date_dict.update(result.groupdict())
    #                 born_death.append(date_dict)
    #
    #         if len(born_death) > 0:
    #             output['date_born'] = born_death[0]
    #             output['date_death'] = born_death[1] if len(born_death) > 1 else dict()
    #
    #     next_split = text[end:].find("、") + end + 1
    #     if next_split > -1:
    #         descr = text[next_split:]
    #     else:
    #         descr = text[end:]
    #
    #     output['descr'] = descr
    #     return output
    # else:
    #     return {'name': fullname,'reason': "html_parse_failure", 'text': "", 'html': page_text}


def _find_open_close_sympbol(string, char_open, char_close):
    start = string.find(char_open)
    if start < 0:
        return (-1,-1)
    i = start + 1
    needs_close =  [start]
    final_pos = int()
    while i < len(string):
        if string[i] == char_open:
            needs_close.append(i)
        if string[i] == char_close:
            needs_close.pop()
            if len(needs_close) == 0:
                return (start, i)
        i += 1
    return (start, -1)


def get_wiki_temple_list():
    html_path = "https://ja.wikipedia.org"
    html_base_page = "/wiki/日本の寺院一覧"
    page = requests.get("{0}{1}".format(html_path, html_base_page))
    page.encoding = 'UTF-8'
    tree = html.fromstring(page.text)
    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'
    base = tree.xpath(xpath_base)
    # expect only one item in base
    # get list of links
    list_ahref = base[0].findall('.//li/a')
    progress = progress_bar(len(list_ahref), "getting ({})寺院 from wikipedia".format(len(list_ahref)))
    temple_list = list()
    failed_links = list()
    for link in list_ahref:
        progress.inc_counter()
        if link.attrib.get("href") is not None:
            try:
                page = requests.get("{0}{1}".format(html_path, link.attrib['href']))
                output = parse_wiki_temple_page(page)
                temple_list.append(output)
            except:
                e = sys.exc_info()[0]
                failed_links.append({'name': link.text_content(), 'link': link.attrib['href'], 'error': e})


    temple_list = sorted(temple_list,
                         key = lambda x: (bool(x['title']),bool(x['rdng']), bool(x['founded'])),
                         reverse=True)
    progress.complete()
    return temple_list, failed_links


def parse_wiki_temple_page(page):
    return parse_wiki_shrine_page(page)


def get_wiki_shrine_list():

    html_path = "https://ja.wikipedia.org"
    html_base_page = "/wiki/神社一覧"
    page = requests.get("{0}{1}".format(html_path, html_base_page))
    page.encoding = 'UTF-8'
    tree = html.fromstring(page.text)
    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'
    base = tree.xpath(xpath_base)
    # expect only one item in base
    # get list of links
    list_ahref = base[0].findall('.//li/a')
    failed_links = list()
    output_list = list()
    count = len(list_ahref)
    progress = progress_bar(count, "getting ({})神社 from wikipedia".format(count))
    for each in list_ahref[:count]:
        progress.inc_counter()
        if each.text is not None and each.text[-1] in ['社', '宮'] and each.attrib.get("href") is not None:
            try:
                page = requests.get("{}{}".format(html_path,each.attrib.get("href")))
                output = parse_wiki_shrine_page(page)
                output_list.append(output)
            except:
                e = str(sys.exc_info()[0])
                failed_links.append({'name':each.text, 'link': each.attrib.get("href"), 'error':e})
    progress.complete()
    return output_list, failed_links


def parse_wiki_shrine_page(page):
    output_data = {'title': str(),
                   'rdng': str(),
                   'location': list(),
                   'founded': str()}

    tree = html.fromstring(page.text)
    output_data['title'] = tree.get_element_by_id('firstHeading').text_content().split(" (")[0]

    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'

    xpath_sup = '/p'
    text_details_list = tree.xpath(xpath_base + xpath_sup)
    regex = '（(['+_codeblock_hiragana+']+)[/）|、]'
    match = re.search(regex, text_details_list[0].text_content().replace(' ',''))
    output_data['rdng'] = match.group(1)
    xpath_sup = '/table[@class="infobox"]/tr'
    table = tree.xpath(xpath_base + xpath_sup)
    for each in table:
        label_column = each.getchildren()[0]
        if label_column.text_content() == '所在地':
            regex = '(['+_codeblock_kanji+']+?[都|道|府|県])(['+_codeblock_kanji+']+?[市|郡])*' \
                        '(['+_codeblock_kanji+']+?[区])*(['+_codeblock_kanji+']+?[町|村])*'
            match = re.findall(regex, each[1].text_content())
            if len(match) > 0:
                output_data['location'] = list(filter(None,match[0]))
        elif label_column.text_content() == '創建' or label_column.text_content() == '創建年':
            regex_date = '(?<!['+_codeblock_kanji+'])([0-9]{3,4})[年]'
            match = re.findall(regex_date, each[1].text_content())
            if len(match) > 0:
                output_data['founded'] = match[0]

    return output_data


def get_wiki_shiro_list():
    progress = progress_bar(5, "getting 市町村 from wikipedia...")
    html_path = "https://ja.wikipedia.org"
    html_base_page = "/wiki/日本の城一覧"
    page = requests.get("{0}{1}".format(html_path, html_base_page))
    page.encoding = 'UTF-8'
    tree = html.fromstring(page.text)
    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'
    return


def parse_wiki_shiro_page(page):


    return


def get_wiki_shichouson_list():

    html_path = "https://ja.wikipedia.org"
    html_base_page = "/wiki/日本の地方公共団体一覧"
    page = requests.get("{0}{1}".format(html_path, html_base_page))
    page.encoding = 'UTF-8'
    tree = html.fromstring(page.text)
    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'
    base = tree.xpath(xpath_base)
    # expect only one item in base
    list_a = base[0].findall('.//b/a')
    list_a += base[0].findall('.//span/a')
    list_a += base[0].findall('.//li/a')

    location_list = list()
    parse_failure_list = list()
    counter = len(list_a)
    failed_links = list()
    locations_set = set()
    progress = progress_bar(counter, "getting ({})市町村 from wikipedia...".format(counter))
    for a in list_a:
        counter -= 1
        data = {'title':str(), 'rdng':str(), 'location': list(), 'population': str(), 'area': str()}
        if a.text is None:
            continue
        if a.text[-1] in ['郡', '局', '市','町','村','区']:
            title = {'title': 'title', 'data': a.text}
            rdng = {'title': 'rdng', 'data': str(), 'xpath': '/p',
                    'function': lambda xpath_result:
                            re.search('（(['+_codeblock_hiragana +']+)[）|、]', xpath_result[0].get_content()).group(1)}
            locations = list()
            location_rule_one = {'title': '都道府県', 'data': locations, 'xpath': '/table[@class="infobox bordered"]/tr',
                                 'function': lambda found_row: found_row.getchildren()[1].text_content().split(" "),
                                 'helper_function': parse_wiki_helper_infotable}
            location_rule_two = {'title': '郡', 'data': locations, 'xpath': '/table[@class="infobox bordered"]/tr',
                                 'function': lambda found_row: found_row.getchildren()[1].text_content(),
                                 'helper_function': parse_wiki_helper_infotable}
            location_rule_two = {'title': '郡', 'data': locations, 'xpath': '/table[@class="infobox bordered"]/tr',
                                 'function': lambda found_row: found_row.getchildren()[1].text_content(),
                                 'helper_function': parse_wiki_helper_infotable}
            population = {'title': '総人口', 'data': str(), 'xpath': '/table[@class="infobox bordered"]/tr',
                          'function': lambda found_row: found_row.getchildren()[0].text_content().replace(',',""),
                          'helper_function': parse_wiki_helper_infotable}


            try:
                location_page = requests.get("{0}{1}".format(html_path, a.get("href")))
                parsed_location = parse_wiki_shichouson(location_page)
                location_list.append(parsed_location)
            except:
                e = sys.exc_info()[0]

            #
            # if parse_failure_list is None:
            #     parse_failure_list.append(a.text)
            # else:
            #     parsed_failed = False
            #     for value in parsed_location.values():
            #         if value is None:
            #             parse_failure_list.append(a.text)
            #             parse_failed = True
            #             break
            #     if not parsed_failed:
            #         location_list.append(parsed_location)
        progress.inc_counter()
        if counter == 0:
            break
    progress.complete()
    return location_list, failed_links


def parse_wiki_helper_rematch(xpath_result, data_field):
    return re.match(data_field['regex'], xpath_result.text_content()).group(1)


def parse_wiki_helper_infotable(xpath_result, data_field):
    for each in xpath_result:
        label_column = each.getchildren()[0].text_content()
        if label_column in data_field['title']:
            return each.getchildrend()[1]


def parse_wiki_page(page, parse_rules):
    # parse rules - list of tuples: (data field, xpath, handler_function(xpath_result))
    tree = html.fromstring(page.text)
    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'
    for parse_rule in parse_rules:
        xpath_result = tree.xpath(xpath_base + parse_rule[1])
        parse_rule[0]['data'] = parse_rule[2](xpath_result, parse_rule[0])


def parse_wiki_shichouson(page):
    tree = html.fromstring(page.text)
    title = tree.get_element_by_id('firstHeading').text_content().split(" (")[0]
    reading = None
    population = None
    area = None
    location = None
    xpath_base = '//html/body/div[@id="content"]/div[@id="bodyContent"]/div[@id="mw-content-text"]'
    if title[-1] == '郡':
        xpath_sup = '/div[@class="pathnavbox"]/ul/li/div'
        text_details_list = tree.xpath(xpath_base + xpath_sup)
        if len(text_details_list) == 0:
            xpath_sup = '/div[@class="pathnavbox"]'
            text_details_list = tree.xpath(xpath_base + xpath_sup)

        text_location_list = text_details_list[-1].text_content().split("\xa0> ")
        if text_location_list[1] == '北海道':
            location = ['北海道', text_location_list[2]]
        else:
            location = [text_location_list[-2]]
        name = text_location_list[-1]
        xpath_sup = '/p'
        text_details_list = tree.xpath(xpath_base + xpath_sup)
        reading_start = text_details_list[0].text_content().find('（')
        reading_end = text_details_list[0].text_content().find('）')
        reading = text_details_list[0].text_content()[reading_start+1:reading_end]
        population = None
        area = None
        text_list = text_details_list[1].text_content().split('、')
        population = text_list[0].replace('人口','').replace('人','').replace(',','')
        area = text_list[1].replace('面積','').replace('km²','').replace(',','')

    elif title[-1] == '局':
        location = ['北海道']
        xpath_sup = '/p'
        text_details_list = tree.xpath(xpath_base + xpath_sup)
        reading_start = text_details_list[0].text_content().find('（')
        reading_end = text_details_list[0].text_content().find('）')
        reading = text_details_list[0].text_content()[reading_start+1:reading_end]

        xpath_sup = '/table[@class="wikitable"]/tr'
        table = tree.xpath(xpath_base + xpath_sup)
        for each in table:
            label_column = each.getchildren()[0]
            if label_column.text_content() == '面積':
                area = each.getchildren()[1].getchildren()[0].text_content().replace(",","")
            elif label_column.text_content() == '総人口':
                population = each.getchildren()[1].getchildren()[0].text_content().replace(',',"")

    elif title[-1] in ['市','町','村','区']:
        location = list()
        xpath_sup = '/p'
        text_details_list = tree.xpath(xpath_base + xpath_sup)
        reading_start = text_details_list[0].text_content().find('（')
        reading_end = text_details_list[0].text_content().find('）')
        reading = text_details_list[0].text_content()[reading_start+1:reading_end]

        xpath_sup = '/table[@class="infobox bordered"]/tr'
        table = tree.xpath(xpath_base + xpath_sup)
        for each in table:
            label_column = each.getchildren()[0]
            if label_column.text_content() == '面積':
                area = each.getchildren()[1].getchildren()[0].text_content().replace(",","")
            elif label_column.text_content() == '総人口':
                population = each.getchildren()[1].getchildren()[0].text_content().replace(',',"")
            elif label_column.text_content() == '都道府県':
                location += each.getchildren()[1].text_content().split(" ")
            elif label_column.text_content() in ['郡','市']:
                location.append(each.getchildren()[1].text_content())

    return {'txt':title, 'rdng':reading, 'location': location, 'population': population, 'area': area}
    #   print(title)
    #   print("-"*20)
    #   print("Reading: {0}".format(reading))
    #   print("Location: {0}".format(' '.join(location)))
    #   if population is not None:
    #    print("Population: {0}".format(population))
    #   if area is not None:
    #   print("Area: {0}km²".format(area))