__author__ = 'Brian'

from sqlalchemy import create_engine, Table, Column, Integer, Boolean, String, ForeignKey, Unicode
from sqlalchemy.ext.declarative import declarative_base
from progressbar import progress_bar
from JMDictParser import create_dict_from_jmdict_list
from wchartype import is_full_digit, is_full_punct
import string
_data_location = "Data Files/"
_database_location = "wnjpn.db"

# This application makes use of the Japanese WordNet. The following is
# the copyright information of this database:
#
# Copyright: 2012-2015 Francis Bond
# Copyright: 2009-2011 NICT
# Japanese WordNet
#
# This software and database is being provided to you, the LICENSEE, by
# the National Institute of Information and Communications Technology
# under the following license.  By obtaining, using and/or copying this
# software and database, you agree that you have read, understood, and
# will comply with these terms and conditions:
#
#   Permission to use, copy, modify and distribute this software and
#   database and its documentation for any purpose and without fee or
#   royalty is hereby granted, provided that you agree to comply with
#   the following copyright notice and statements, including the
#   disclaimer, and that the same appear on ALL copies of the software,
#   database and documentation, including modifications that you make
#   for internal use or for distribution.
#
# Japanese WordNet Copyright 2009-2011 by the National Institute of
# Information and Communications Technology (NICT); 2012-2014 by Francis
# Bond.  All rights reserved.
#
# THIS SOFTWARE AND DATABASE IS PROVIDED "AS IS" AND NICT MAKES NO
# REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED.  BY WAY OF EXAMPLE,
# BUT NOT LIMITATION, NICT MAKES NO REPRESENTATIONS OR WARRANTIES OF
# MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE
# OF THE LICENSED SOFTWARE, DATABASE OR DOCUMENTATION WILL NOT INFRINGE
# ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
#
# The name of the National Institute of Information and Communications
# Technology may not be used in advertising or publicity pertaining to
# distribution of the software and/or database.  Title to copyright in
# this software, database and any associated documentation shall at all
# times remain with National Institute of Information and Communications
# Technology and LICENSEE agrees to preserve same.

def _make_word_entry(lemma, lexid, freq):
    return dict({'lemma': lemma, 'lexid': lexid, 'freq': freq})

def _make_synset_dict():
    return dict({'jpn_words': [], 'eng_words': [],
                 'eng_def': [], 'jpn_def': [],
                 'eng_ex': [], 'jpn_ex': []})

def make_wordnet_dictionary():
    progress_function = progress_bar(1,"making dictionary from wordnet")
    base = declarative_base()
    engine = create_engine('sqlite:///'+_data_location+_database_location, echo=False)

    # Make synset dictionary with words
    synset_dict = dict()
    sql = "SELECT synset, lemma, sense.lang AS lang, lexid, freq FROM sense JOIN word ON sense.wordid = word.wordid"
    result = engine.execute(sql).fetchall()
    progress = progress_bar(len(result), "processing synsets")
    for row in result:
        progress.inc_counter()
        if synset_dict.get(row['synset']) is None:
            synset_dict.update({row['synset']:_make_synset_dict()})
        word = _make_word_entry(row['lemma'], row['lexid'], row['freq'])
        if row['lang'] == 'eng':
            synset_dict[row['synset']]['eng_words'].append(word)
        if row['lang'] == 'jpn':
            synset_dict[row['synset']]['jpn_words'].append(word)
    progress.complete()

    sql = "SELECT synset, lang, def FROM synset_def ORDER BY sid ASC"
    result = engine.execute(sql).fetchall()
    progress = progress_bar(len(result), "processing definitions")
    for row in result:
        progress.inc_counter()
        if row['lang'] == 'eng' and row['def'] not in synset_dict[row['synset']]['eng_def']:
            synset_dict[row['synset']]['eng_def'].append(row['def'])
        if row['lang'] == 'jpn' and row['def'] not in synset_dict[row['synset']]['jpn_def']:
            synset_dict[row['synset']]['jpn_def'].append(row['def'])
    progress.complete()

    sql = "SELECT synset, lang, def FROM synset_ex ORDER BY sid ASC"
    result = engine.execute(sql).fetchall()
    progress = progress_bar(len(result), "processing examples")
    for row in result:
        progress.inc_counter()
        if row['lang'] == 'eng' and row['def'] not in synset_dict[row['synset']]['eng_ex']:
            synset_dict[row['synset']]['eng_ex'].append(row['def'])
        if row['lang'] == 'jpn' and row['def'] not in synset_dict[row['synset']]['jpn_ex']:
            synset_dict[row['synset']]['jpn_ex'].append(row['def'])
    progress.complete()

    progress_function.complete()
    return synset_dict

def make_lemma_dictionaries(synset_dict):

    #reduce_japanese_in_synset_dict(synset_dict, jmdict_list)
    eng_lemma_dict = dict()
    jpn_lemma_dict = dict()
    progress_function = progress_bar(len(synset_dict), "make Japanese and English dicitonaries from synset")
    for entry in synset_dict.values():
        progress_function.inc_counter()
        for eng_word in entry['eng_words']:
            if eng_lemma_dict.get(eng_word['lemma']) is None:
                eng_lemma_dict.update({eng_word['lemma']:dict()})
            for jpn_word in entry['jpn_words']:
                if eng_lemma_dict[eng_word['lemma']].get(jpn_word['lemma']) is None:
                    eng_lemma_dict[eng_word['lemma']].update({jpn_word['lemma']:''})
        for jpn_word in entry['jpn_words']:
            if jpn_word['lemma'] not in jpn_lemma_dict:
                jpn_lemma_dict.update({jpn_word['lemma']:set()})
            for eng_word in entry['eng_words']:
                jpn_lemma_dict[jpn_word['lemma']].add(eng_word['lemma'].lower().replace("_", " "))
    progress_function.complete()
    return jpn_lemma_dict


def reduce_japanese_in_synset_dict(synset_dict, jmdict_list, ime_dict):
    progress = progress_bar(len(synset_dict), "removing japanese lemmas not found in jmdict")
    jmdict = create_dict_from_jmdict_list(jmdict_list, needs_furigana=False, needs_ime=False)
    removed_japanese = dict()

    for entry in synset_dict.values():
        progress.inc_counter()
        keep_entries = list()
        keep_list = list()
        keep_reading_list= list()
        words_to_remove = list()
        # First find all words that are in jmdict and make a list
        # Separate off words that don't have kanji
        # Sort found words by importance,
        # Do not add words that are already added from the same entry
        # Do not add words that share the same reading
        # Next look for remaining words in ime (following same remarks above)
        # Output remaining words to file
        for jpn_word in entry['jpn_words']:
            lemma = jpn_word['lemma']
            if len(lemma) > 1 and _is_punctuation(lemma[0]):
                lemma = lemma[1:]
            if len(lemma) > 1 and _is_punctuation(lemma[-1]):
                lemma = lemma[:-1]
            if len(lemma) > 1 and jmdict.get(lemma) is not None:
                kanji_list = jmdict[lemma]
                for kanji, entry in kanji_list:

                    for kanji_rdng in kanji['kanji_rdng']:
                        keep_reading_list.append(kanji_rdng['rdng']['txt'])
                keep_list.append(jpn_word)

            else:
                words_to_remove.append(jpn_word)

        if len(keep_list) > 0:
            entry['jpn_words'] = keep_list
            for jpn_word in words_to_remove:
                if removed_japanese.get(jpn_word['lemma']) is None:
                    removed_japanese.update({jpn_word['lemma']:jpn_word})

    progress.complete()
    print("Removed {0} Japanese words from synset dictionary.".format(len(removed_japanese)))
    return removed_japanese


def _is_punctuation(char):
    return is_full_punct(char) or char in string.punctuation